import pytest

from llhardware import _cpp, io


@pytest.fixture(name="owen")
def _owen() -> io.RuOwen:
    return io.RuOwen(_cpp.io.RuOwen(None, True, int(0.25e9)))  # type: ignore


@pytest.mark.parametrize(
    "name,res",
    [
        ("A.LEN", 7890),
        ("SL.H", 60448),
        ("PV", 47327),
        ("R.OUT", 39238),
        ("O", 13800),
        ("C.SP.O", 46941),
        ("CJ-.C", 64104),
        ("EV-1", 11410),
        ("INIT", 233),
    ],
)
def test_name2cmd(owen: io.RuOwen, name: str, res: int) -> None:
    assert owen.name2cmd(name) == res


@pytest.mark.parametrize(
    "args,res",
    [
        ((1, True, 7890, bytes([])), "GHHGHUTIKGJI"),
        ((1, True, 51328, bytes([0, 0])), "GHHISOOGGGGGQSUR"),
        ((1, False, 14835, bytes([0, 0, 0, 0, 0])), "GHGLJPVJGGGGGGGGGGGRJJ"),
        ((1, False, 57725, bytes([195, 71, 230, 0, 0])), "GHGLUHNTSJKNUMGGGGLPTV"),
        ((1, False, 46894, bytes([0])), "GHGHRNIUGGMJSQ"),
        ((1, False, 37127, bytes([65, 200, 0, 0, 0])), "GHGLPHGNKHSOGGGGGGJOMV"),
        ((1, False, 46944, bytes([8])), "GHGHRNMGGORMUL"),
    ],
)
def test_make_packet(owen: io.RuOwen, args: tuple[int, bool, int, bytes], res: str) -> None:
    assert owen.make_packet(*args) == res


@pytest.mark.parametrize(
    "msg,data",
    [
        ("GHGHHUTIGGJKGK", io.RuOwen.PacketData(1, False, 7890, bytes([0]))),
        ("GHGJSOOGGGGGGGUQRK", io.RuOwen.PacketData(1, False, 51328, bytes([0, 0, 0]))),
        ("GHGLUHNTSJKNUMGGGGLPTV", io.RuOwen.PacketData(1, False, 57725, bytes([195, 71, 230, 0, 0]))),
        (
            "GHGOITLRJKJGJGJGIUJJJGLMUPPR",
            io.RuOwen.PacketData(1, False, 11611, bytes([52, 48, 48, 48, 46, 51, 48, 86])),
        ),
        ("GHGJGIJJKNRKMLLNJK", io.RuOwen.PacketData(1, False, 563, bytes([71, 180, 101]))),
        ("GHGHJONIMKKIMP", io.RuOwen.PacketData(1, False, 14450, bytes([100]))),
    ],
)
def test_parse_packet(owen: io.RuOwen, msg: str, data: io.RuOwen.PacketData) -> None:
    assert owen.parse_packet(msg) == data


@pytest.mark.parametrize("msg", ["", "GHGLUHNTSJKNUMGGGGLPTD"])
def test_parse_packet_error(owen: io.RuOwen, msg: str) -> None:
    with pytest.raises(RuntimeError):
        owen.parse_packet(msg)
