from typing import List, Tuple

import pytest

from llhardware.planners import GroupPlan, SimplePlan
from llhardware.planners.interfaces import ICompiledStepperMotorPlan


@pytest.mark.parametrize(
    "planner,points",
    [
        (
            SimplePlan.create(-6000, 22500, 0.0095).freeze(500000, (0, 0), 10000 / 1e9).compile(1_000_000, (0, 0)),
            [
                (0, 0.0),
                (1_000_000, 0.0),
                (1_100_000, -0.435 - 1),
                (1_200_000, -0.84 - 2),
                (1_262_400, -1.07751936 - 2.624),
                (1_262_500, -3.702890625),
                (1_262_600, -3.70326186),
                (6_000_000, 12.375),
                (10_000_000, 78.375),
                (100_000_000, 78.375),
            ],
        ),
        (
            SimplePlan.create(0, -14250, 0.0095).freeze(500000, (10, 0.5), 10000 / 1e9).compile(1_000_000, (0, 0.1)),
            [
                (0, 0.1),
                (1_500_000, 5.1 - 0.5625),
                (2_000_000, 10.1 - 1.5),
                (2_021_150, 10.3115 - 1.5479229919),
                (2_021_250, 8.7643488281),
                (2_021_350, 8.7641206331),
                (10_000_000, -57.1875),
                (6_000_000, -12.1875),
                (100_000_000, -57.1875),
            ],
        ),
        (
            SimplePlan.create(1000, 1000, 1).freeze(0, (-900, 0), 1000 / 1e9).compile(500_000_000, (0, 0)),
            [
                (0, 0.0),
                (500_000_000, 0.0),
                (600_000_000, 0.0),
                (800_000_000, 0.0),
                (900_000_000, 0.0),
                (953_224_000, 53.224),
                (1_000_000_000, 100.0),
            ],
        ),
        (
            SimplePlan.create(100000, 100000 - 0.1, 60).freeze(0, (0, 0), 1000 / 1e9).compile(0, (0, 0)),
            [
                (0, 0.0),
                (15_000_000_000, 1_500_000 - 0.1875),
                (30_000_000_000, 3_000_000 - 0.75),
                (60_000_000_000, 6_000_000 - 3),
            ],
        ),
        (
            # All 3 parts (Newton, normal, Newton)
            SimplePlan.create(-3e7, 3e7 - 1000, 0.003).freeze(0, (0, 0), 1000 / 1e9).compile(0, (0, 0)),
            [
                (0, 0.0),
                (5000, -149.75000416666666),
                (155000, -4409.754004166667),
                (156000, -4436.644056),
                (750012, -16875.273751560024),
                (1343000, -22253.810608166666),
                (1345000, -22260.051504166662),
                (3000000, -1.5),
                (30000000, -1.5),
            ],
        ),
        (
            GroupPlan.create([SimplePlan.create(0, 100, 0.1), SimplePlan.create(100, 0, 0.1)])
            .freeze(0, (0, 0), 1000 / 1e9)
            .compile(0, (0, 0)),
            [
                (0, 0.0),
                (int(0.05e9), 1.25),
                (int(0.1e9), 5.0),
                (int(0.15e9), 8.75),
                (int(0.2e9), 10.0),
                (int(2e9), 10.0),
            ],
        ),
    ],
)
def test_get_pos(planner: ICompiledStepperMotorPlan, points: List[Tuple[int, float]]) -> None:
    for cur_time, expected_value in points:
        assert sum(planner.get_pos(cur_time)) == pytest.approx(expected_value, abs=1e-4)


@pytest.mark.parametrize(
    "planner,start_pos,cur_time_ns,end_pos",
    [
        (
            SimplePlan.create(-6000, 22500, 0.0095).freeze(500000, (0, 0), 10000 / 1e9).compile(1_000_000, (0, 0)),
            0,
            1_000_000,
            78,
        ),
        (
            SimplePlan.create(0, -14250, 0.0095).freeze(500000, (10, 0.5), 10000 / 1e9).compile(1_000_000, (0, 0.1)),
            0,
            1_000_000,
            -57,
        ),
        (
            SimplePlan.create(1000, 1000, 1).freeze(0, (-900, 0), 1000 / 1e9).compile(500_000_000, (0, 0)),
            0,
            500_000_000,
            100,
        ),
        (
            # Only Newton
            SimplePlan.create(10000, 10000 - 0.03, 60).freeze(0, (0, 0), 100000 / 1e9).compile(0, (0, 0)),
            0,
            0,
            599_999,
        ),
        (
            # All 3 parts (Newton, normal, Newton)
            SimplePlan.create(-3e7, 3e7 - 1000, 0.003).freeze(0, (0, 0), 1000 / 1e9).compile(0, (0, 0)),
            0,
            0,
            -2,
        ),
        (
            GroupPlan.create([SimplePlan.create(0, 100, 0.1), SimplePlan.create(100, 0, 0.1)])
            .freeze(0, (0, 0), 1000 / 1e9)
            .compile(0, (0, 0)),
            0,
            0,
            10,
        ),
    ],
)
@pytest.mark.timeout(10)
def test_compute(planner: ICompiledStepperMotorPlan, start_pos: int, cur_time_ns: int, end_pos: int) -> None:
    cur_time_ns, pos = planner.compute(cur_time_ns)
    assert pos == start_pos

    while cur_time_ns > 0:
        expected_pos = round(sum(planner.get_pos(cur_time_ns)))
        prev_pos = round(sum(planner.get_pos(cur_time_ns - 20)))
        assert abs(expected_pos - prev_pos) == 1
        cur_time_ns, pos = planner.compute(cur_time_ns)
        assert pos == expected_pos
    assert pos == end_pos
