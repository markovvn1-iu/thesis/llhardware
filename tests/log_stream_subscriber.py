import time
from typing import Any, Optional, TypeVar

import pytest

from llhardware.core import SimpleStream, StreamSubscriber

DataItemT = TypeVar("DataItemT")


class LogStreamSubscriber(StreamSubscriber[DataItemT]):
    logs: list[tuple[DataItemT, int]]
    start_time_ns: int

    @classmethod
    def create(
        cls, stream: Optional["SimpleStream[DataItemT]"] = None, get_last_value: bool = False
    ) -> "LogStreamSubscriber[DataItemT]":
        self = cls()
        if stream is not None:
            stream.add(self, get_last_value=get_last_value)
        return self

    def __init__(self, *args: Any, **kwargs: Any) -> None:
        self.logs = []
        super().__init__(*args, **kwargs)
        self.start_time_ns = time.monotonic_ns()

    def _on_get_data(self, item: DataItemT) -> None:
        self.logs.append((item, time.monotonic_ns()))

    def assert_logs_exact(self, expected: list[tuple[DataItemT, float]]) -> None:
        for a, b in zip(self.logs, expected):
            assert a[0] == b[0], "value is not equal"
            assert (a[1] - self.start_time_ns) / 1e9 == pytest.approx(b[1], abs=1e-3)
        assert len(self.logs) == len(expected)

    def assert_values(self, expected: list[DataItemT]) -> None:
        assert [i[0] for i in self.logs] == expected
