import time
from typing import Optional, Union

import pytest

from llhardware.core import BusyThreadUpdater, EventThreadUpdater, IOUpdate, LambdaSimpleUpdate, SimpleUpdate


class MyThreadUpdater:
    _thr: Union[BusyThreadUpdater, EventThreadUpdater]
    logs: list[tuple[int, int, int]]
    start_time_ns: Optional[int] = None

    def __init__(self, busy_waiting: bool) -> None:
        if busy_waiting:
            self._thr = BusyThreadUpdater.create()
        else:
            self._thr = EventThreadUpdater.create()
        self.logs = []

    def sleep_until(self, target_time: float) -> None:
        assert self.start_time_ns is not None
        time.sleep(target_time - (time.monotonic_ns() - self.start_time_ns) / 1e9)

    def create_updater(self, _id: int, update_delay_ns: int = -1, allow_skips: bool = True) -> LambdaSimpleUpdate:
        updater = LambdaSimpleUpdate.create(
            lambda _, cur_time: self.logs.append((_id, cur_time, time.monotonic_ns())), update_delay_ns, allow_skips
        )
        self.add(updater)
        return updater

    def start(self) -> None:
        self._thr.start()
        self.start_time_ns = time.monotonic_ns()

    def stop(self) -> None:
        self._thr.stop()

    def add(self, updater: Union[SimpleUpdate, IOUpdate]) -> None:
        if isinstance(self._thr, BusyThreadUpdater):
            assert isinstance(updater, SimpleUpdate)
            self._thr.add(updater)
        else:
            self._thr.add(updater)

    def remove(self, updater: SimpleUpdate) -> bool:
        return self._thr.remove(updater)

    def assert_logs_exact(self, expected: list[tuple[int, float]], e: float = 1e-3) -> None:
        assert self.start_time_ns is not None
        assert len(self.logs) == len(expected)
        for a, b in zip(self.logs, expected):
            assert a[0] == b[0], "id is not equal"
            assert (a[1] - self.start_time_ns) / 1e9 == pytest.approx(b[1], abs=e)
            assert (a[2] - self.start_time_ns) / 1e9 == pytest.approx(b[1], abs=e)
