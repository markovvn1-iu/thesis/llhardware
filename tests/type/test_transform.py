from llhardware.types import FloatDataItem, Vec3FloatDataItem


def test_simple_transform() -> None:
    transform = Vec3FloatDataItem.get_x()
    res = transform.transform(Vec3FloatDataItem.create(1, 2, 3))
    assert isinstance(res, FloatDataItem) and res.value == 1
