from llhardware.core import StreamMaster
from llhardware.types import FloatDataItem, GyroAccelTempDataItem, Vec3FloatDataItem

from ..log_stream_subscriber import LogStreamSubscriber


def test_float_stream() -> None:
    stream: StreamMaster[FloatDataItem] = StreamMaster.create()
    sub = LogStreamSubscriber.create(stream)
    stream.push(FloatDataItem.create(0.4))
    stream.push(FloatDataItem.create(-4.2))
    assert [i[0].value for i in sub.logs] == [0.4, -4.2]


def test_vec3_to_float_stream() -> None:
    stream: StreamMaster[Vec3FloatDataItem] = StreamMaster.create()
    sub_x = LogStreamSubscriber.create(stream | Vec3FloatDataItem.get_x())
    sub_y = LogStreamSubscriber.create(stream | Vec3FloatDataItem.get_y())
    stream.push(Vec3FloatDataItem.create(1, 2, 3))
    stream.push(Vec3FloatDataItem.create(4, None, 5))
    stream.push(Vec3FloatDataItem.create(6, 7, 8))
    assert all(isinstance(i[0], FloatDataItem) for i in sub_x.logs)
    assert [i[0].value for i in sub_x.logs] == [1, 4, 6]
    assert all(isinstance(i[0], FloatDataItem) for i in sub_y.logs)
    assert [i[0].value for i in sub_y.logs] == [2, 7]


def test_gyro_to_float_stream() -> None:
    stream: StreamMaster[GyroAccelTempDataItem] = StreamMaster.create()
    sub_gx = LogStreamSubscriber.create(stream | GyroAccelTempDataItem.get_gyro() | Vec3FloatDataItem.get_x())
    sub_ay = LogStreamSubscriber.create(stream | GyroAccelTempDataItem.get_accel() | Vec3FloatDataItem.get_y())
    sub_temp = LogStreamSubscriber.create(stream | GyroAccelTempDataItem.get_temp() | (lambda x: x.value * 2))
    stream.push(GyroAccelTempDataItem.create((1, 2, 3), (4, 5, 6), 7))
    stream.push(GyroAccelTempDataItem.create((8, None, None), (None, 9, None), 10))
    stream.push(GyroAccelTempDataItem.create((None, 11, None), (None, 12, None), 13))
    stream.push(GyroAccelTempDataItem.create(None, (None, None, 14), 15))
    stream.push(GyroAccelTempDataItem.create((16, None, None), None, None))
    assert all(isinstance(i[0], FloatDataItem) for i in sub_gx.logs)
    assert [i[0].value for i in sub_gx.logs] == [1, 8, 16]
    assert all(isinstance(i[0], FloatDataItem) for i in sub_ay.logs)
    assert [i[0].value for i in sub_ay.logs] == [5, 9, 12]
    assert all(isinstance(i[0], float) for i in sub_temp.logs)
    assert [i[0] for i in sub_temp.logs] == [7 * 2, 10 * 2, 13 * 2, 15 * 2]
