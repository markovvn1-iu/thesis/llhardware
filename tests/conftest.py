from typing import Iterator

import pytest

from llhardware.core.thread_updater import BusyThreadUpdater


@pytest.fixture(name="thr")
def _thr() -> Iterator[BusyThreadUpdater]:
    thr = BusyThreadUpdater.create()
    thr.start()
    try:
        yield thr
    finally:
        thr.stop()
