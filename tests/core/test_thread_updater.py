import gc
import random
import time
from weakref import ref

import pytest

from ..my_thread_updater import MyThreadUpdater


@pytest.mark.parametrize("busy_waiting", [True, False])
def test_one_simple(busy_waiting: bool) -> None:
    thread_updater = MyThreadUpdater(busy_waiting=busy_waiting)
    updater1 = thread_updater.create_updater(0, 100_000_000)
    assert updater1.is_attached
    thread_updater.start()
    time.sleep(0.55)
    thread_updater.stop()
    thread_updater.assert_logs_exact(
        [
            (0, 0.0),
            (0, 0.1),
            (0, 0.2),
            (0, 0.3),
            (0, 0.4),
            (0, 0.5),
        ]
    )
    del updater1


@pytest.mark.parametrize("busy_waiting", [True, False])
def test_two_simple(busy_waiting: bool) -> None:
    thread_updater = MyThreadUpdater(busy_waiting=busy_waiting)
    updater1 = thread_updater.create_updater(0, 100_000_000)
    time.sleep(0.05)
    updater2 = thread_updater.create_updater(1, 100_000_000)
    thread_updater.start()
    time.sleep(0.225)
    thread_updater.stop()
    thread_updater.assert_logs_exact(
        [
            (0, 0.00),
            (1, 0.00),
            (0, 0.05),
            (1, 0.10),
            (0, 0.15),
            (1, 0.20),
        ]
    )
    del updater1, updater2


@pytest.mark.parametrize("busy_waiting", [True, False])
@pytest.mark.parametrize("delete", [0, 1, 2])
def test_two_detach(busy_waiting: bool, delete: int) -> None:
    thread_updater = MyThreadUpdater(busy_waiting=busy_waiting)
    updater1 = thread_updater.create_updater(0, 500_000_000)
    updater2 = thread_updater.create_updater(1, 150_000_000)
    thread_updater.start()
    time.sleep(0.65)
    assert updater1.is_attached and updater2.is_attached
    if delete == 0:
        del updater2
        gc.collect()
    elif delete == 1:
        assert thread_updater.remove(updater2)
        assert updater1.is_attached and not updater2.is_attached
    elif delete == 2:
        assert updater2.detach()
        assert updater1.is_attached and not updater2.is_attached
    time.sleep(0.50)
    thread_updater.stop()
    thread_updater.assert_logs_exact(
        [
            (0, 0.00),
            (1, 0.00),
            (1, 0.15),
            (1, 0.30),
            (1, 0.45),
            (0, 0.50),
            (1, 0.60),
            (0, 1.00),
        ]
    )
    del updater1


@pytest.mark.parametrize("busy_waiting", [True, False])
def test_no_updaters_stop(busy_waiting: bool) -> None:
    thread_updater = MyThreadUpdater(busy_waiting=busy_waiting)
    thread_updater.start()
    start_time = time.monotonic_ns()
    thread_updater.stop()
    assert time.monotonic_ns() - start_time < 0.05e9


@pytest.mark.parametrize("busy_waiting", [True, False])
def test_stop_with_updater(busy_waiting: bool) -> None:
    thread_updater = MyThreadUpdater(busy_waiting=busy_waiting)
    updater1 = thread_updater.create_updater(0)
    thread_updater.start()
    start_time = time.monotonic_ns()
    thread_updater.stop()
    del updater1
    assert time.monotonic_ns() - start_time < 0.05e9


def test_call_and_detach() -> None:
    for _ in range(1000):
        thread_updater = MyThreadUpdater(busy_waiting=True)
        updater1 = thread_updater.create_updater(0, 500_000_000)
        thread_updater.start()
        updater1.call_soon(0)
        del updater1
        thread_updater.stop()


def test_auto_detach() -> None:
    thread_updater1 = MyThreadUpdater(busy_waiting=True)
    thread_updater2 = MyThreadUpdater(busy_waiting=True)
    updater1 = thread_updater1.create_updater(0, 200_000_000)
    thread_updater1.start()
    thread_updater2.start()
    thread_updater1.sleep_until(0.3)

    updater1.update_delay_ns = 100_000_000
    thread_updater1.add(updater1)
    thread_updater1.sleep_until(0.3 + 0.25)

    updater1.update_delay_ns = 200_000_000
    thread_updater2.add(updater1)

    thread_updater1.sleep_until(0.3 + 0.25 + 0.1)
    thread_updater1.stop()

    thread_updater1.sleep_until(0.3 + 0.25 + 0.3)
    updater1.detach()

    thread_updater1.sleep_until(0.3 + 0.25 + 0.7)
    thread_updater2.stop()
    thread_updater1.assert_logs_exact(
        [
            (0, 0.0),
            (0, 0.2),
            (0, 0.3),
            (0, 0.4),
            (0, 0.5),
            (0, 0.55),
            (0, 0.75),
        ]
    )


@pytest.mark.parametrize("busy_waiting", [True, False])
@pytest.mark.parametrize("allow_skips,res_count", [(False, 6), (True, 2)])
def test_allow_skips(busy_waiting: bool, allow_skips: bool, res_count: int) -> None:
    thread_updater = MyThreadUpdater(busy_waiting=busy_waiting)
    updater1 = thread_updater.create_updater(0, 100_000_000, allow_skips=allow_skips)
    time.sleep(0.525)
    thread_updater.start()
    time.sleep(0.025)
    thread_updater.stop()
    thread_updater.assert_logs_exact([(0, 0.0)] * res_count)
    del updater1


@pytest.mark.parametrize("busy_waiting", [True, False])
def test_as_fast_as_possible(busy_waiting: bool) -> None:
    thread_updater = MyThreadUpdater(busy_waiting=busy_waiting)
    updater1 = thread_updater.create_updater(0, 0)
    updater2 = thread_updater.create_updater(1, 0)
    thread_updater.start()
    time.sleep(0.1)
    thread_updater.stop()
    assert len(thread_updater.logs) > 10_000
    for i, log in enumerate(thread_updater.logs):
        assert log[0] == i % 2
    del updater1, updater2


@pytest.mark.parametrize("busy_waiting", [True, False])
def test_force_call_later(busy_waiting: bool) -> None:
    """Check that call_later is predictable and not overridden by the output of the updater."""
    thread_updater = MyThreadUpdater(busy_waiting=busy_waiting)
    updater1 = thread_updater.create_updater(0, 0)
    thread_updater.start()

    thread_updater.sleep_until(0.05)

    for i in range(10):
        updater1.call_later(0, 50_000_000)
        thread_updater.sleep_until(0.05 + 0.1 * (i + 1))

    thread_updater.stop()
    logs = thread_updater.logs
    thread_updater.logs = [l1 for l1, l2 in zip(logs, logs[1:]) if l2[1] - l1[1] >= 45_000_000]
    thread_updater.assert_logs_exact([(0, 0.1 * i + 0.05) for i in range(10)])
    del updater1


@pytest.mark.parametrize("busy_waiting", [True, False])
def test_call_soon(busy_waiting: bool) -> None:
    thread_updater = MyThreadUpdater(busy_waiting=busy_waiting)
    updater1 = thread_updater.create_updater(0, 100_000_000, allow_skips=False)
    updater1.call_soon(1)
    updater1.call_soon(2)
    updater1.call_soon(3)
    thread_updater.start()
    time.sleep(0.25)
    thread_updater.stop()
    thread_updater.assert_logs_exact([(0, 0.0)] * 4 + [(0, 0.1), (0, 0.2)])
    del updater1


@pytest.mark.parametrize("busy_waiting", [True, False])
def test_call_later(busy_waiting: bool) -> None:
    thread_updater = MyThreadUpdater(busy_waiting=busy_waiting)
    updater1 = thread_updater.create_updater(0, 100_000_000, allow_skips=False)
    updater1.call_later(1, 50_000_000)
    updater1.call_later(2, 25_000_000)
    updater1.call_later(3, 75_000_000)
    thread_updater.start()
    time.sleep(0.25)
    thread_updater.stop()
    thread_updater.assert_logs_exact([(0, 0.0), (0, 0.025), (0, 0.05), (0, 0.075), (0, 0.1), (0, 0.2)])
    del updater1


def test_weak_ref() -> None:
    thread_updater = MyThreadUpdater(busy_waiting=True)
    updater1 = thread_updater.create_updater(0, 100_000_000, allow_skips=False)
    wUpdater1 = ref(updater1)
    updater1.call_soon(1)
    updater1.call_soon(2)
    thread_updater.start()
    time.sleep(0.25)
    thread_updater.add(updater1)
    thread_updater.add(updater1)
    updater1.call_soon(3)
    gc.collect()
    assert wUpdater1() is not None
    del updater1
    gc.collect()
    assert wUpdater1() is None
    thread_updater.stop()


@pytest.mark.parametrize("busy_waiting", [True, False])
def test_intensive_updaters(busy_waiting: bool) -> None:
    thread_updater = MyThreadUpdater(busy_waiting=busy_waiting)
    updaters1 = [thread_updater.create_updater(i, 100_000_000) for i in range(50)]
    updaters2 = [thread_updater.create_updater(i, 100_000_000) for i in range(50, 100)]
    thread_updater.start()
    for u in updaters1 + updaters2:
        u.call_soon(1)
    time.sleep(0.05)
    del updaters1
    gc.collect()
    time.sleep(0.1)
    thread_updater.stop()
    thread_updater.assert_logs_exact([(i, 0.0) for i in range(100)] * 2 + [(i, 0.1) for i in range(50, 100)], e=1e-2)


@pytest.mark.parametrize("busy_waiting", [True, False])
def test_intensive_create(busy_waiting: bool) -> None:
    for _ in range(1000):
        thread_updater = MyThreadUpdater(busy_waiting=busy_waiting)
        updaters1 = [thread_updater.create_updater(i, random.randint(10_000_000, int(1e9))) for i in range(50)]
        updaters2 = [thread_updater.create_updater(i, random.randint(10_000_000, int(1e9))) for i in range(50, 100)]
        thread_updater.start()
        for u in updaters1 + updaters2:
            u.call_soon(1)
        del updaters1
        thread_updater.stop()
