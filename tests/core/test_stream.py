import gc
import time
from weakref import ref

import pytest

from llhardware.core import StreamMaster

from ..log_stream_subscriber import LogStreamSubscriber


@pytest.fixture(name="stream")
def _stream() -> StreamMaster[int]:
    return StreamMaster[int].create()


def test_simple(stream: StreamMaster[int]) -> None:
    sub1 = LogStreamSubscriber.create(stream)
    sub2 = LogStreamSubscriber.create(stream)
    stream.push(5)
    stream.push(8)
    stream.push(-12)
    sub1.assert_values([5, 8, -12])
    sub2.assert_values([5, 8, -12])
    stream.push(-13)
    sub1.assert_values([5, 8, -12, -13])
    sub2.assert_values([5, 8, -12, -13])


def test_auto_unsubscribe() -> None:
    stream1: StreamMaster[int] = StreamMaster.create()
    stream2: StreamMaster[int] = StreamMaster.create()
    sub1: LogStreamSubscriber[int] = LogStreamSubscriber.create()
    stream1.add(sub1)
    stream2.add(sub1)
    stream1.push(4)
    stream2.push(5)
    time.sleep(0.2)
    stream1.push(6)
    stream2.push(7)
    sub1.assert_logs_exact([(5, 0), (7, 0.2)])


def test_weak_ref() -> None:
    stream1: StreamMaster[int] = StreamMaster.create()
    sub1: LogStreamSubscriber[int] = LogStreamSubscriber.create(stream1)
    wSub1 = ref(sub1)
    stream1.add(sub1)
    stream1.add(sub1)
    stream1.push(123)
    gc.collect()
    assert wSub1() is not None
    del sub1
    gc.collect()
    assert wSub1() is None


def test_transform() -> None:
    stream1: StreamMaster[int] = StreamMaster.create()
    sub1: LogStreamSubscriber[int] = LogStreamSubscriber.create(stream1)
    sub2: LogStreamSubscriber[int] = LogStreamSubscriber.create(stream1 | (lambda x: 2 * x))
    stream1.push(123)
    time.sleep(0.2)
    stream1.push(321)
    sub1.assert_logs_exact([(123, 0), (321, 0.2)])
    sub2.assert_logs_exact([(123 * 2, 0), (321 * 2, 0.2)])


def test_group_transform() -> None:
    stream1: StreamMaster[int] = StreamMaster.create()
    sub1: LogStreamSubscriber[int] = LogStreamSubscriber.create(stream1)
    sub2: LogStreamSubscriber[int] = LogStreamSubscriber.create(stream1 | (lambda x: 2 * x) | (lambda x: 3 + x))
    stream1.push(123)
    time.sleep(0.2)
    stream1.push(321)
    sub1.assert_logs_exact([(123, 0), (321, 0.2)])
    sub2.assert_logs_exact([(123 * 2 + 3, 0), (321 * 2 + 3, 0.2)])


def test_get_last_value() -> None:
    stream1: StreamMaster[int] = StreamMaster.create()
    stream1.push(123)
    stream1.push(321)
    sub1: LogStreamSubscriber[int] = LogStreamSubscriber.create(stream1)
    sub2: LogStreamSubscriber[int] = LogStreamSubscriber.create(stream1, True)
    sub3: LogStreamSubscriber[int] = LogStreamSubscriber.create(stream1 | (lambda x: 2 * x) | (lambda x: 3 + x), True)
    sub1.assert_logs_exact([])
    sub2.assert_logs_exact([(321, 0)])
    sub3.assert_logs_exact([(321 * 2 + 3, 0)])


def test_closed_stream() -> None:
    stream1: StreamMaster[int] = StreamMaster.create()
    stream1.push(123)
    stream1.close()
    stream1.push(321)
    sub1: LogStreamSubscriber[int] = LogStreamSubscriber.create(stream1, True)
    sub2: LogStreamSubscriber[int] = LogStreamSubscriber.create(stream1 | (lambda x: 2 * x) | (lambda x: 3 + x), True)
    sub1.assert_logs_exact([(123, 0)])
    sub2.assert_logs_exact([(123 * 2 + 3, 0)])
