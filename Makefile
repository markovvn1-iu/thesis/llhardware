VENV ?= .venv
CODE = llhardware scripts tests

.PHONY: venv
venv:
	python -m venv $(VENV)
	#$(VENV)/bin/python -m pip install --upgrade pip
	$(VENV)/bin/python -m pip install poetry
	CMAKE_BUILD_PARALLEL_LEVEL=8 $(VENV)/bin/poetry build
	$(VENV)/bin/poetry install

.PHONY: lint
lint:
	# check package version
	$(VENV)/bin/poetry lock --check
	# python linters
	$(VENV)/bin/pflake8 --jobs 4 --statistics --show-source $(CODE)
	$(VENV)/bin/pylint --recursive=y --jobs 4 $(CODE)
	$(VENV)/bin/mypy $(CODE)
	$(VENV)/bin/black --skip-string-normalization --check $(CODE)

.PHONY: test
test:
	$(VENV)/bin/pytest -v tests

.PHONY: format
format:
	$(VENV)/bin/isort $(CODE)
	$(VENV)/bin/black $(CODE)
	$(VENV)/bin/pautoflake --recursive --remove-all-unused-imports --in-place $(CODE)

.PHONY: up
up:
	$(VENV)/bin/python -m mv_armature

.PHONY: build
build:
	[ -f setup.py ] && rm setup.py || true
	CMAKE_BUILD_PARALLEL_LEVEL=12 $(VENV)/bin/poetry build -f  wheel
	$(VENV)/bin/poetry install

.PHONY: build_arm
build_arm:
	docker buildx build --platform linux/arm64 -t poetry_arm_builder --build-arg CMAKE_BUILD_PARALLEL_LEVEL=16 --output type=local,dest=dist/ .
