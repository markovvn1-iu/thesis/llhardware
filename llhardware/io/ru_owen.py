from dataclasses import dataclass
from typing import Optional, final

from llhardware import _cpp, core, types
from llhardware.core.thread_types import CppIOUpdate

from .serial import Serial


@final
class RuOwen(CppIOUpdate):
    _base: _cpp.io.RuOwen

    @dataclass(frozen=True)
    class PacketData:
        addr: int
        read_or_write: bool
        cmd: int
        data: bytes

    @property
    def addr_len_8(self) -> bool:
        return self._base.addr_len_8

    @classmethod
    def create(cls, serial: Serial, *, addr_len_8: bool = True, timeout_ns: int = 250_000_000) -> "RuOwen":
        if not isinstance(serial, Serial):
            raise TypeError(f"serial has type '{type(serial).__name__}' when an 'Serial' is expected")
        if not isinstance(addr_len_8, bool):
            raise TypeError(f"addr_len_8 has type '{type(addr_len_8).__name__}' when an 'bool' is expected")
        if not isinstance(timeout_ns, int):
            raise TypeError(f"timeout_ns has type '{type(timeout_ns).__name__}' when an 'int' is expected")
        if timeout_ns < 1_000_000:
            raise ValueError("timeout_ns must be >= 1_000_000")
        return cls(_cpp.io.RuOwen(serial._base, addr_len_8, timeout_ns))

    @staticmethod
    def name2cmd(name: str) -> int:
        if not isinstance(name, str):
            raise TypeError(f"name has type '{type(name).__name__}' when an 'str' is expected")
        return _cpp.io.RuOwen.name2cmd(name)

    def __init__(self, base: _cpp.io.RuOwen) -> None:
        if not isinstance(base, _cpp.io.RuOwen):
            raise TypeError(f"base has type '{type(base).__name__}' when an '_cpp.io.RuOwen' is expected")
        super().__init__(base)
        self._base = base

    def make_packet(self, addr: int, read_or_write: bool, cmd: int, data: Optional[bytes] = None) -> str:
        if not isinstance(addr, int):
            raise TypeError(f"addr has type '{type(addr).__name__}' when an 'int' is expected")
        if self.addr_len_8 and (not 0 < addr < 256):
            raise ValueError("if addr_len_8 is True, addr must be in range [0, 255]")
        if not self.addr_len_8 and (not 0 < addr < 2048):
            raise ValueError("if addr_len_8 is False, addr must be in range [0, 2047]")
        if not isinstance(read_or_write, bool):
            raise TypeError(f"read_or_write has type '{type(read_or_write).__name__}' when an 'bool' is expected")
        if not isinstance(cmd, int):
            raise TypeError(f"cmd has type '{type(cmd).__name__}' when an 'int' is expected")
        if not 0 <= cmd < (2 << 16):
            raise ValueError(f"cmd must be in range [0, {(2 << 16) - 1}]")
        if data is not None:
            if not isinstance(data, bytes):
                raise TypeError(f"data has type '{type(data).__name__}' when an 'bytes' is expected")
            if len(data) > 15:
                raise ValueError("data too long (max 15 bytes)")
        return self._base.make_packet(addr, read_or_write, cmd, list(data) if data is not None else [])

    def parse_packet(self, packet: str) -> PacketData:
        if not isinstance(packet, str):
            raise TypeError(f"packet has type '{type(packet).__name__}' when an 'str' is expected")
        res = self._base.parse_packet(packet)
        return self.PacketData(res.addr, res.read_or_write, res.cmd, bytes(res.data))

    def send_packet(self, packet: str) -> core.Stream[types.StrDataItem]:
        if not isinstance(packet, str):
            raise TypeError(f"packet has type '{type(packet).__name__}' when an 'str' is expected")
        return core.Stream(self._base.send_packet(packet))
