from typing import Optional, final

from llhardware import _cpp
from llhardware.core.thread_types import CppSimpleUpdate
from llhardware.core.thread_updater import ThreadUpdaterSimple

from .gpio import GPIO
from .interfaces import IPWMFillFactor, IPWMPulseWidth


@final
class SoftPWM(CppSimpleUpdate, IPWMPulseWidth, IPWMFillFactor):
    _base: _cpp.io.SoftPWM

    @classmethod
    def create(
        cls, pin: GPIO.IDigitalWritePin, pwm_period_ns: int, thr: Optional[ThreadUpdaterSimple] = None
    ) -> "SoftPWM":
        self = cls(_cpp.io.SoftPWM(pin._base, pwm_period_ns))
        if thr is not None:
            thr.add(self)
        return self
