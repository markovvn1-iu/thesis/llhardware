from typing import Optional, final

from llhardware import _cpp, core, types
from llhardware.core.thread_types import CppIOUpdate
from llhardware.core.thread_updater import ThreadUpdaterIO

from .serial import Serial


@final
class IBus(CppIOUpdate):
    _base: _cpp.io.IBus

    @property
    def stream(self) -> core.Stream[types.IBusChannelsDataItem]:
        return core.Stream(self._base.stream)

    @classmethod
    def create(
        cls, serial: Serial, channels: int = 10, thr: Optional[ThreadUpdaterIO] = None, *, timeout_ns: int = 25_000_000
    ) -> "IBus":
        if not isinstance(serial, Serial):
            raise TypeError(f"serial has type '{type(serial).__name__}' when an 'Serial' is expected")
        if not isinstance(channels, int):
            raise TypeError(f"channels has type '{type(channels).__name__}' when an 'int' is expected")
        if not 1 <= channels <= 14:
            raise ValueError("channels must be in range [1, 14]")
        if not isinstance(timeout_ns, int):
            raise TypeError(f"timeout_ns has type '{type(timeout_ns).__name__}' when an 'int' is expected")
        if timeout_ns < 1_000_000:
            raise ValueError("timeout_ns must be >= 1_000_000")
        if (thr is not None) and (not isinstance(thr, ThreadUpdaterIO)):
            raise TypeError(f"thr has type '{type(thr).__name__}' when an 'ThreadUpdaterIO' or None is expected")

        self = cls(_cpp.io.IBus(serial._base, channels, timeout_ns))
        if thr is not None:
            thr.add(self)
        return self

    def __init__(self, base: _cpp.io.IBus) -> None:
        if not isinstance(base, _cpp.io.IBus):
            raise TypeError(f"base has type '{type(base).__name__}' when an '_cpp.io.IBus' is expected")
        super().__init__(base)
        self._base = base
