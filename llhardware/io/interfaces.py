from typing import Protocol

from llhardware import _cpp


class IPWMPulseWidth(Protocol):
    _base: _cpp.io.IPWMPulseWidth

    @property
    def pulse_width(self) -> int:
        return self._base.pulse_width

    @pulse_width.setter
    def pulse_width(self, width: int) -> None:
        if not isinstance(width, int):
            raise TypeError(f"width has type '{type(width).__name__}' when an 'int' is expected")
        if width < 0:
            raise ValueError("width must be greater of than equal to 0")
        self._base.pulse_width = width


class IPWMFillFactor(Protocol):
    _base: _cpp.io.IPWMFillFactor

    @property
    def fill_factor(self) -> float:
        return self._base.fill_factor

    @fill_factor.setter
    def fill_factor(self, factor: float) -> None:
        if not isinstance(factor, (float, int)):
            raise TypeError(f"factor has type '{type(factor).__name__}' when an 'float' is expected")
        if factor < 0 or factor > 1:
            raise ValueError("factor must be in range [0; 1]")
        self._base.fill_factor = factor
