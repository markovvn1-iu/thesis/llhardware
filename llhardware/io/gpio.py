from typing import Optional, Union, final

from typing_extensions import TypeAlias

from llhardware import _cpp, core, types
from llhardware.core.thread_types import CppIOUpdate
from llhardware.core.thread_updater import ThreadUpdaterIO


@final
class GPIO:
    GPIOMode: TypeAlias = _cpp.io.GPIO.GPIOMode
    DigitalValue: TypeAlias = _cpp.io.GPIO.DigitalValue
    PullMode: TypeAlias = _cpp.io.GPIO.PullMode
    InterruptMode: TypeAlias = _cpp.io.GPIO.InterruptMode

    class IPin:
        _base: _cpp.io.GPIO.IPin

        @property
        def pin(self) -> int:
            return self._base.pin

    class IDigitalReadPin(IPin):
        _base: _cpp.io.GPIO.IDigitalReadPin

        def digital_read(self) -> "GPIO.DigitalValue":
            return self._base.digital_read()

    class IPullUpDnControlPin(IPin):
        _base: _cpp.io.GPIO.IPullUpDnControlPin

        def pull_control(self, mode: "GPIO.PullMode") -> None:
            return self._base.pull_control(mode)

    class IDigitalInterruptPin(IPin):
        _base: _cpp.io.GPIO.IDigitalInterruptPin

        @property
        def interrupt_stream(self) -> core.Stream[types.BoolDataItem]:
            return core.Stream(self._base.interrupt_stream)

        def setup_interrupt(self, mode: "GPIO.InterruptMode") -> None:
            self._base.setup_interrupt(mode)

    class IDigitalWritePin(IPin):
        _base: _cpp.io.GPIO.IDigitalWritePin

        def digital_write(self, value: Union["GPIO.DigitalValue", int]) -> None:
            if isinstance(value, int):
                self._base.digital_write(GPIO.DigitalValue.LOW if value == 0 else GPIO.DigitalValue.HIGH)
            else:
                self._base.digital_write(value)

    class MemInputPin(IDigitalReadPin, IPullUpDnControlPin):
        _base: _cpp.io.GPIO.MemInputPin

        def __init__(self, base: _cpp.io.GPIO.MemInputPin) -> None:
            self._base = base

    class MemOutputPin(IDigitalWritePin):
        _base: _cpp.io.GPIO.MemOutputPin

        def __init__(self, base: _cpp.io.GPIO.MemOutputPin) -> None:
            self._base = base

    class SysInputPin(IDigitalReadPin, IPullUpDnControlPin, IDigitalInterruptPin, CppIOUpdate):
        _base: _cpp.io.GPIO.SysInputPin

        def __init__(self, base: _cpp.io.GPIO.SysInputPin) -> None:
            super().__init__(base)
            self._base = base

    class SysOutputPin(IDigitalWritePin):
        _base: _cpp.io.GPIO.SysOutputPin

        def __init__(self, base: _cpp.io.GPIO.SysOutputPin) -> None:
            self._base = base

    @classmethod
    def create(cls, mode: Optional["GPIO.GPIOMode"] = None) -> "GPIO":
        return cls(_cpp.io.GPIO(mode or GPIO.GPIOMode.GPIO_MODE))

    base: _cpp.io.GPIO

    def __init__(self, base: _cpp.io.GPIO) -> None:
        self._base = base

    def input(self, pin: int, *, inverse: bool = False) -> MemInputPin:
        return GPIO.MemInputPin(self._base.input(pin, inverse))

    def sys_input(self, pin: int, thr: Optional[ThreadUpdaterIO] = None, *, inverse: bool = False) -> SysInputPin:
        res = GPIO.SysInputPin(self._base.sys_input(pin, inverse))
        if thr is not None:
            thr.add(res)
        return res

    def output(self, pin: int, *, inverse: bool = False, init_value: DigitalValue = DigitalValue.LOW) -> MemOutputPin:
        return GPIO.MemOutputPin(self._base.output(pin, inverse, init_value))

    def sys_output(
        self, pin: int, *, inverse: bool = False, init_value: DigitalValue = DigitalValue.LOW
    ) -> SysOutputPin:
        return GPIO.SysOutputPin(self._base.sys_output(pin, inverse, init_value))
