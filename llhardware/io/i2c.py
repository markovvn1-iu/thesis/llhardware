from enum import Enum
from typing import List, final

from typing_extensions import TypeAlias

from llhardware import _cpp


def _check_range(value: int, name: str, mi: int, ma: int) -> None:
    if value not in range(mi, ma + 1):
        raise ValueError(f"{name} must to be in range [{mi}; {ma}]")


class I2CDevice:
    _addr: int
    __base: _cpp.io.I2CDevice

    @property
    def addr(self) -> int:
        return self._addr

    def __init__(self, base: _cpp.io.I2CDevice, addr: int) -> None:
        _check_range(addr, "Address of I2C", 1, 127)
        self.__base = base
        self._addr = addr

    def read_byte(self) -> int:
        return self.__base.read_byte()

    def write_byte(self, value: int) -> None:
        _check_range(value, "Value", 0, 255)
        self.__base.write_byte(value)

    def read_byte_data(self, command: int) -> int:
        _check_range(command, "Command", 0, 255)
        return self.__base.read_byte_data(command)

    def write_byte_data(self, command: int, value: int) -> None:
        _check_range(command, "Command", 0, 255)
        _check_range(value, "Value", 0, 255)
        self.__base.write_byte_data(command, value)

    def read_word_data(self, command: int) -> int:
        _check_range(command, "Command", 0, 255)
        return self.__base.read_word_data(command)

    def write_word_data(self, command: int, value: int) -> None:
        _check_range(command, "Command", 0, 255)
        _check_range(value, "Value", 0, 65535)
        self.__base.write_word_data(command, value)

    def read_block_data(self, command: int, length: int) -> bytes:
        _check_range(command, "Command", 0, 255)
        _check_range(length, "Length", 1, 255)
        return self.__base.read_block_data(command, length)

    def write_block_data(self, command: int, data: bytes) -> None:
        if not isinstance(data, bytes):
            raise TypeError(f"data must be of type byte, but it has type {type(data).__name__}")
        _check_range(command, "Command", 0, 255)
        _check_range(len(data), "Data length", 0, 255)
        self.__base.write_block_data(command, data)


class I2CScanMode(Enum):
    MODE_AUTO = 0
    MODE_QUICK = 1
    MODE_READ = 2


@final
class I2C:
    ScanMode: TypeAlias = I2CScanMode
    __base: _cpp.io.I2C

    @classmethod
    def create(cls, file_name: str) -> "I2C":
        return I2C(_cpp.io.I2C(file_name))

    @property
    def file_name(self) -> str:
        return self.__base.file_name

    def __init__(self, base: _cpp.io.I2C) -> None:
        self.__base = base

    def __del__(self) -> None:
        self.__base.close()

    def close(self) -> None:
        self.__base.close()

    def scan(self, first: int = 0x08, last: int = 0x77, mode: ScanMode = ScanMode.MODE_AUTO) -> List[I2CDevice]:
        _check_range(first, "Start address", 0x00, 0x7F)
        _check_range(last, "Last address", 0x00, 0x7F)
        if first > last:
            return []
        return [self.device(addr) for addr in self.__base.scan(first, last, mode.value)]

    def device(self, addr: int) -> I2CDevice:
        return I2CDevice(_cpp.io.I2CDevice(self.__base, addr), addr)
