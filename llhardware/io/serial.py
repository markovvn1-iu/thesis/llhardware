from enum import Enum
from typing import final

from llhardware import _cpp


@final
class Serial:
    _base: _cpp.io.Serial

    class Baundrate(Enum):
        B50 = 50
        B75 = 75
        B110 = 110
        B134 = 134
        B150 = 150
        B200 = 200
        B300 = 300
        B600 = 600
        B1200 = 1200
        B1800 = 1800
        B2400 = 2400
        B4800 = 4800
        B9600 = 9600
        B19200 = 19200
        B38400 = 38400
        B57600 = 57600
        B115200 = 115200
        B230400 = 230400
        B460800 = 460800
        B500000 = 500000
        B576000 = 576000
        B921600 = 921600
        B1000000 = 1000000
        B1152000 = 1152000
        B1500000 = 1500000
        B2000000 = 2000000
        B2500000 = 2500000
        B3000000 = 3000000
        B3500000 = 3500000
        B4000000 = 4000000

    @classmethod
    def create(cls, filename: str, baudrate: Baundrate) -> "Serial":
        if not isinstance(filename, str):
            raise TypeError(f"filename has type '{type(filename).__name__}' when an 'str' is expected")
        if not isinstance(baudrate, Serial.Baundrate):
            raise TypeError(f"baudrate has type '{type(baudrate).__name__}' when an 'Serial.Baundrate' is expected")
        return cls(_cpp.io.Serial(filename, baudrate.value))

    @property
    def fd(self) -> int:
        return self._base.fd

    @property
    def filename(self) -> str:
        return self._base.filename

    @property
    def baudrate(self) -> Baundrate:
        return self.Baundrate(self._base.baudrate)

    def __init__(self, base: _cpp.io.Serial) -> None:
        if not isinstance(base, _cpp.io.Serial):
            raise TypeError(f"base has type '{type(base).__name__}' when an '_cpp.io.Serial' is expected")
        self._base = base

    def read(self, nbytes: int) -> bytes:
        if not isinstance(nbytes, int):
            raise TypeError(f"nbytes has type '{type(nbytes).__name__}' when an 'int' is expected")
        return self._base.read(nbytes)

    def write(self, data: bytes) -> int:
        if not isinstance(data, bytes):
            raise TypeError(f"data has type '{type(data).__name__}' when an 'bytes' is expected")
        return self._base.write(data)

    def close(self) -> None:
        self._base.close()
