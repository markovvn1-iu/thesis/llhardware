from typing import Protocol

from llhardware import _cpp


class IStepperMotor(Protocol):
    _base: _cpp.devices.IStepperMotor

    @property
    def steps(self) -> int:
        return self._base.steps

    def move(self, steps: int) -> None:
        if not isinstance(steps, int):
            raise TypeError(f"steps has type '{type(steps).__name__}' when an 'int' is expected")
        self._base.move(steps)

    def move_to(self, target_steps: int) -> None:
        if not isinstance(target_steps, int):
            raise TypeError(f"target_steps has type '{type(target_steps).__name__}' when an 'int' is expected")
        self._base.move_to(target_steps)
