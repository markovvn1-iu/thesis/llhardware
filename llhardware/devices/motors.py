from typing import Optional, final

from llhardware import _cpp, io
from llhardware.core.thread_types import CppSimpleUpdate
from llhardware.core.thread_updater import ThreadUpdaterSimple
from llhardware.io.interfaces import IPWMFillFactor, IPWMPulseWidth

from .interfaces import IStepperMotor


@final
class DigitalServo:
    _base: _cpp.devices.DigitalServo

    @classmethod
    def create(
        cls, pwm: IPWMPulseWidth, *, pos_range: tuple[float, float], pulse_width_range: tuple[int, int]
    ) -> "DigitalServo":
        if (
            not isinstance(pos_range, tuple)
            or (len(pos_range) != 2)
            or any(not isinstance(i, (int, float)) for i in pos_range)
        ):
            raise TypeError("pos_range must be a tuple with two float")
        if (
            not isinstance(pulse_width_range, tuple)
            or (len(pulse_width_range) != 2)
            or any(not isinstance(i, int) for i in pulse_width_range)
        ):
            raise TypeError("pulse_width_range must be a tuple with two int")
        if pos_range[0] == pos_range[1]:
            raise ValueError("Start and end in pos_range must be different")
        return cls(_cpp.devices.DigitalServo(pwm._base, pos_range, pulse_width_range))

    def __init__(self, base: _cpp.devices.DigitalServo) -> None:
        self._base = base

    @property
    def position(self) -> float:
        return self._base.position

    @position.setter
    def position(self, position: float) -> None:
        self._base.position = position


@final
class AnalogServo:
    _base: _cpp.devices.AnalogServo

    @classmethod
    def create(
        cls, pwm: IPWMFillFactor, *, pos_range: tuple[float, float], fill_factor_range: tuple[float, float]
    ) -> "AnalogServo":
        if (
            not isinstance(pos_range, tuple)
            or (len(pos_range) != 2)
            or any(not isinstance(i, (int, float)) for i in pos_range)
        ):
            raise TypeError("pos_range must be a tuple with two float")
        if (
            not isinstance(fill_factor_range, tuple)
            or (len(fill_factor_range) != 2)
            or any(not isinstance(i, (int, float)) for i in fill_factor_range)
        ):
            raise TypeError("fill_factor_range must be a tuple with two float")
        if pos_range[0] == pos_range[1]:
            raise ValueError("Start and end in pos_range must be different")
        return cls(_cpp.devices.AnalogServo(pwm._base, pos_range, fill_factor_range))

    def __init__(self, base: _cpp.devices.AnalogServo) -> None:
        self._base = base

    @property
    def position(self) -> float:
        return self._base.position

    @position.setter
    def position(self, position: float) -> None:
        self._base.position = position


@final
class ElectronicSpeedController:
    _base: _cpp.devices.ElectronicSpeedController

    @classmethod
    def create(
        cls,
        pwm: IPWMPulseWidth,
        *,
        speed_range: tuple[float, float],
        pulse_width_range: tuple[int, int],
        init_speed: Optional[float] = None,
    ) -> "ElectronicSpeedController":
        if (
            not isinstance(speed_range, tuple)
            or (len(speed_range) != 2)
            or any(not isinstance(i, (int, float)) for i in speed_range)
        ):
            raise TypeError("speed_range must be a tuple with two float")
        if (
            not isinstance(pulse_width_range, tuple)
            or (len(pulse_width_range) != 2)
            or any(not isinstance(i, (int, float)) for i in pulse_width_range)
        ):
            raise TypeError("pulse_width_range must be a tuple with two float")
        if speed_range[0] == speed_range[1]:
            raise ValueError("Start and end in speed_range must be different")
        self = cls(_cpp.devices.ElectronicSpeedController(pwm._base, speed_range, pulse_width_range))

        if init_speed is not None:
            self.speed = init_speed
        return self

    def __init__(self, base: _cpp.devices.ElectronicSpeedController) -> None:
        self._base = base

    @property
    def speed(self) -> float:
        return self._base.speed

    @speed.setter
    def speed(self, speed: float) -> None:
        self._base.speed = speed


@final
class GPIOStepperMotor(CppSimpleUpdate, IStepperMotor):
    _base: _cpp.devices.GPIOStepperMotor

    @classmethod
    def create(
        cls,
        step: io.GPIO.IDigitalWritePin,
        dir_: io.GPIO.IDigitalWritePin,
        thr: Optional[ThreadUpdaterSimple] = None,
        *,
        gpio_delay_ns: int = 1000,
    ) -> "GPIOStepperMotor":
        if not isinstance(step, io.GPIO.IDigitalWritePin):
            raise TypeError(f"step has type '{type(step).__name__}' when an 'io.GPIO.IDigitalWritePin' is expected")
        if not isinstance(dir_, io.GPIO.IDigitalWritePin):
            raise TypeError(f"dir_ has type '{type(dir_).__name__}' when an 'io.GPIO.IDigitalWritePin' is expected")
        if not isinstance(gpio_delay_ns, int):
            raise TypeError(f"gpio_delay_ns has type '{type(gpio_delay_ns).__name__}' when an 'int' is expected")
        if gpio_delay_ns < 0:
            raise ValueError("gpio_delay_ns moust be >= 0")
        self = cls(_cpp.devices.GPIOStepperMotor(step._base, dir_._base, gpio_delay_ns))
        if thr is not None:
            thr.add(self)
        return self

    def __init__(self, base: _cpp.devices.GPIOStepperMotor) -> None:
        super().__init__(base)
        self._base = base
