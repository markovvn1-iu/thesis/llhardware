from abc import ABC, abstractmethod
from typing import Generic, TypeVar

from llhardware import _cpp


class BaseItem(ABC):
    _base: _cpp.core.controllers.BaseItem

    @property
    def is_attached(self) -> bool:
        return self._base.is_attached

    def detach(self) -> bool:
        return self._base.detach()

    def _on_attach(self) -> None:
        pass

    def _on_detach(self) -> None:
        pass


class CollectionItem(BaseItem):
    _base: _cpp.core.controllers.CollectionItem


class PeriodicUpdateItem(BaseItem):
    _base: _cpp.core.controllers.PeriodicUpdateItem

    def _call_at(self, id_: int, update_time_ns: int) -> None:
        if not isinstance(id_, int):
            raise TypeError(f"id_ has type '{type(id_).__name__}' when an 'int' is expected")
        if not isinstance(update_time_ns, int):
            raise TypeError(f"update_time_ns has type '{type(update_time_ns).__name__}' when an 'int' is expected")
        self._base._call_at(id_, update_time_ns)

    def _call_soon(self, id_: int) -> None:
        if not isinstance(id_, int):
            raise TypeError(f"id_ has type '{type(id_).__name__}' when an 'int' is expected")
        self._base._call_soon(id_)

    def _call_later(self, id_: int, delay_ns: int) -> None:
        if not isinstance(id_, int):
            raise TypeError(f"id_ has type '{type(id_).__name__}' when an 'int' is expected")
        if not isinstance(delay_ns, int):
            raise TypeError(f"delay_ns has type '{type(delay_ns).__name__}' when an 'int' is expected")
        self._base._call_later(id_, delay_ns)

    def _call_cancel(self, id_: int) -> None:
        if not isinstance(id_, int):
            raise TypeError(f"id_ has type '{type(id_).__name__}' when an 'int' is expected")
        self._base._call_cancel(id_)

    @abstractmethod
    def _update(self, id_: int, update_time_ns: int, cur_time_ns: int) -> int:
        pass


class FileUpdateItem(BaseItem):
    _base: _cpp.core.controllers.FileUpdateItem

    def _set_event_mask(self, fd: int, mask: int) -> None:
        if not isinstance(fd, int):
            raise TypeError(f"fd has type '{type(fd).__name__}' when an 'int' is expected")
        if not isinstance(mask, int):
            raise TypeError(f"mask has type '{type(mask).__name__}' when an 'int' is expected")
        self._base._set_event_mask(fd, mask)

    def _unset_event_mask(self, fd: int) -> None:
        if not isinstance(fd, int):
            raise TypeError(f"fd has type '{type(fd).__name__}' when an 'int' is expected")
        self._base._unset_event_mask(fd)

    @abstractmethod
    def _on_event(self, fd: int, flag: int) -> None:
        pass


DataItemT = TypeVar("DataItemT")


class StreamItem(Generic[DataItemT], BaseItem):
    _base: _cpp.core.controllers.StreamItem

    @abstractmethod
    def _on_get_data(self, item: DataItemT) -> None:
        pass
