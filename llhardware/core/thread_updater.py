from typing import Union, final

from llhardware import _cpp

from . import controllers as ct
from .thread_types import IOUpdate, SimpleUpdate


class ThreadUpdaterBase:
    _base: _cpp.core.ThreadUpdaterBase

    def start(self) -> None:
        self._base.start()

    def stop(self) -> None:
        self._base.stop()

    def has(self, obj: ct.CollectionItem) -> bool:
        return self._base.has(obj._base)

    def remove(self, obj: ct.BaseItem) -> bool:
        if not isinstance(obj, ct.BaseItem):
            raise TypeError(f"obj has type '{type(obj).__name__}' when an 'ct.BaseItem' is expected")
        return self._base.remove(obj._base)

    def clear(self) -> None:
        self._base.clear()


class ThreadUpdaterSimple:
    _base: _cpp.core.ThreadUpdaterSimple

    def add(self, obj: SimpleUpdate) -> None:
        if not isinstance(obj, SimpleUpdate):
            raise TypeError(f"obj has type '{type(obj).__name__}' when an 'SimpleUpdate' is expected")
        self._base.add(obj._base)


class ThreadUpdaterIO:
    _base: _cpp.core.ThreadUpdaterIO

    def add(self, obj: IOUpdate) -> None:
        if not isinstance(obj, IOUpdate):
            raise TypeError(f"obj has type '{type(obj).__name__}' when an 'IOUpdate' is expected")
        self._base.add(obj._base)


@final
class BusyThreadUpdater(ThreadUpdaterBase, ThreadUpdaterSimple):
    _base: _cpp.core.BusyThreadUpdater

    @classmethod
    def create(cls) -> "BusyThreadUpdater":
        return cls(_cpp.core.BusyThreadUpdater())

    def __init__(self, base: _cpp.core.BusyThreadUpdater) -> None:
        if not isinstance(base, _cpp.core.BusyThreadUpdater):
            raise TypeError(f"base has type '{type(base).__name__}' when an '_cpp.core.BusyThreadUpdater' is expected")
        self._base = base


@final
class EventThreadUpdater(ThreadUpdaterBase, ThreadUpdaterSimple, ThreadUpdaterIO):
    _base: _cpp.core.EventThreadUpdater

    @classmethod
    def create(cls) -> "EventThreadUpdater":
        return cls(_cpp.core.EventThreadUpdater())

    def __init__(self, base: _cpp.core.EventThreadUpdater) -> None:
        if not isinstance(base, _cpp.core.EventThreadUpdater):
            raise TypeError(f"base has type '{type(base).__name__}' when an '_cpp.core.EventThreadUpdater' is expected")
        self._base = base

    def add(self, obj: Union[SimpleUpdate, IOUpdate]) -> None:
        if not isinstance(obj, (SimpleUpdate, IOUpdate)):
            raise TypeError(f"obj has type '{type(obj).__name__}' when an 'SimpleUpdate' or 'IOUpdate' is expected")
        self._base.add(obj._base)
