import asyncio
from typing import Callable, Generic, Optional, TypeVar, Union, final

from loguru import logger
from typing_extensions import TypeAlias

from llhardware import _cpp
from llhardware.types import DataItem, DataTransform, LambdaDataTransform

from . import controllers as ct

DataItemT1 = TypeVar("DataItemT1")
DataItemT2 = TypeVar("DataItemT2")


DataTransformOrLambda: TypeAlias = Union[DataTransform[DataItemT1, DataItemT2], Callable[[DataItemT1], DataItemT2]]


class StreamSubscriber(ct.CollectionItem, ct.StreamItem[DataItemT1]):
    class PyStreamSubscriber(Generic[DataItemT2], _cpp.core.StreamSubscriber):
        _main: "StreamSubscriber[DataItemT2]"  # TODO: fix cyclical dependencies

        def __init__(self, main: "StreamSubscriber[DataItemT2]") -> None:
            self._main = main
            super().__init__()

        @logger.catch
        def _on_get_data(self, item: _cpp.types.DataItem) -> None:
            self._main._on_get_data(DataItem._from_cpp(item))

        @logger.catch
        def _on_attach(self) -> None:
            self._main._on_attach()

        @logger.catch
        def _on_detach(self) -> None:
            self._main._on_detach()

    _base: _cpp.core.StreamSubscriber

    def __init__(self, base: Optional[_cpp.core.StreamSubscriber] = None) -> None:
        super().__init__()
        if base is None:
            base = StreamSubscriber.PyStreamSubscriber(self)
        if not isinstance(base, _cpp.core.StreamSubscriber):
            raise TypeError(f"base has type '{type(base).__name__}' when an '_cpp.core.StreamSubscriber' is expected")
        self._base = base


class CppStreamSubscriber(StreamSubscriber[DataItemT1]):
    def _on_get_data(self, item: DataItemT1) -> None:
        self._base._on_get_data(DataItem._to_cpp(item))

    def _on_attach(self) -> None:
        self._base._on_attach()

    def _on_detach(self) -> None:
        self._base._on_detach()


@final
class LambdaStreamSubscriber(StreamSubscriber[DataItemT1]):
    __func: Optional[Callable[[DataItemT1], None]]

    @classmethod
    def create(
        cls,
        func: Callable[[DataItemT1], None],
        stream: Optional["SimpleStream[DataItemT1]"] = None,
        *,
        get_last_value: bool = False,
    ) -> "LambdaStreamSubscriber[DataItemT1]":
        self = cls(func)
        if stream is not None:
            stream.add(self, get_last_value=get_last_value)
        return self

    def __init__(self, func: Callable[[DataItemT1], None]) -> None:
        super().__init__()
        self.__func = func

    def _on_get_data(self, item: DataItemT1) -> None:
        assert self.__func is not None
        self.__func(item)


@final
class AsyncFutureStreamSubscriber(asyncio.Future[DataItemT1], StreamSubscriber[DataItemT1]):
    _base: _cpp.core.StreamSubscriber

    @classmethod
    def create(
        cls, stream: "SimpleStream[DataItemT1]", *, loop: Optional[asyncio.AbstractEventLoop] = None
    ) -> "AsyncFutureStreamSubscriber[DataItemT1]":
        self = cls(loop)
        if not stream.add(self, get_last_value=True):
            self.cancel()
        return self

    def __init__(self, loop: Optional[asyncio.AbstractEventLoop]) -> None:
        asyncio.Future.__init__(self, loop=loop)  # pylint: disable=non-parent-init-called
        StreamSubscriber.__init__(self)

    def _on_detach(self) -> None:
        self._loop.call_soon_threadsafe(self.cancel)

    def _on_get_data(self, item: DataItemT1) -> None:
        self._loop.call_soon_threadsafe(lambda: self.set_result(item))


class SimpleStream(Generic[DataItemT1]):
    _base: _cpp.core.SimpleStream

    def __init__(self, base: _cpp.core.SimpleStream) -> None:
        self._base = base

    def add(self, sub: StreamSubscriber[DataItemT1], *, get_last_value: bool = False) -> bool:
        return self._base.add(sub._base, get_last_value)

    def transform(self, transform: DataTransformOrLambda[DataItemT1, DataItemT2]) -> "SimpleStream[DataItemT2]":
        if isinstance(transform, DataTransform):
            return SimpleStream(self._base.transform(transform._base))
        return SimpleStream(self._base.transform(LambdaDataTransform.create(transform)._base))

    def __or__(self, transform: DataTransformOrLambda[DataItemT1, DataItemT2]) -> "SimpleStream[DataItemT2]":
        return self.transform(transform)


class Stream(SimpleStream[DataItemT1]):
    _base: _cpp.core.Stream

    @property
    def last_item(self) -> Optional[DataItemT1]:
        return DataItem._from_cpp(self._base.last_item)

    def __init__(self, base: _cpp.core.Stream) -> None:
        super().__init__(base)
        self._base = base

    def has(self, sub: ct.CollectionItem) -> None:
        return self._base.has(sub._base)

    def remove(self, sub: ct.BaseItem) -> bool:
        return self._base.remove(sub._base)


@final
class StreamMaster(Stream[DataItemT1]):
    _base: _cpp.core.StreamMaster

    @classmethod
    def create(cls) -> "StreamMaster[DataItemT1]":
        return cls(_cpp.core.StreamMaster())

    def __init__(self, base: _cpp.core.StreamMaster) -> None:
        super().__init__(base)

    def push(self, item: DataItemT1) -> None:
        self._base.push(item._base if isinstance(item, DataItem) else _cpp.types.PyDataItem(item))

    def close(self) -> None:
        self._base.close()
