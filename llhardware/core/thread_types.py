from typing import Callable, Optional, final

from loguru import logger

from llhardware import _cpp

from . import controllers as ct


class SimpleUpdate(ct.CollectionItem, ct.PeriodicUpdateItem):
    class PySimpleUpdate(_cpp.core.SimpleUpdate):
        _main: "SimpleUpdate"  # TODO: fix cyclical dependencies

        def __init__(self, main: "SimpleUpdate") -> None:
            self._main = main
            super().__init__()

        def __del__(self) -> None:
            self.detach()

        @logger.catch(default=-1)
        def _update(self, id_: int, update_time_ns: int, cur_time_ns: int) -> int:
            res = self._main._update(id_, update_time_ns, cur_time_ns)
            if not isinstance(res, int):
                raise TypeError(
                    f"return value of _update method has type '{type(res).__name__}' when an 'int' is expected"
                )
            return res

        @logger.catch
        def _on_attach(self) -> None:
            self._main._on_attach()

        @logger.catch
        def _on_detach(self) -> None:
            self._main._on_detach()

    _base: _cpp.core.SimpleUpdate

    def __init__(self, base: Optional[_cpp.core.SimpleUpdate] = None) -> None:
        super().__init__()
        if base is None:
            base = SimpleUpdate.PySimpleUpdate(self)
        if not isinstance(base, _cpp.core.SimpleUpdate):
            raise TypeError(f"base has type '{type(base).__name__}' when an '_cpp.core.SimpleUpdate' is expected")
        self._base = base


class IOUpdate(ct.CollectionItem, ct.PeriodicUpdateItem, ct.FileUpdateItem):
    class PyIOUpdate(_cpp.core.IOUpdate):
        _main: "IOUpdate"  # TODO: fix cyclical dependencies

        def __init__(self, main: "IOUpdate") -> None:
            self._main = main
            super().__init__()

        def __del__(self) -> None:
            self.detach()

        @logger.catch()
        def _on_event(self, fd: int, flag: int) -> None:
            self._main._on_event(fd, flag)

        @logger.catch(default=-1)
        def _update(self, id_: int, update_time_ns: int, cur_time_ns: int) -> int:
            res = self._main._update(id_, update_time_ns, cur_time_ns)
            if not isinstance(res, int):
                raise TypeError(
                    f"return value of _update method has type '{type(res).__name__}' when an 'int' is expected"
                )
            return res

        @logger.catch
        def _on_attach(self) -> None:
            self._main._on_attach()

        @logger.catch
        def _on_detach(self) -> None:
            self._main._on_detach()

    _base: _cpp.core.IOUpdate

    def __init__(self, base: Optional[_cpp.core.IOUpdate] = None) -> None:
        super().__init__()
        if base is None:
            base = IOUpdate.PyIOUpdate(self)
        if not isinstance(base, _cpp.core.IOUpdate):
            raise TypeError(f"base has type '{type(base).__name__}' when an '_cpp.core.IOUpdate' is expected")
        self._base = base

    def _update(self, id_: int, _update_time_ns: int, _cur_time_ns: int) -> int:
        return 0


class CppSimpleUpdate(SimpleUpdate):
    def _update(self, id_: int, update_time_ns: int, cur_time_ns: int) -> int:
        if not isinstance(id_, int):
            raise TypeError(f"id_ has type '{type(id_).__name__}' when an 'int' is expected")
        if not isinstance(update_time_ns, int):
            raise TypeError(f"update_time_ns has type '{type(update_time_ns).__name__}' when an 'int' is expected")
        if not isinstance(cur_time_ns, int):
            raise TypeError(f"cur_time_ns has type '{type(cur_time_ns).__name__}' when an 'int' is expected")
        return self._base._update(id_, update_time_ns, cur_time_ns)

    def _on_attach(self) -> None:
        self._base._on_attach()

    def _on_detach(self) -> None:
        self._base._on_detach()


class CppIOUpdate(IOUpdate):
    def _update(self, id_: int, update_time_ns: int, cur_time_ns: int) -> int:
        if not isinstance(id_, int):
            raise TypeError(f"id_ has type '{type(id_).__name__}' when an 'int' is expected")
        if not isinstance(update_time_ns, int):
            raise TypeError(f"update_time_ns has type '{type(update_time_ns).__name__}' when an 'int' is expected")
        if not isinstance(cur_time_ns, int):
            raise TypeError(f"cur_time_ns has type '{type(cur_time_ns).__name__}' when an 'int' is expected")
        return self._base._update(id_, update_time_ns, cur_time_ns)

    def _on_event(self, fd: int, flag: int) -> None:
        if not isinstance(fd, int):
            raise TypeError(f"fd has type '{type(fd).__name__}' when an 'int' is expected")
        if not isinstance(flag, int):
            raise TypeError(f"flag has type '{type(flag).__name__}' when an 'int' is expected")
        return self._base._on_event(fd, flag)

    def _on_attach(self) -> None:
        self._base._on_attach()

    def _on_detach(self) -> None:
        self._base._on_detach()


@final
class LambdaSimpleUpdate(SimpleUpdate):
    __func: Optional[Callable[[int, int], None]]
    update_delay_ns: int
    allow_skips: bool

    @classmethod
    def create(
        cls, func: Callable[[int, int], None], update_delay_ns: int, allow_skips: bool = True
    ) -> "LambdaSimpleUpdate":
        return cls(func, update_delay_ns, allow_skips)

    def __init__(self, func: Callable[[int, int], None], update_delay_ns: int, allow_skips: bool = True) -> None:
        super().__init__()
        self.__func = func
        self.update_delay_ns = update_delay_ns
        self.allow_skips = allow_skips

    def _on_attach(self) -> None:
        self._call_soon(0)

    def _update(self, id_: int, update_time_ns: int, cur_time_ns: int) -> int:
        assert self.__func is not None
        self.__func(id_, cur_time_ns)
        if id_ != 0:
            return 0
        if self.update_delay_ns == 0:
            return cur_time_ns
        if self.allow_skips:
            return max(update_time_ns + self.update_delay_ns, cur_time_ns)
        return update_time_ns + self.update_delay_ns

    def call_at(self, id_: int, update_time_ns: int) -> None:
        self._call_at(id_, update_time_ns)

    def call_soon(self, id_: int) -> None:
        self._call_soon(id_)

    def call_later(self, id_: int, delay_ns: int) -> None:
        self._call_later(id_, delay_ns)

    def call_cancel(self, id_: int) -> None:
        self._call_cancel(id_)
