from typing import List, Tuple, final

from llhardware import _cpp

from .interfaces import IFrozenStepperMotorPlan


class Plan:
    _base: _cpp.planners.Plan

    def __init__(self, base: _cpp.planners.Plan) -> None:
        self._base = base

    def freeze(self, start_time_ns: int, start_pos: Tuple[int, float], warmup_k: float) -> IFrozenStepperMotorPlan:
        return IFrozenStepperMotorPlan(self._base.freeze(start_time_ns, start_pos, warmup_k))


class FrozenStepperMotorSimplePlan(IFrozenStepperMotorPlan):
    _base: _cpp.planners.FrozenStepperMotorSimplePlan

    @property
    def start_vel(self) -> float:
        return self._base.start_vel

    @property
    def end_vel(self) -> float:
        return self._base.end_vel

    def __init__(self, base: _cpp.planners.FrozenStepperMotorSimplePlan) -> None:
        super().__init__(base)
        self._base = base


@final
class SimplePlan(Plan):
    _base: _cpp.planners.SimplePlan

    @property
    def start_vel(self) -> float:
        return self._base.start_vel

    @property
    def end_vel(self) -> float:
        return self._base.end_vel

    @property
    def duration(self) -> float:
        return self._base.duration

    @classmethod
    def create(cls, start_vel: float, end_vel: float, duration: float) -> "SimplePlan":
        return cls(_cpp.planners.SimplePlan(start_vel, end_vel, duration))

    def __init__(self, base: _cpp.planners.SimplePlan) -> None:
        super().__init__(base)
        self._base = base

    def freeze(self, start_time_ns: int, start_pos: Tuple[int, float], warmup_k: float) -> FrozenStepperMotorSimplePlan:
        return FrozenStepperMotorSimplePlan(self._base.freeze(start_time_ns, start_pos, warmup_k))


class FrozenStepperMotorGroupPlan(IFrozenStepperMotorPlan):
    _base: _cpp.planners.FrozenStepperMotorGroupPlan

    def __init__(self, base: _cpp.planners.FrozenStepperMotorGroupPlan) -> None:
        super().__init__(base)
        self._base = base


@final
class GroupPlan(Plan):
    _base: _cpp.planners.GroupPlan

    @property
    def plans(self) -> List[Plan]:
        return [Plan(p) for p in self._base.plans]

    @classmethod
    def create(cls, plans: List[Plan]) -> "GroupPlan":
        return cls(_cpp.planners.GroupPlan([p._base for p in plans]))

    def __init__(self, base: _cpp.planners.GroupPlan) -> None:
        super().__init__(base)
        self._base = base

    def freeze(self, start_time_ns: int, start_pos: Tuple[int, float], warmup_k: float) -> FrozenStepperMotorGroupPlan:
        return FrozenStepperMotorGroupPlan(self._base.freeze(start_time_ns, start_pos, warmup_k))
