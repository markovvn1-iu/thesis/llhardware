from typing import Tuple

from llhardware import _cpp


class ICompiledStepperMotorPlan:
    _base: _cpp.planners.ICompiledStepperMotorPlan

    @property
    def start_time_ns(self) -> int:
        return self._base.start_time_ns

    @property
    def end_time_ns(self) -> int:
        return self._base.end_time_ns

    @property
    def start_pos(self) -> Tuple[int, float]:
        return self._base.start_pos

    @property
    def end_pos(self) -> Tuple[int, float]:
        return self._base.end_pos

    def __init__(self, base: _cpp.planners.ICompiledStepperMotorPlan) -> None:
        self._base = base

    def get_pos(self, cur_time_ns: int) -> Tuple[int, float]:
        if not isinstance(cur_time_ns, int):
            raise TypeError(f"cur_time_ns has type '{type(cur_time_ns).__name__}' when an 'int' is expected")
        if cur_time_ns < 0:
            raise ValueError("cur_time_ns must be greater of than equal to 0")
        return self._base.get_pos(cur_time_ns)

    def compute(self, cur_time_ns: int) -> Tuple[int, int]:
        if not isinstance(cur_time_ns, int):
            raise TypeError(f"cur_time_ns has type '{type(cur_time_ns).__name__}' when an 'int' is expected")
        if cur_time_ns < 0:
            raise ValueError("cur_time_ns must be greater of than equal to 0")
        return self._base.compute(cur_time_ns)


class IFrozenStepperMotorPlan:
    _base: _cpp.planners.IFrozenStepperMotorPlan

    @property
    def start_time_ns(self) -> int:
        return self._base.start_time_ns

    @property
    def end_time_ns(self) -> int:
        return self._base.end_time_ns

    @property
    def start_pos(self) -> Tuple[int, float]:
        return self._base.start_pos

    @property
    def end_pos(self) -> Tuple[int, float]:
        return self._base.end_pos

    def __init__(self, base: _cpp.planners.IFrozenStepperMotorPlan) -> None:
        self._base = base

    def get_pos(self, cur_time_ns: int) -> Tuple[int, float]:
        if not isinstance(cur_time_ns, int):
            raise TypeError(f"cur_time_ns has type '{type(cur_time_ns).__name__}' when an 'int' is expected")
        if cur_time_ns < 0:
            raise ValueError("cur_time_ns must be greater of than equal to 0")
        return self._base.get_pos(cur_time_ns)

    def compile(self, cur_time_ns: int, cur_pos: Tuple[int, float]) -> ICompiledStepperMotorPlan:
        if not isinstance(cur_time_ns, int):
            raise TypeError(f"cur_time_ns has type '{type(cur_time_ns).__name__}' when an 'int' is expected")
        if cur_time_ns < 0:
            raise ValueError("cur_time_ns must be greater of than equal to 0")
        return ICompiledStepperMotorPlan(self._base.compile(cur_time_ns, cur_pos))
