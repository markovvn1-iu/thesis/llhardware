from dataclasses import dataclass
from typing import Optional, Tuple, final

from llhardware import _cpp
from llhardware.core.thread_types import CppSimpleUpdate
from llhardware.core.thread_updater import ThreadUpdaterSimple
from llhardware.devices.interfaces import IStepperMotor
from llhardware.planners.interfaces import IFrozenStepperMotorPlan


@final
class StepperMotorPlanController(CppSimpleUpdate):
    _base: _cpp.controllers.StepperMotorPlanController

    @dataclass
    class State:
        cur_time: int
        pos: Tuple[int, float]

    @classmethod
    def create(cls, motor: IStepperMotor, thr: Optional[ThreadUpdaterSimple] = None) -> "StepperMotorPlanController":
        self = cls(_cpp.controllers.StepperMotorPlanController(motor._base))
        if thr is not None:
            thr.add(self)
        return self

    @property
    def cur_state(self) -> State:
        return self.State(*self._base.cur_state)

    def __init__(self, base: _cpp.controllers.StepperMotorPlanController) -> None:
        super().__init__(base)
        self._base = base

    def set_plan(self, plan: IFrozenStepperMotorPlan) -> None:
        self._base.set_plan(plan._base)
