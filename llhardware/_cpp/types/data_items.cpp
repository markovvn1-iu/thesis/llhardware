#include "data_items.hpp"

using namespace llhardware::type;

PDataTransform BoolDataItem::get_value() {
    return std::make_shared<LambdaDataTransform>([](PDataItem item){
        auto res = from(item);
        return PyDataItem::create(pybind11::bool_(res->value));
    });
}

PDataTransform IntDataItem::get_value() {
    return std::make_shared<LambdaDataTransform>([](PDataItem item){
        auto res = from(item);
        return PyDataItem::create(pybind11::int_(res->value));
    });
}

PDataTransform FloatDataItem::get_value() {
    return std::make_shared<LambdaDataTransform>([](PDataItem item){
        auto res = from(item);
        return PyDataItem::create(pybind11::float_(res->value));
    });
}

PDataTransform StrDataItem::get_value() {
    return std::make_shared<LambdaDataTransform>([](PDataItem item){
        auto res = from(item);
        return PyDataItem::create(pybind11::str(res->value));
    });
}

PDataTransform Vec3FloatDataItem::get_x() {
    return std::make_shared<LambdaDataTransform>([](PDataItem item){
        auto res = from(item);
        if (res->mask & 0b001) return FloatDataItem::create(res->x);
        return std::shared_ptr<DataItem>(NULL);
    });
}

PDataTransform Vec3FloatDataItem::get_y() {
    return std::make_shared<LambdaDataTransform>([](PDataItem item){
        auto res = from(item);
        if (res->mask & 0b010) return FloatDataItem::create(res->y);
        return std::shared_ptr<DataItem>(NULL);
    });
}

PDataTransform Vec3FloatDataItem::get_z() {
    return std::make_shared<LambdaDataTransform>([](PDataItem item){
        auto res = from(item);
        if (res->mask & 0b100) return FloatDataItem::create(res->z);
        return std::shared_ptr<DataItem>(NULL);
    });
}

PDataTransform GyroAccelTempDataItem::get_gyro() {
    return std::make_shared<LambdaDataTransform>([](PDataItem item){
        auto res = from(item);
        if (res->mask & 0b0000111) return Vec3FloatDataItem::create(res->gx, res->gy, res->gz, res->mask & 0b111);
        return std::shared_ptr<DataItem>(NULL);
    });
}

PDataTransform GyroAccelTempDataItem::get_accel() {
    return std::make_shared<LambdaDataTransform>([](PDataItem item){
        auto res = from(item);
        if (res->mask & 0b0111000) return Vec3FloatDataItem::create(res->ax, res->ay, res->az, (res->mask >> 3) & 0b111);
        return std::shared_ptr<DataItem>(NULL);
    });
}

PDataTransform GyroAccelTempDataItem::get_temp() {
    return std::make_shared<LambdaDataTransform>([](PDataItem item){
        auto res = from(item);
        if (res->mask & 0b1000000) return FloatDataItem::create(res->temp);
        return std::shared_ptr<DataItem>(NULL);
    });
}

PDataTransform IBusChannelsDataItem::get_channel(uint32_t channel) {
    return std::make_shared<LambdaDataTransform>([channel](PDataItem item){
        auto res = from(item);
        if (channel < res->value.size()) return IntDataItem::create(res->value[channel]);
        return std::shared_ptr<DataItem>(NULL);
    });
}

PDataTransform IBusChannelsDataItem::get_is_connected() {
    return std::make_shared<LambdaDataTransform>([](PDataItem item){
        auto res = from(item);
        return BoolDataItem::create(res->is_connected);
    });
}