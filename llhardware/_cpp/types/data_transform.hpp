#pragma once

#include <pybind11/pybind11.h>

#include <vector>

#include "data_item_base.hpp"


namespace llhardware::type {

class DataTransform {
public:
    virtual PDataItem transform(PDataItem item) = 0;
};

using PDataTransform = std::shared_ptr<DataTransform>;

class LambdaDataTransform : public DataTransform {
private:
    std::function<PDataItem(PDataItem)> func;

public:
    LambdaDataTransform(std::function<PDataItem(PDataItem)> func) : func(func) {}
    PDataItem transform(PDataItem item) override { return func(item); }
};

class GroupDataTransform : public DataTransform {
private:
    std::vector<PDataTransform> transforms;

public:
    GroupDataTransform(std::vector<PDataTransform> transforms) : transforms(transforms) {}
    PDataItem transform(PDataItem item) override;
};

}  // namespace llhardware::type