#pragma once

#include <pybind11/pybind11.h>

namespace llhardware::type {
    void init_module(pybind11::module &m);
}; // namespace llhardware::type
