#pragma once

#include <pybind11/pybind11.h>


namespace llhardware::type {

struct DataItem;
using PDataItem = std::shared_ptr<DataItem>;

struct DataItem {
private:
    const int _type;
    const char* const _type_name;

protected:
    DataItem(int type, const char* type_name): _type(type), _type_name(type_name) {}

    template<typename T>
    static std::shared_ptr<T> cast(PDataItem item) {
        std::shared_ptr<T> res = std::dynamic_pointer_cast<T>(item);
        if (!res) throw std::runtime_error("Failed to convert to " + std::string(T::class_type_name()) + ". Object has type " + std::string(item->type_name()));
        return res;
    }

public:
    virtual ~DataItem() {}
    int type() const { return _type; }
    const char* type_name() const { return _type_name; }
};

}  // namespace llhardware::type