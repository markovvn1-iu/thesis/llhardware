#pragma once

#include <pybind11/pybind11.h>

#include "data_item_base.hpp"
#include "data_transform.hpp"


namespace llhardware::type {

enum DataItemType {
    PYTHON_OBJECT = 0,
    BOOL,
    INT,
    FLOAT,
    STRING,
    VEC3_FLOAT,
    GYRO_ACCEL_TEMP,
    IBUS_CHANNELS
};

struct PyDataItem : public DataItem {
    const pybind11::object value;

    static DataItemType class_type() { return DataItemType::PYTHON_OBJECT; };
    static const char* class_type_name() { return "PyDataItem"; };
    inline static std::shared_ptr<PyDataItem> from(PDataItem item) { return cast<PyDataItem>(item); }
    template<class... T> static std::shared_ptr<DataItem> create(T&&... t) { return std::static_pointer_cast<DataItem>(std::make_shared<PyDataItem>(std::forward<T>(t)...)); }

    PyDataItem(pybind11::object value) : DataItem(class_type(), class_type_name()), value(value) {}
};

struct BoolDataItem : public DataItem {
    const bool value;

    static DataItemType class_type() { return DataItemType::BOOL; };
    static const char* class_type_name() { return "BoolDataItem"; };
    inline static std::shared_ptr<BoolDataItem> from(PDataItem item) { return cast<BoolDataItem>(item); }
    template<class... T> static std::shared_ptr<DataItem> create(T&&... t) { return std::static_pointer_cast<DataItem>(std::make_shared<BoolDataItem>(std::forward<T>(t)...)); }

    BoolDataItem(bool value) : DataItem(class_type(), class_type_name()), value(value) {}

    static PDataTransform get_value();
};

struct IntDataItem : public DataItem {
    const int32_t value;

    static DataItemType class_type() { return DataItemType::INT; };
    static const char* class_type_name() { return "IntDataItem"; };
    inline static std::shared_ptr<IntDataItem> from(PDataItem item) { return cast<IntDataItem>(item); }
    template<class... T> static std::shared_ptr<DataItem> create(T&&... t) { return std::static_pointer_cast<DataItem>(std::make_shared<IntDataItem>(std::forward<T>(t)...)); }

    IntDataItem(int32_t value) : DataItem(class_type(), class_type_name()), value(value) {}

    static PDataTransform get_value();
};

struct FloatDataItem : public DataItem {
    const double value;

    static DataItemType class_type() { return DataItemType::FLOAT; };
    static const char* class_type_name() { return "FloatDataItem"; };
    inline static std::shared_ptr<FloatDataItem> from(PDataItem item) { return cast<FloatDataItem>(item); }
    template<class... T> static std::shared_ptr<DataItem> create(T&&... t) { return std::static_pointer_cast<DataItem>(std::make_shared<FloatDataItem>(std::forward<T>(t)...)); }

    FloatDataItem(double value) : DataItem(class_type(), class_type_name()), value(value) {}

    static PDataTransform get_value();
};

struct StrDataItem : public DataItem {
    const std::string value;

    static DataItemType class_type() { return DataItemType::STRING; };
    static const char* class_type_name() { return "StrDataItem"; };
    inline static std::shared_ptr<StrDataItem> from(PDataItem item) { return cast<StrDataItem>(item); }
    template<class... T> static std::shared_ptr<DataItem> create(T&&... t) { return std::static_pointer_cast<DataItem>(std::make_shared<StrDataItem>(std::forward<T>(t)...)); }

    StrDataItem(std::string value) : DataItem(class_type(), class_type_name()), value(value) {}

    static PDataTransform get_value();
};

struct Vec3FloatDataItem : public DataItem {
friend void init_module(pybind11::module &m);
private:
    std::tuple<double, double, double, int> py_value() { return {x, y, z, mask}; }

public:
    const double x, y, z;
    const int mask;

    static DataItemType class_type() { return DataItemType::VEC3_FLOAT; };
    static const char* class_type_name() { return "Vec3FloatDataItem"; };
    inline static std::shared_ptr<Vec3FloatDataItem> from(PDataItem item) { return cast<Vec3FloatDataItem>(item); }
    template<class... T> static std::shared_ptr<DataItem> create(T&&... t) { return std::static_pointer_cast<DataItem>(std::make_shared<Vec3FloatDataItem>(std::forward<T>(t)...)); }

    Vec3FloatDataItem(double x, double y, double z, int mask)
        : DataItem(class_type(), class_type_name()), x(x), y(y), z(z), mask(mask) {}

    static PDataTransform get_x();
    static PDataTransform get_y();
    static PDataTransform get_z();
};

struct GyroAccelTempDataItem : public DataItem {
friend void init_module(pybind11::module &m);
private:
    std::tuple<double, double, double, double, double, double, double, int> py_value() { return {gx, gy, gz, ax, ay, az, temp, mask}; }

public:
    const double gx, gy, gz;
    const double ax, ay, az;
    const double temp;
    const int mask;

    static DataItemType class_type() { return DataItemType::GYRO_ACCEL_TEMP; };
    static const char* class_type_name() { return "GyroAccelTempDataItem"; };
    inline static std::shared_ptr<GyroAccelTempDataItem> from(PDataItem item) { return cast<GyroAccelTempDataItem>(item); }
    template<class... T> static std::shared_ptr<DataItem> create(T&&... t) { return std::static_pointer_cast<DataItem>(std::make_shared<GyroAccelTempDataItem>(std::forward<T>(t)...)); }

    GyroAccelTempDataItem(double gx, double gy, double gz, double ax, double ay, double az, double temp, int mask)
        : DataItem(class_type(), class_type_name()), gx(gx), gy(gy), gz(gz), ax(ax), ay(ay), az(az), temp(temp), mask(mask) {}

    static PDataTransform get_gyro();
    static PDataTransform get_accel();
    static PDataTransform get_temp();
};

struct IBusChannelsDataItem : public DataItem {
friend void init_module(pybind11::module &m);
public:
    const std::vector<uint16_t> value;
    const bool is_connected;

    static DataItemType class_type() { return DataItemType::IBUS_CHANNELS; };
    static const char* class_type_name() { return "IBusChannelsDataItem"; };
    inline static std::shared_ptr<IBusChannelsDataItem> from(PDataItem item) { return cast<IBusChannelsDataItem>(item); }
    template<class... T> static std::shared_ptr<DataItem> create(T&&... t) { return std::static_pointer_cast<DataItem>(std::make_shared<IBusChannelsDataItem>(std::forward<T>(t)...)); }

    IBusChannelsDataItem(const std::vector<uint16_t>& value, bool is_connected)
        : DataItem(class_type(), class_type_name()), value(value), is_connected(is_connected) {}

    static PDataTransform get_channel(uint32_t channel);
    static PDataTransform get_is_connected();
};


}  // namespace llhardware::type