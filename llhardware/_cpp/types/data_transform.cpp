#include "data_transform.hpp"


using namespace llhardware::type;

PDataItem GroupDataTransform::transform(PDataItem item) {
    for (auto it = transforms.begin(); item && (it != transforms.end()); it++) item = (*it)->transform(item);
    return item;
}
