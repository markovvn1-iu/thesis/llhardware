#include <pybind11/pybind11.h>
#include <pybind11/stl.h>
#include <pybind11/functional.h>

#include "types.hpp"
#include "module.hpp"

namespace py = pybind11;
namespace llhardware::type {

class PyDataTransform : public DataTransform {
    /* Inherit the constructors */
    using DataTransform::DataTransform;

    type::PDataItem transform(type::PDataItem item) override {
        PYBIND11_OVERRIDE_PURE(
            type::PDataItem,   /* Return type */
            DataTransform,     /* Parent class */
            transform,         /* Name of function in C++ */
            item               /* Argument(s) */
        );
    }
};

void init_module(py::module &m) {
    py::enum_<DataItemType>(m, "DataItemType")
        .value("PYTHON_OBJECT", DataItemType::PYTHON_OBJECT)
        .value("BOOL", DataItemType::BOOL)
        .value("INT", DataItemType::INT)
        .value("FLOAT", DataItemType::FLOAT)
        .value("STRING", DataItemType::STRING)
        .value("VEC3_FLOAT", DataItemType::VEC3_FLOAT)
        .value("GYRO_ACCEL_TEMP", DataItemType::GYRO_ACCEL_TEMP)
        .value("IBUS_CHANNELS", DataItemType::IBUS_CHANNELS)
        .export_values();

    py::class_<DataItem, PDataItem>(m, "DataItem")
        .def_property_readonly("type", &DataItem::type);
    
    py::class_<DataTransform, PDataTransform, PyDataTransform>(m, "DataTransform")
        .def(py::init<>())
        .def("transform", &DataTransform::transform);
    
    py::class_<LambdaDataTransform, std::shared_ptr<LambdaDataTransform>, DataTransform>(m, "LambdaDataTransform")
        .def(py::init<std::function<type::PDataItem(type::PDataItem)>>());
    
    py::class_<GroupDataTransform, std::shared_ptr<GroupDataTransform>, DataTransform>(m, "GroupDataTransform")
        .def(py::init<std::vector<PDataTransform>>());

    py::class_<PyDataItem, std::shared_ptr<PyDataItem>, DataItem>(m, "PyDataItem")
        .def(py::init<py::object>())
        .def_readonly("value", &PyDataItem::value);

    py::class_<BoolDataItem, std::shared_ptr<BoolDataItem>, DataItem>(m, "BoolDataItem")
        .def(py::init<bool>())
        .def_readonly("value", &BoolDataItem::value)
        .def_static("get_value", &FloatDataItem::get_value);

    py::class_<IntDataItem, std::shared_ptr<IntDataItem>, DataItem>(m, "IntDataItem")
        .def(py::init<int32_t>())
        .def_readonly("value", &IntDataItem::value)
        .def_static("get_value", &FloatDataItem::get_value);

    py::class_<FloatDataItem, std::shared_ptr<FloatDataItem>, DataItem>(m, "FloatDataItem")
        .def(py::init<double>())
        .def_readonly("value", &FloatDataItem::value)
        .def_static("get_value", &FloatDataItem::get_value);

    py::class_<StrDataItem, std::shared_ptr<StrDataItem>, DataItem>(m, "StrDataItem")
        .def(py::init<std::string>())
        .def_readonly("value", &StrDataItem::value)
        .def_static("get_value", &StrDataItem::get_value);

    py::class_<Vec3FloatDataItem, std::shared_ptr<Vec3FloatDataItem>, DataItem>(m, "Vec3FloatDataItem")
        .def(py::init<double, double, double, int>())
        .def_property_readonly("value", &Vec3FloatDataItem::py_value)
        .def_static("get_x", &Vec3FloatDataItem::get_x)
        .def_static("get_y", &Vec3FloatDataItem::get_y)
        .def_static("get_z", &Vec3FloatDataItem::get_z);

    py::class_<GyroAccelTempDataItem, std::shared_ptr<GyroAccelTempDataItem>, DataItem>(m, "GyroAccelTempDataItem")
        .def(py::init<double, double, double, double, double, double, double, int>())
        .def_property_readonly("value", &GyroAccelTempDataItem::py_value)
        .def_static("get_gyro", &GyroAccelTempDataItem::get_gyro)
        .def_static("get_accel", &GyroAccelTempDataItem::get_accel)
        .def_static("get_temp", &GyroAccelTempDataItem::get_temp);

    py::class_<IBusChannelsDataItem, std::shared_ptr<IBusChannelsDataItem>, DataItem>(m, "IBusChannelsDataItem")
        .def(py::init<std::vector<uint16_t>&, bool>())
        .def_readonly("value", &IBusChannelsDataItem::value)
        .def_readonly("is_connected", &IBusChannelsDataItem::is_connected)
        .def_static("get_channel", &IBusChannelsDataItem::get_channel)
        .def_static("get_is_connected", &IBusChannelsDataItem::get_is_connected);
}

};  // namespace llhardware::io
