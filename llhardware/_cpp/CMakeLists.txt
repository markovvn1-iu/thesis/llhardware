cmake_minimum_required(VERSION 3.15)
project(_cpp)

set(CMAKE_CXX_STANDARD 17)
set(CMAKE_CXX_FLAGS_RELEASE "-O3")

find_package(PythonInterp REQUIRED)
find_package(pybind11 REQUIRED)

file(GLOB_RECURSE TYPES_SRC "types/*.cpp")
file(GLOB_RECURSE CORE_SRC "core/*.cpp")
file(GLOB_RECURSE IO_SRC "io/*.cpp")
file(GLOB_RECURSE DEVICES_SRC "devices/*.cpp")
file(GLOB_RECURSE CONTROLLERS_SRC "controllers/*.cpp")
file(GLOB_RECURSE PLANNERS_SRC "planners/*.cpp")

pybind11_add_module(${PROJECT_NAME} "module.cpp" ${CORE_SRC} ${IO_SRC} ${DEVICES_SRC} ${TYPES_SRC} ${CONTROLLERS_SRC} ${PLANNERS_SRC})
target_include_directories(${PROJECT_NAME} PRIVATE "${CMAKE_CURRENT_SOURCE_DIR}")
