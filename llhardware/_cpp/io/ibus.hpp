#pragma once

#include <vector>

#include "core/core.hpp"
#include "serial.hpp"

namespace llhardware::io {

// The FlySky iBus protocol
class IBus : public core::IOUpdate {
private:
    static const uint8_t PROTOCOL_LENGTH = 0x20;  // <len><cmd><data....><chkl><chkh>
    static const uint8_t PROTOCOL_COMMAND = 0x40; // Command is always 0x40

    uint8_t channels;
    uint64_t timeout_ns;

    uint8_t buf[PROTOCOL_LENGTH];
    uint8_t buf_i = 0; // 0 - len, 1 - cmd, 2..29 - data, 30..31 - chk

    const std::shared_ptr<core::StreamMaster> stream;

protected:
    std::shared_ptr<Serial> serial;

    void on_attach() override;
    uint64_t update(int id, uint64_t update_time_ns, uint64_t cur_time_ns) override;
    void on_event(int fd, uint32_t flag) override;

public:
    IBus(std::shared_ptr<Serial> serial, uint8_t channels = 10, uint64_t timeout_ns = 25000000);
    ~IBus();

    inline std::shared_ptr<core::Stream> get_stream() { return stream; }
};

};  // namespace llhardware::io