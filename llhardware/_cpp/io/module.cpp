#include <pybind11/pybind11.h>
#include <pybind11/stl.h>

#include "io.hpp"

namespace py = pybind11;
namespace llhardware::io {

class PyIPWMPulseWidth : public IPWMPulseWidth {
public:
    /* Inherit the constructors */
    using IPWMPulseWidth::IPWMPulseWidth;

    uint64_t get_pulse_width() override {
        PYBIND11_OVERRIDE_PURE_NAME(
            uint64_t,           /* Return type */
            IPWMPulseWidth,     /* Parent class */
            "_get_pulse_width", /* Name of method in Python */
            get_pulse_width,    /* Name of function in C++ */
        );
    }

    void set_pulse_width(uint64_t width) override {
        PYBIND11_OVERRIDE_PURE_NAME(
            void,               /* Return type */
            IPWMPulseWidth,     /* Parent class */
            "_set_pulse_width", /* Name of method in Python */
            set_pulse_width,    /* Name of function in C++ */
            width              /* Arguments */
        );
    }
};

class PyIPWMFillFactor : public IPWMFillFactor {
public:
    /* Inherit the constructors */
    using IPWMFillFactor::IPWMFillFactor;

    float get_fill_factor() override {
        PYBIND11_OVERRIDE_PURE_NAME(
            uint64_t,           /* Return type */
            IPWMFillFactor,     /* Parent class */
            "_get_fill_factor", /* Name of method in Python */
            get_fill_factor,    /* Name of function in C++ */
        );
    }

    void set_fill_factor(float factor) override {
        PYBIND11_OVERRIDE_PURE_NAME(
            void,               /* Return type */
            IPWMFillFactor,     /* Parent class */
            "_set_fill_factor", /* Name of method in Python */
            set_fill_factor,    /* Name of function in C++ */
            factor             /* Arguments */
        );
    }
};

void init_module(py::module &m) {
    py::class_<I2C, PI2C>(m, "I2C")
        .def(py::init<const std::string&>())
        .def_readonly("filename", &I2C::filename)
        .def("close", &I2C::close)
        .def("scan", &I2C::scan, py::call_guard<py::gil_scoped_release>());

    py::class_<I2CDevice, std::shared_ptr<I2CDevice>>(m, "I2CDevice")
        .def(py::init<PI2C, uint8_t>())
        .def("read_byte", &I2CDevice::py_read_byte, py::call_guard<py::gil_scoped_release>())
        .def("write_byte", &I2CDevice::write_byte, py::call_guard<py::gil_scoped_release>())
        .def("read_byte_data", &I2CDevice::py_read_byte_data, py::call_guard<py::gil_scoped_release>())
        .def("write_byte_data", &I2CDevice::write_byte_data, py::call_guard<py::gil_scoped_release>())
        .def("read_word_data", &I2CDevice::py_read_word_data, py::call_guard<py::gil_scoped_release>())
        .def("write_word_data", &I2CDevice::write_word_data, py::call_guard<py::gil_scoped_release>())
        .def("read_block_data", &I2CDevice::py_read_block_data, py::call_guard<py::gil_scoped_release>())
        .def("write_block_data", &I2CDevice::py_write_block_data, py::call_guard<py::gil_scoped_release>());

    const auto gpio = py::class_<GPIO>(m, "GPIO")
        .def(py::init<GPIO::GPIOMode>())
        .def("input", &GPIO::input, py::call_guard<py::gil_scoped_release>())
        .def("sys_input", &GPIO::sys_input, py::call_guard<py::gil_scoped_release>())
        .def("output", &GPIO::output, py::call_guard<py::gil_scoped_release>())
        .def("sys_output", &GPIO::sys_output, py::call_guard<py::gil_scoped_release>());

    py::enum_<GPIO::GPIOMode>(gpio, "GPIOMode")
        .value("GPIO_MODE", GPIO::GPIOMode::GPIO_MODE)
        .value("PINS_MODE", GPIO::GPIOMode::PINS_MODE)
        .value("PHYS_MODE", GPIO::GPIOMode::PHYS_MODE)
        .export_values();

    py::enum_<GPIO::DigitalValue>(gpio, "DigitalValue")
        .value("LOW", GPIO::DigitalValue::LOW)
        .value("HIGH", GPIO::DigitalValue::HIGH)
        .export_values();

    py::enum_<GPIO::PullMode>(gpio, "PullMode")
        .value("PUD_OFF", GPIO::PullMode::PUD_OFF)
        .value("PUD_DOWN", GPIO::PullMode::PUD_DOWN)
        .value("PUD_UP", GPIO::PullMode::PUD_UP)
        .export_values();

    py::enum_<GPIO::InterruptMode>(gpio, "InterruptMode")
        .value("NONE", GPIO::InterruptMode::NONE)
        .value("FALLING", GPIO::InterruptMode::FALLING)
        .value("RISING", GPIO::InterruptMode::RISING)
        .value("BOTH", GPIO::InterruptMode::BOTH)
        .export_values();

    py::class_<GPIO::IPin, std::shared_ptr<GPIO::IPin>>(gpio, "IPin")
        .def_readonly("pin", &GPIO::IPin::pin);

    py::class_<GPIO::IDigitalReadPin, std::shared_ptr<GPIO::IDigitalReadPin>, GPIO::IPin>(gpio, "IDigitalReadPin")
        .def("digital_read", &GPIO::IDigitalReadPin::digital_read, py::call_guard<py::gil_scoped_release>());

    py::class_<GPIO::IPullUpDnControlPin, std::shared_ptr<GPIO::IPullUpDnControlPin>, GPIO::IPin>(gpio, "IPullUpDnControlPin")
        .def("pull_control", &GPIO::IPullUpDnControlPin::pull_control, py::call_guard<py::gil_scoped_release>());

    py::class_<GPIO::IDigitalInterruptPin, std::shared_ptr<GPIO::IDigitalInterruptPin>, GPIO::IPin>(gpio, "IDigitalInterruptPin")
        .def("setup_interrupt", &GPIO::IDigitalInterruptPin::setup_interrupt, py::call_guard<py::gil_scoped_release>())
        .def_property_readonly("interrupt_stream", &GPIO::IDigitalInterruptPin::get_interrupt_stream);

    py::class_<GPIO::IDigitalWritePin, std::shared_ptr<GPIO::IDigitalWritePin>, GPIO::IPin>(gpio, "IDigitalWritePin")
        .def("digital_write", &GPIO::IDigitalWritePin::digital_write, py::call_guard<py::gil_scoped_release>());

    py::class_<GPIO::MemInputPin, std::shared_ptr<GPIO::MemInputPin>, GPIO::IDigitalReadPin, GPIO::IPullUpDnControlPin>(gpio, "MemInputPin");

    py::class_<GPIO::MemOutputPin, std::shared_ptr<GPIO::MemOutputPin>, GPIO::IDigitalWritePin>(gpio, "MemOutputPin");

    py::class_<GPIO::SysInputPin, std::shared_ptr<GPIO::SysInputPin>, GPIO::IDigitalReadPin, GPIO::IPullUpDnControlPin, GPIO::IDigitalInterruptPin, core::IOUpdate>(gpio, "SysInputPin");

    py::class_<GPIO::SysOutputPin, std::shared_ptr<GPIO::SysOutputPin>, GPIO::IDigitalWritePin>(gpio, "SysOutputPin");

    py::class_<IPWMPulseWidth, std::shared_ptr<IPWMPulseWidth>, PyIPWMPulseWidth>(m, "IPWMPulseWidth")
        .def("_get_pulse_width", &IPWMPulseWidth::get_pulse_width, py::call_guard<py::gil_scoped_release>())
        .def("_set_pulse_width", &IPWMPulseWidth::set_pulse_width, py::call_guard<py::gil_scoped_release>())
        .def_property("pulse_width", &IPWMPulseWidth::get_pulse_width, &IPWMPulseWidth::set_pulse_width, py::call_guard<py::gil_scoped_release>());

    py::class_<IPWMFillFactor, std::shared_ptr<IPWMFillFactor>, PyIPWMFillFactor>(m, "IPWMFillFactor")
        .def("_get_fill_factor", &IPWMFillFactor::get_fill_factor, py::call_guard<py::gil_scoped_release>())
        .def("_set_fill_factor", &IPWMFillFactor::set_fill_factor, py::call_guard<py::gil_scoped_release>())
        .def_property("fill_factor", &IPWMFillFactor::get_fill_factor, &IPWMFillFactor::set_fill_factor, py::call_guard<py::gil_scoped_release>());

    py::class_<SoftPWM, std::shared_ptr<SoftPWM>, core::SimpleUpdate, IPWMPulseWidth, IPWMFillFactor>(m, "SoftPWM")
        .def(py::init<std::shared_ptr<io::GPIO::IDigitalWritePin>, uint64_t>())
        .def_property("pulse_width", &SoftPWM::get_pulse_width, &SoftPWM::set_pulse_width, py::call_guard<py::gil_scoped_release>())
        .def_property("fill_factor", &SoftPWM::get_fill_factor, &SoftPWM::set_fill_factor, py::call_guard<py::gil_scoped_release>());

    py::class_<Serial, std::shared_ptr<Serial>>(m, "Serial")
        .def(py::init<std::string, uint32_t>())
        .def_property_readonly("fd", &Serial::get_fd)
        .def_property_readonly("filename", &Serial::get_filename)
        .def_property_readonly("baudrate", &Serial::get_baudrate)
        .def("read", &Serial::py_read, py::call_guard<py::gil_scoped_release>())
        .def("write", &Serial::py_write, py::call_guard<py::gil_scoped_release>())
        .def("close", &Serial::close, py::call_guard<py::gil_scoped_release>());

    py::class_<IBus, std::shared_ptr<IBus>, core::IOUpdate>(m, "IBus")
        .def(py::init<std::shared_ptr<Serial>, uint8_t, uint64_t>())
        .def_property_readonly("stream", &IBus::get_stream);

    const auto ru_owen = py::class_<RuOwen, std::shared_ptr<RuOwen>, core::IOUpdate>(m, "RuOwen")
        .def(py::init<std::shared_ptr<Serial>, bool, uint64_t>())
        .def_static("name2cmd", &RuOwen::name2cmd)
        .def_property_readonly("addr_len_8", &RuOwen::get_addr_len_8)
        .def("make_packet", &RuOwen::make_packet, py::call_guard<py::gil_scoped_release>())
        .def("parse_packet", &RuOwen::parse_packet, py::call_guard<py::gil_scoped_release>())
        .def("send_packet", &RuOwen::send_packet, py::call_guard<py::gil_scoped_release>());

    py::class_<RuOwen::PacketData>(ru_owen, "PacketData")
        .def_readonly("addr", &RuOwen::PacketData::addr)
        .def_readonly("cmd", &RuOwen::PacketData::cmd)
        .def_readonly("data", &RuOwen::PacketData::data)
        .def_readonly("read_or_write", &RuOwen::PacketData::read_or_write);
}

};  // namespace llhardware::io
