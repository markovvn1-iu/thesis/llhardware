#include "gpio.hpp"

#include <cstdio>
#include <cstring>
#include <fcntl.h>
#include <sys/mman.h>
#include <poll.h>

#include "core/core.hpp"
#include "gpio_consts.hpp"

using namespace llhardware::io;

/*
 * piGpioLayout:
 *	Return a number representing the hardware revision of the board.
 *	This is not strictly the board revision but is used to check the
 *	layout of the GPIO connector - and there are 2 types that we are
 *	really interested in here. The very earliest Pi's and the
 *	ones that came after that which switched some pins ....
 *
 *	Revision 1 really means the early Model A and B's.
 *	Revision 2 is everything else - it covers the B, B+ and CM.
 *		... and the Pi 2 - which is a B+ ++  ...
 *		... and the Pi 0 - which is an A+ ...
 *
 *	The main difference between the revision 1 and 2 system that I use here
 *	is the mapping of the GPIO pins. From revision 2, the Pi Foundation changed
 *	3 GPIO pins on the (original) 26-way header - BCM_GPIO 22 was dropped and
 *	replaced with 27, and 0 + 1 - I2C bus 0 was changed to 2 + 3; I2C bus 1.
 *
 *	Additionally, here we set the piModel2 flag too. This is again, nothing to
 *	do with the actual model, but the major version numbers - the GPIO base
 *	hardware address changed at model 2 and above (not the Zero though)
 *
 *********************************************************************************
 */

int piGpioLayout() {
    FILE *cpuFd;
    char line[120];
    char *c;

    static int gpioLayout = -1;
    if (gpioLayout != -1) return gpioLayout;  // No point checking twice

    if ((cpuFd = fopen("/proc/cpuinfo", "r")) == NULL)
        throw GPIOLayoutException("Unable to open /proc/cpuinfo");

    // Start by looking for the Architecture to make sure we're really running
    //	on a Pi. I'm getting fed-up with people whinging at me because
    //	they can't get it to work on weirdFruitPi boards...
    while (fgets(line, 120, cpuFd) != NULL)
        if (strncmp(line, "Hardware", 8) == 0) break;
    if (strncmp(line, "Hardware", 8) != 0) throw GPIOLayoutException("No \"Hardware\" line");

#ifdef GPIO_DEBUG
    printf("piGpioLayout: Hardware: %s\n", line);
#endif

    // See if it's BCM2708 or BCM2709 or the new BCM2835.
    if (!(strstr(line, "BCM2708") || strstr(line, "BCM2709") || strstr(line, "BCM2835")))
        throw GPIOException("Unable to determine hardware version. I see: " + std::string(line) +
            ", expecting BCM2708, BCM2709 or BCM2835. This module support RaspberryPi only");

    // Isolate the Revision line
    rewind(cpuFd);
    while (fgets(line, 120, cpuFd) != NULL)
        if (strncmp(line, "Revision", 8) == 0) break;
    fclose(cpuFd);
    if (strncmp(line, "Revision", 8) != 0) throw GPIOLayoutException("No \"Revision\" line");

    // Chomp trailing CR/NL
    for (c = &line[strlen(line) - 1]; (*c == '\n') || (*c == '\r'); --c)
        *c = 0;

#ifdef GPIO_DEBUG
    printf("piGpioLayout: Revision string: %s\n", line);
#endif

    // Scan to the first character of the revision number
    for (c = line; *c; ++c)
        if (*c == ':') break;
    if (*c != ':') throw GPIOLayoutException("Bogus \"Revision\" line (no colon)");

    // Chomp spaces
    ++c;
    while (isspace(*c)) ++c;
    if (!isxdigit(*c)) throw GPIOLayoutException("Bogus \"Revision\" line (no hex digit at start of revision)");

    // Make sure its long enough
    if (strlen(c) < 4) throw GPIOLayoutException("Bogus revision line (too small)");

    // Isolate last 4 characters: (in-case of overvolting or new encoding scheme)
    c = c + strlen(c) - 4;

#ifdef GPIO_DEBUG
    printf("piGpioLayout: last4Chars are: \"%s\"\n", c);
#endif

    if ((strcmp(c, "0002") == 0) || (strcmp(c, "0003") == 0))
        gpioLayout = 1;
    else
        gpioLayout = 2; 	// Covers everything else from the B revision 2 to the B+, the Pi v2, v3, zero and CM's.

#ifdef GPIO_DEBUG
    printf("piGpioLayout: Returning revision: %d\n", gpioLayout);
#endif

    return gpioLayout;
}

/*
 * piBoardId:
 *	Return the real details of the board we have.
 *
 *	This is undocumented and really only intended for the GPIO command.
 *	Use at your own risk!
 *
 *	Seems there are some boards with 0000 in them (mistake in manufacture)
 *	So the distinction between boards that I can see is:
 *
 *		0000 - Error
 *		0001 - Not used
 *
 *	Original Pi boards:
 *		0002 - Model B,  Rev 1,   256MB, Egoman
 *		0003 - Model B,  Rev 1.1, 256MB, Egoman, Fuses/D14 removed.
 *
 *	Newer Pi's with remapped GPIO:
 *		0004 - Model B,  Rev 1.2, 256MB, Sony
 *		0005 - Model B,  Rev 1.2, 256MB, Egoman
 *		0006 - Model B,  Rev 1.2, 256MB, Egoman
 *
 *		0007 - Model A,  Rev 1.2, 256MB, Egoman
 *		0008 - Model A,  Rev 1.2, 256MB, Sony
 *		0009 - Model A,  Rev 1.2, 256MB, Egoman
 *
 *		000d - Model B,  Rev 1.2, 512MB, Egoman	(Red Pi, Blue Pi?)
 *		000e - Model B,  Rev 1.2, 512MB, Sony
 *		000f - Model B,  Rev 1.2, 512MB, Egoman
 *
 *		0010 - Model B+, Rev 1.2, 512MB, Sony
 *		0013 - Model B+  Rev 1.2, 512MB, Embest
 *		0016 - Model B+  Rev 1.2, 512MB, Sony
 *		0019 - Model B+  Rev 1.2, 512MB, Egoman
 *
 *		0011 - Pi CM,    Rev 1.1, 512MB, Sony
 *		0014 - Pi CM,    Rev 1.1, 512MB, Embest
 *		0017 - Pi CM,    Rev 1.1, 512MB, Sony
 *		001a - Pi CM,    Rev 1.1, 512MB, Egoman
 *
 *		0012 - Model A+  Rev 1.1, 256MB, Sony
 *		0015 - Model A+  Rev 1.1, 512MB, Embest
 *		0018 - Model A+  Rev 1.1, 256MB, Sony
 *		001b - Model A+  Rev 1.1, 256MB, Egoman
 *
 *	A small thorn is the olde style overvolting - that will add in
 *		1000000
 *
 *	The Pi compute module has an revision of 0011 or 0014 - since we only
 *	check the last digit, then it's 1, therefore it'll default to not 2 or
 *	3 for a	Rev 1, so will appear as a Rev 2. This is fine for the most part, but
 *	we'll properly detect the Compute Module later and adjust accordingly.
 *
 * And then things changed with the introduction of the v2...
 *
 * For Pi v2 and subsequent models - e.g. the Zero:
 *
 *   [USER:8] [NEW:1] [MEMSIZE:3] [MANUFACTURER:4] [PROCESSOR:4] [TYPE:8] [REV:4]
 *   NEW          23: will be 1 for the new scheme, 0 for the old scheme
 *   MEMSIZE      20: 0=256M 1=512M 2=1G
 *   MANUFACTURER 16: 0=SONY 1=EGOMAN 2=EMBEST
 *   PROCESSOR    12: 0=2835 1=2836
 *   TYPE         04: 0=MODELA 1=MODELB 2=MODELA+ 3=MODELB+ 4=Pi2 MODEL B 5=ALPHA 6=CM
 *   REV          00: 0=REV0 1=REV1 2=REV2
 *********************************************************************************
 */
void piBoardId(int* model, int* rev, int* mem, int* maker, int* warranty) {
    FILE *cpuFd;
    char line [120];
    char *c;

    // Will deal with the properly later on - for now, lets just get it going...
    // unsigned int modelNum;

    piGpioLayout();  // Call this first to make sure all's OK. Don't care about the result.

    if ((cpuFd = fopen("/proc/cpuinfo", "r")) == NULL)
        throw GPIOLayoutException("Unable to open /proc/cpuinfo");

    while (fgets(line, 120, cpuFd) != NULL)
        if (strncmp(line, "Revision", 8) == 0) break;

    fclose(cpuFd);

    if (strncmp(line, "Revision", 8) != 0)
        throw GPIOLayoutException("No \"Revision\" line");

    // Chomp trailing CR/NL
    for (c = &line [strlen (line) - 1]; (*c == '\n') || (*c == '\r'); --c)
        *c = 0;

#ifdef GPIO_DEBUG
    printf ("piBoardId: Revision string: %s\n", line);
#endif

    // Need to work out if it's using the new or old encoding scheme:
    // Scan to the first character of the revision number
    for (c = line; *c; ++c)
        if (*c == ':') break;
    if (*c != ':') throw GPIOLayoutException("Bogus \"Revision\" line (no colon)");

    // Chomp spaces
    ++c;
    while (isspace(*c)) ++c;
    if (!isxdigit(*c))
        throw GPIOLayoutException("Bogus \"Revision\" line (no hex digit at start of revision)");

    unsigned int revision = (unsigned int)strtol(c, NULL, 16);  // Hex number with no leading 0x

    // Check for new way:
    if ((revision & (1 << 23)) != 0) {  // New way
#ifdef GPIO_DEBUG
        printf ("piBoardId: New Way: revision is: %08X\n", revision);
#endif
        int bRev, bType, bProc, bMfg, bMem, bWarranty;
        bRev      = (revision & (0x0F <<  0)) >>  0;
        bType     = (revision & (0xFF <<  4)) >>  4;
        bProc     = (revision & (0x0F << 12)) >> 12;  // Not used for now.
        bMfg      = (revision & (0x0F << 16)) >> 16;
        bMem      = (revision & (0x07 << 20)) >> 20;
        bWarranty = (revision & (0x03 << 24)) != 0;

        *model    = bType;
        *rev      = bRev;
        *mem      = bMem;
        *maker    = bMfg;
        *warranty = bWarranty;

#ifdef GPIO_DEBUG
        printf ("piBoardId: rev: %d, type: %d, proc: %d, mfg: %d, mem: %d, warranty: %d\n",
            bRev, bType, bProc, bMfg, bMem, bWarranty);
#endif
    } else {  // Old way
#ifdef GPIO_DEBUG
        printf ("piBoardId: Old Way: revision is: %s\n", c);
#endif

        if (!isdigit(*c))
            throw GPIOLayoutException("Bogus \"Revision\" line (no digit at start of revision)");

        // Make sure its long enough
        if (strlen(c) < 4)
            throw GPIOLayoutException("Bogus \"Revision\" line (not long enough)");

        // If longer than 4, we'll assume it's been overvolted
        *warranty = strlen(c) > 4;

        // Extract last 4 characters:
        c = c + strlen(c) - 4;

        // Fill out the replys as appropriate
        /**/ if (strcmp (c, "0002") == 0) { *model = PI_MODEL_B ; *rev = PI_VERSION_1  ; *mem = 0; *maker = PI_MAKER_EGOMAN ; }
        else if (strcmp (c, "0003") == 0) { *model = PI_MODEL_B ; *rev = PI_VERSION_1_1; *mem = 0; *maker = PI_MAKER_EGOMAN ; }

        else if (strcmp (c, "0004") == 0) { *model = PI_MODEL_B ; *rev = PI_VERSION_1_2; *mem = 0; *maker = PI_MAKER_SONY   ; }
        else if (strcmp (c, "0005") == 0) { *model = PI_MODEL_B ; *rev = PI_VERSION_1_2; *mem = 0; *maker = PI_MAKER_EGOMAN ; }
        else if (strcmp (c, "0006") == 0) { *model = PI_MODEL_B ; *rev = PI_VERSION_1_2; *mem = 0; *maker = PI_MAKER_EGOMAN ; }

        else if (strcmp (c, "0007") == 0) { *model = PI_MODEL_A ; *rev = PI_VERSION_1_2; *mem = 0; *maker = PI_MAKER_EGOMAN ; }
        else if (strcmp (c, "0008") == 0) { *model = PI_MODEL_A ; *rev = PI_VERSION_1_2; *mem = 0; *maker = PI_MAKER_SONY   ; }
        else if (strcmp (c, "0009") == 0) { *model = PI_MODEL_A ; *rev = PI_VERSION_1_2; *mem = 0; *maker = PI_MAKER_EGOMAN ; }

        else if (strcmp (c, "000d") == 0) { *model = PI_MODEL_B ; *rev = PI_VERSION_1_2; *mem = 1; *maker = PI_MAKER_EGOMAN ; }
        else if (strcmp (c, "000e") == 0) { *model = PI_MODEL_B ; *rev = PI_VERSION_1_2; *mem = 1; *maker = PI_MAKER_SONY   ; }
        else if (strcmp (c, "000f") == 0) { *model = PI_MODEL_B ; *rev = PI_VERSION_1_2; *mem = 1; *maker = PI_MAKER_EGOMAN ; }

        else if (strcmp (c, "0010") == 0) { *model = PI_MODEL_BP; *rev = PI_VERSION_1_2; *mem = 1; *maker = PI_MAKER_SONY   ; }
        else if (strcmp (c, "0013") == 0) { *model = PI_MODEL_BP; *rev = PI_VERSION_1_2; *mem = 1; *maker = PI_MAKER_EMBEST ; }
        else if (strcmp (c, "0016") == 0) { *model = PI_MODEL_BP; *rev = PI_VERSION_1_2; *mem = 1; *maker = PI_MAKER_SONY   ; }
        else if (strcmp (c, "0019") == 0) { *model = PI_MODEL_BP; *rev = PI_VERSION_1_2; *mem = 1; *maker = PI_MAKER_EGOMAN ; }

        else if (strcmp (c, "0011") == 0) { *model = PI_MODEL_CM; *rev = PI_VERSION_1_1; *mem = 1; *maker = PI_MAKER_SONY   ; }
        else if (strcmp (c, "0014") == 0) { *model = PI_MODEL_CM; *rev = PI_VERSION_1_1; *mem = 1; *maker = PI_MAKER_EMBEST ; }
        else if (strcmp (c, "0017") == 0) { *model = PI_MODEL_CM; *rev = PI_VERSION_1_1; *mem = 1; *maker = PI_MAKER_SONY   ; }
        else if (strcmp (c, "001a") == 0) { *model = PI_MODEL_CM; *rev = PI_VERSION_1_1; *mem = 1; *maker = PI_MAKER_EGOMAN ; }

        else if (strcmp (c, "0012") == 0) { *model = PI_MODEL_AP; *rev = PI_VERSION_1_1; *mem = 0; *maker = PI_MAKER_SONY   ; }
        else if (strcmp (c, "0015") == 0) { *model = PI_MODEL_AP; *rev = PI_VERSION_1_1; *mem = 1; *maker = PI_MAKER_EMBEST ; }
        else if (strcmp (c, "0018") == 0) { *model = PI_MODEL_AP; *rev = PI_VERSION_1_1; *mem = 0; *maker = PI_MAKER_SONY   ; }
        else if (strcmp (c, "001b") == 0) { *model = PI_MODEL_AP; *rev = PI_VERSION_1_1; *mem = 0; *maker = PI_MAKER_EGOMAN ; }

        else                   { *model = PI_MODEL_UNKNOWN; *rev = PI_VERSION_UNKNOWN; *mem =   0; *maker = PI_MAKER_UNKNOWN; }
    }
}

GPIO::LockedPin::~LockedPin() {
    Core& core = Core::get_instance();
    std::lock_guard lock(core.data_mutex);
    core.locked_pin.erase(pin);
}

GPIO::Core::Core() {
    int fd;

#ifdef GPIO_DEBUG
    printf ("wiringPi: wiringPiSetup called\n");
#endif

    // Get the board ID information. We're not really using the information here,
    //	but it will give us information like the GPIO layout scheme (2 variants
    //	on the older 26-pin Pi's) and the GPIO peripheral base address.
    //	and if we're running on a compute module, then wiringPi pin numbers
    //	don't really many anything, so force native BCM mode anyway.

    int mem, overVolted;
    piBoardId(&model, &rev, &mem, &maker, &overVolted);
    gpioLayout = piGpioLayout();

    unsigned int piGpioBase = 0;
    switch (model) {
        case PI_MODEL_A:	case PI_MODEL_B:
        case PI_MODEL_AP:	case PI_MODEL_BP:
        case PI_ALPHA:	    case PI_MODEL_CM:
        case PI_MODEL_ZERO:	case PI_MODEL_ZERO_W:
            piGpioBase = GPIO_PERI_BASE_OLD;
            piGpioPupOffset = GPPUD;
        break;

        case PI_MODEL_4B:
        case PI_MODEL_400:
        case PI_MODEL_CM4:
            piGpioBase = GPIO_PERI_BASE_2711;
            piGpioPupOffset = GPPUPPDN0;
        break;

        default:
            piGpioBase = GPIO_PERI_BASE_2835;
            piGpioPupOffset = GPPUD;
        break;
    }

    // Open the master /dev/ memory control device
    // Device strategy: December 2016:
    //	Try /dev/mem. If that fails, then
    //	try /dev/gpiomem. If that fails then game over.

    if ((fd = open("/dev/mem", O_RDWR | O_SYNC | O_CLOEXEC)) < 0) {
        if ((fd = open("/dev/gpiomem", O_RDWR | O_SYNC | O_CLOEXEC)) >= 0) {  // We're using gpiomem
            piGpioBase   = 0;
            usingGpioMem = true;
        } else {
            throw GPIOException(
                "GPIO::Core::Core: Unable to open /dev/mem or /dev/gpiomem: " + std::string(strerror(errno)) + ". "
                "The program can not access the GPIO hardware. Try running with sudo?");
        }
    }

    // Set the offsets into the memory interface.
    unsigned int GPIO_PADS 	     = piGpioBase + 0x00100000;
    unsigned int GPIO_CLOCK_BASE = piGpioBase + 0x00101000;
    unsigned int GPIO_BASE	     = piGpioBase + 0x00200000;
    unsigned int GPIO_TIMER	     = piGpioBase + 0x0000B000;
    unsigned int GPIO_PWM	     = piGpioBase + 0x0020C000;

    // Map the individual hardware components

    //	GPIO:
    hw.gpio = (uint32_t *)mmap(0, BLOCK_SIZE, PROT_READ|PROT_WRITE, MAP_SHARED, fd, GPIO_BASE);
    if (hw.gpio == MAP_FAILED)
        throw GPIOException("GPIO::Core::Core: mmap (GPIO) failed: " + std::string(strerror(errno)));

    //	PWM
    hw.pwm = (uint32_t *)mmap(0, BLOCK_SIZE, PROT_READ|PROT_WRITE, MAP_SHARED, fd, GPIO_PWM);
    if (hw.pwm == MAP_FAILED)
        throw GPIOException("GPIO::Core::Core: mmap (PWM) failed: " + std::string(strerror(errno)));

    //	Clock control (needed for PWM)
    hw.clk = (uint32_t *)mmap(0, BLOCK_SIZE, PROT_READ|PROT_WRITE, MAP_SHARED, fd, GPIO_CLOCK_BASE);
    if (hw.clk == MAP_FAILED)
        throw GPIOException("GPIO::Core::Core: mmap (CLOCK) failed: " + std::string(strerror(errno)));

    //	The drive pads
    hw.pads = (uint32_t *)mmap(0, BLOCK_SIZE, PROT_READ|PROT_WRITE, MAP_SHARED, fd, GPIO_PADS);
    if (hw.pads == MAP_FAILED)
        throw GPIOException("GPIO::Core::Core: mmap (PADS) failed: " + std::string(strerror(errno)));

    //	The system timer
    hw.timer = (uint32_t *)mmap(0, BLOCK_SIZE, PROT_READ|PROT_WRITE, MAP_SHARED, fd, GPIO_TIMER);
    if (hw.timer == MAP_FAILED)
        throw GPIOException("GPIO::Core::Core: mmap (TIMER) failed: " + std::string(strerror(errno)));

    // Set the timer to free-running, 1MHz.
    //	0xF9 is 249, the timer divide is base clock / (divide+1)
    //	so base clock is 250MHz / 250 = 1MHz.
    *(hw.timer + TIMER_CONTROL) = 0x0000280;
    *(hw.timer + TIMER_PRE_DIV) = 0x00000F9;
    hw.timerIrqRaw = hw.timer + TIMER_IRQ_RAW;
}

GPIO::Core& GPIO::Core::get_instance() {
    // TODO: что будет если эту функцию в первый раз вызовут несколько потоков одновременно? Будет ли создано несколько инстансов?
    static GPIO::Core instance;
    return instance;
}

std::shared_ptr<GPIO::LockedPin> GPIO::Core::lock_pin(int pin) {
    {
        std::lock_guard lock(data_mutex);
        if (locked_pin.count(pin) > 0) throw GPIOException("GPIO::Core::lock_pin: Pin " + std::to_string(pin) + " is already locked");
        locked_pin.insert(pin);
    }
    return std::make_shared<GPIO::LockedPin>(pin);
}

void GPIO::Core::pull_control(int pin, PullMode mode) {
    if (piGpioPupOffset == GPPUPPDN0) {
        // Pi 4B pull up/down method
        unsigned int* pullreg = hw.gpio + (GPPUPPDN0 + (pin>>4));
        int shift = (pin & 0xf) << 1;
        unsigned int pull = 0;

        switch (mode) {
            case PullMode::PUD_OFF: pull = 0b00; break;
            case PullMode::PUD_UP: pull = 0b01; break;
            case PullMode::PUD_DOWN: pull = 0b10; break;
        }

        const std::lock_guard lock(hw_mutex);
        *pullreg = *pullreg & ~(0b11 << shift) | (pull << shift);
    } else {
        const std::lock_guard lock(hw_mutex);
        // legacy pull up/down method
        hw.gpio[GPPUD]             = mode & 3;         usleep(5);
        hw.gpio[gpioToPUDCLK[pin]] = 1 << (pin & 31);  usleep(5);

        hw.gpio[GPPUD]             = 0;                usleep(5);
        hw.gpio[gpioToPUDCLK[pin]] = 0;                usleep(5);
    }
}

void GPIO::Core::sys_export(int pin) {
    FILE* fd;
    if ((fd = fopen("/sys/class/gpio/export", "w")) == NULL)
        throw GPIOException("GPIO::Core::sys_export(): Unable to open GPIO export interface: " + std::string(strerror(errno)));
    fprintf(fd, "%d\n", pin);
    fclose(fd);

    usleep(100000); // wait 100ms for correct permissions
}

void GPIO::Core::sys_set_direction(int pin, const char* direction) {
    FILE* fd;
    char fName[128];
    sprintf(fName, "/sys/class/gpio/gpio%d/direction", pin);
    if ((fd = fopen(fName, "w")) == NULL)
        throw GPIOException("GPIO::Core::sys_set_direction(): Unable to open GPIO direction interface for pin " + std::to_string(pin) + ": " + std::string(strerror(errno)));
    fprintf(fd, "%s\n", direction);
    fclose(fd);
}

void GPIO::Core::sys_unexport(int pin) {
    FILE* fd;
    if ((fd = fopen("/sys/class/gpio/unexport", "w")) == NULL)
        throw GPIOException("GPIO::Core::sys_unexport(): Unable to open GPIO unexport interface: " + std::string(strerror(errno)));
    fprintf(fd, "%d\n", pin);
    fclose(fd);
}

// void GPIO::Core::pin_mode(int pin, PinMode mode) {
//     assert_setuped("Core::pin_mode");
//     // TODO: check if the pin is busy

//     uint8_t& fSel = gpioToGPFSEL[pin];
//     uint8_t& shift = gpioToShift[pin];

//     const std::lock_guard lock(mutex);
//     if (mode == PinMode::INPUT) {
//         hw.gpio[fSel] = (hw.gpio[fSel] & ~(7 << shift));  // Sets bits to zero = input
//     } else if (mode == PinMode::OUTPUT) {
//         hw.gpio[fSel] = (hw.gpio[fSel] & ~(7 << shift)) | (1 << shift);
//     } else {
//         throw std::logic_error("Not implemented");
//     }
// }

// void GPIO::Core::export_sys(int pin) {
//     assert_setuped("Core::export_sys");
    
// }

// void GPIO::Core::unexport_sys(int pin) {
//     assert_setuped("Core::unexport_sys");
// }

// GPIO::DigitalValue GPIO::Core::digital_read(int pin) {
//     const std::lock_guard lock(mutex);
//     return ((hw.gpio[gpioToGPLEV[pin]] & (1 << (pin & 31))) != 0) ? DigitalValue::HIGH : DigitalValue::LOW;
// }

// void GPIO::Core::pull_control(int pin, PullMode value) {
//     if (piGpioPupOffset == GPPUPPDN0) {
//         // Pi 4B pull up/down method
//         int pullreg = GPPUPPDN0 + (pin>>4);
//         int pullshift = (pin & 0xf) << 1;
//         unsigned int pull;

//         switch (value) {
//             case PullMode::PUD_OFF: pull = 0; break;
//             case PullMode::PUD_UP: pull = 1; break;
//             case PullMode::PUD_DOWN: pull = 2; break;
//             default: return; /* An illegal value */
//         }

//         const std::lock_guard lock(mutex);
//         unsigned int pullbits = *(hw.gpio + pullreg);
//         pullbits &= ~(3 << pullshift);
//         pullbits |= (pull << pullshift);
//         *(hw.gpio + pullreg) = pullbits;
//     } else {
//         const std::lock_guard lock(mutex);
//         // legacy pull up/down method
//         hw.gpio[GPPUD]             = value & 3;        usleep(5);
//         hw.gpio[gpioToPUDCLK[pin]] = 1 << (pin & 31);  usleep(5);

//         hw.gpio[GPPUD]             = 0;                usleep(5);
//         hw.gpio[gpioToPUDCLK[pin]] = 0;                usleep(5);
//     }
// }

// void GPIO::Core::digital_write(int pin, DigitalValue value) {
//     const std::lock_guard lock(mutex);
//     // TODO: Add check if the pin in a right mode
//     if (value == LOW)
//         hw.gpio[gpioToGPCLR[pin]] = 1 << (pin & 31);
//     else
//         hw.gpio[gpioToGPSET[pin]] = 1 << (pin & 31);
// }

GPIO::MemInputPin::MemInputPin(std::shared_ptr<GPIO::LockedPin> locked_pin, bool inverse) : IPin(locked_pin), inverse(inverse) {
    gpioLEV = core.hw.gpio + gpioToGPLEV[pin];
    gpioLEVmask = 1 << (pin & 31);

    unsigned int* gpioFSEL = core.hw.gpio + gpioToGPFSEL[pin];
    uint8_t shiftFSEL = gpioToShift[pin];

    {
        const std::lock_guard lock(core.hw_mutex);
        *gpioFSEL &= ~(0b111 << shiftFSEL);  // Sets bits to zero = input
    }
    pull_control(PullMode::PUD_OFF);
}

GPIO::MemInputPin::~MemInputPin() {
    pull_control(PullMode::PUD_OFF);
}

GPIO::DigitalValue GPIO::MemInputPin::digital_read() {
    if (!inverse) {
        const std::lock_guard lock(core.hw_mutex);
        return ((*gpioLEV) & gpioLEVmask) ? DigitalValue::HIGH : DigitalValue::LOW;
    } else {
        const std::lock_guard lock(core.hw_mutex);
        return ((*gpioLEV) & gpioLEVmask) ? DigitalValue::LOW : DigitalValue::HIGH;
    }
}


GPIO::MemOutputPin::MemOutputPin(std::shared_ptr<LockedPin> locked_pin, bool inverse) : IPin(locked_pin), inverse(inverse) {
    gpioSET = core.hw.gpio + gpioToGPSET[pin];
    gpioCLR = core.hw.gpio + gpioToGPCLR[pin];
    gpioMask = 1 << (pin & 31);

    unsigned int* gpioFSEL = core.hw.gpio + gpioToGPFSEL[pin];
    uint8_t shiftFSEL = gpioToShift[pin];

    const std::lock_guard lock(core.hw_mutex);
    *gpioFSEL = *gpioFSEL & ~(0b111 << shiftFSEL) | (1 << shiftFSEL);
}

GPIO::MemOutputPin::~MemOutputPin() {
    unsigned int* gpioFSEL = core.hw.gpio + gpioToGPFSEL[pin];
    uint8_t shiftFSEL = gpioToShift[pin];

    {
        const std::lock_guard lock(core.hw_mutex);
        *gpioFSEL = *gpioFSEL & ~(0b111 << shiftFSEL);
    }
    core.pull_control(pin, PullMode::PUD_OFF);
}

void GPIO::MemOutputPin::digital_write(DigitalValue value) {
    if (inverse) value = value == DigitalValue::LOW ? DigitalValue::HIGH : DigitalValue::LOW;
    const std::lock_guard lock(core.hw_mutex);
    *(value == DigitalValue::LOW ? gpioCLR : gpioSET) = gpioMask;
}



GPIO::SysInputPin::SysInputPin(std::shared_ptr<LockedPin> locked_pin, bool inverse) : IPin(locked_pin), inverse(inverse) {
    IPin::core.sys_export(pin);
    IPin::core.sys_set_direction(pin, "in");
    setup_interrupt(InterruptMode::NONE);
    pull_control(PullMode::PUD_OFF);

    char fName[128];
    sprintf(fName, "/sys/class/gpio/gpio%d/value", pin);
    if ((value_fd = open(fName, O_RDONLY | O_NOCTTY | O_NDELAY | O_NONBLOCK)) < 0)
        throw GPIOException("SysInputPin::SysInputPin(): Unable to open GPIO value interface for pin " + std::to_string(pin) + ": " + std::string(strerror(errno)));
}

GPIO::SysInputPin::~SysInputPin() {
    unset_event_mask(value_fd);
    close(value_fd);
    setup_interrupt(InterruptMode::NONE);
    pull_control(PullMode::PUD_OFF);
    IPin::core.sys_unexport(pin);
}

void GPIO::SysInputPin::on_attach() {
    set_event_mask(value_fd, POLLPRI);
}

void GPIO::SysInputPin::on_event(int fd, uint32_t flag) {
    if (fd != value_fd) return;
    if (flag & POLLPRI) {
        stream->push(type::BoolDataItem::create(digital_read() == GPIO::DigitalValue::HIGH));
    }
}

GPIO::DigitalValue GPIO::SysInputPin::digital_read() {
    char value = '\0';
    lseek(value_fd, 0L, SEEK_SET);
    if (read(value_fd, &value, 1) != 1) throw GPIOException("GPIO::SysInputPin::digital_read(): Failed to read value");
    return (value == (inverse ? '1' : '0')) ? GPIO::DigitalValue::LOW : GPIO::DigitalValue::HIGH;
}

void GPIO::SysInputPin::setup_interrupt(InterruptMode mode) {
    FILE* fd;
    char fName[128];
    sprintf(fName, "/sys/class/gpio/gpio%d/edge", pin) ;
    if ((fd = fopen(fName, "w")) == NULL)
        throw GPIOException("SysInputPin::SysInputPin(): Unable to open GPIO edge interface for pin " + std::to_string(pin) + ": " + std::string(strerror(errno)));
    switch (mode)
    {
        case InterruptMode::NONE:    fprintf(fd, "none\n");    break;
        case InterruptMode::RISING:  fprintf(fd, "rising\n");  break;
        case InterruptMode::FALLING: fprintf(fd, "falling\n"); break;
        case InterruptMode::BOTH:    fprintf(fd, "both\n");    break;
    }
    fclose(fd);
}



GPIO::SysOutputPin::SysOutputPin(std::shared_ptr<LockedPin> locked_pin, bool inverse) : IPin(locked_pin), inverse(inverse) {
    IPin::core.sys_export(pin);
    IPin::core.sys_set_direction(pin, "out");

    char fName[128];
    sprintf(fName, "/sys/class/gpio/gpio%d/value", pin);
    if ((value_fd = open(fName, O_WRONLY)) < 0)
        throw GPIOException("SysInputPin::SysInputPin(): Unable to open GPIO value interface for pin " + std::to_string(pin) + ": " + std::string(strerror(errno)));
}

GPIO::SysOutputPin::~SysOutputPin() {
    close(value_fd);
    IPin::core.sys_set_direction(pin, "in");
    IPin::core.pull_control(pin, PullMode::PUD_OFF);
    IPin::core.sys_unexport(pin);
}

void GPIO::SysOutputPin::digital_write(DigitalValue value) {
    if (write(value_fd, value == DigitalValue::LOW ? "0\n" : "1\n", 2) != 2)
        throw GPIOException("GPIO::SysOutputPin::digital_write(): Failed to write value");
}



GPIO::GPIO(GPIO::GPIOMode mode) : core(GPIO::Core::get_instance()) {
    if (mode == GPIO::GPIOMode::PINS_MODE)
        if ((core.model == PI_MODEL_CM) ||
            (core.model == PI_MODEL_CM3) ||
            (core.model == PI_MODEL_CM3P))
            mode = GPIO::GPIOMode::GPIO_MODE;
    
    this->mode = mode;
    if (mode == GPIO::GPIOMode::GPIO_MODE) return;

    if (core.gpioLayout == 1) {  // A, B, Rev 1, 1.1
        pinToGpio = mode == GPIO::GPIOMode::PINS_MODE ? pinToGpioR1 : physToGpioR1;
    } else {                    // A2, B2, A+, B+, CM, Pi2, Pi3, Zero, Zero W, Zero 2 W
        pinToGpio = mode == GPIO::GPIOMode::PINS_MODE ? pinToGpioR2 : physToGpioR2;
    }
}

int GPIO::get_pin(int pin) const {
    if ((pin & PI_GPIO_MASK) != 0) throw GPIOException("pin " + std::to_string(pin) + " is not belong to Raspberry Pi");
    if (pinToGpio == NULL) return pin;
    return pinToGpio[pin];
}

std::shared_ptr<GPIO::MemInputPin> GPIO::input(int pin, bool inverse) {
    return std::make_shared<GPIO::MemInputPin>(core.lock_pin(get_pin(pin)), inverse);
}

std::shared_ptr<GPIO::SysInputPin> GPIO::sys_input(int pin, bool inverse) {
    return std::make_shared<GPIO::SysInputPin>(core.lock_pin(get_pin(pin)), inverse);
}

std::shared_ptr<GPIO::MemOutputPin> GPIO::output(int pin, bool inverse, GPIO::DigitalValue init_value) {
    auto res = std::make_shared<GPIO::MemOutputPin>(core.lock_pin(get_pin(pin)), inverse);
    res->digital_write(init_value);
    return res;
}

std::shared_ptr<GPIO::SysOutputPin> GPIO::sys_output(int pin, bool inverse, GPIO::DigitalValue init_value) {
    auto res = std::make_shared<GPIO::SysOutputPin>(core.lock_pin(get_pin(pin)), inverse);
    res->digital_write(init_value);
    return res;
}
