#include <cstdint>

namespace llhardware::io {

enum RaspberryPiModel {
    PI_MODEL_UNKNOWN = 0,
    PI_MODEL_A = 0,
    PI_MODEL_B = 1,
    PI_MODEL_AP = 2,
    PI_MODEL_BP = 3,
    PI_MODEL_2 = 4,
    PI_ALPHA = 5,
    PI_MODEL_CM = 6,
    PI_MODEL_07 = 7,
    PI_MODEL_3B = 8,
    PI_MODEL_ZERO = 9,
    PI_MODEL_CM3 = 10,
    PI_MODEL_ZERO_W = 12,
    PI_MODEL_3BP = 13,
    PI_MODEL_3AP = 14,
    PI_MODEL_CM3P = 16,
    PI_MODEL_4B = 17,
    PI_MODEL_ZERO_2W = 18,
    PI_MODEL_400 = 19,
    PI_MODEL_CM4 = 20,
};

enum RaspberryPiVersion {
    PI_VERSION_UNKNOWN = 0,
    PI_VERSION_1 = 0,
    PI_VERSION_1_1 = 1,
    PI_VERSION_1_2 = 2,
    PI_VERSION_2 = 3,
};

enum RaspberryPiMaker {
    PI_MAKER_UNKNOWN = 0,
    PI_MAKER_SONY = 0,
    PI_MAKER_EGOMAN = 1,
    PI_MAKER_EMBEST = 2,
};

// Data for use with the boardId functions.
//	The order of entries here to correspond with the PI_MODEL_X
//	and PI_VERSION_X defines in wiringPi.h
//	Only intended for the gpio command - use at your own risk!

// PiGPIOBase:
//	The base address of the GPIO memory mapped hardware IO

enum PiGPIOBase {
    GPIO_PERI_BASE_OLD  = 0x20000000,
    GPIO_PERI_BASE_2835 = 0x3F000000,
    GPIO_PERI_BASE_2711 = 0xFE000000,
};

// GPPUD:
//	GPIO Pin pull up/down register

enum GPPUD {
    GPPUD     = 37,

    /* 2711 has a different mechanism for pin pull-up/down/enable  */
    GPPUPPDN0 = 57,        /* Pin pull-up/down for pins 15:0  */
    GPPUPPDN1 = 58,        /* Pin pull-up/down for pins 31:16 */
    GPPUPPDN2 = 59,        /* Pin pull-up/down for pins 47:32 */
    GPPUPPDN3 = 60,        /* Pin pull-up/down for pins 57:48 */
};


// Timer
//	Word offsets
enum GPIOTimer {
    TIMER_LOAD =     (0x400 >> 2),
    TIMER_VALUE =    (0x404 >> 2),
    TIMER_CONTROL =  (0x408 >> 2),
    TIMER_IRQ_CLR =  (0x40C >> 2),
    TIMER_IRQ_RAW =  (0x410 >> 2),
    TIMER_IRQ_MASK = (0x414 >> 2),
    TIMER_RELOAD =   (0x418 >> 2),
    TIMER_PRE_DIV =  (0x41C >> 2),
    TIMER_COUNTER =  (0x420 >> 2),
};


// Mask for the bottom 64 pins which belong to the Raspberry Pi
//	The others are available for the other devices
#define	PI_GPIO_MASK	(0xFFFFFFC0)

#define	PAGE_SIZE		(4*1024)
#define	BLOCK_SIZE		(4*1024)


// pinToGpio:
//      Take a Wiring pin (0 through X) and re-map it to the BCM_GPIO pin
//      Cope for 3 different board revisions here.

// Revision 1, 1.1:
static int pinToGpioR1[64] = {
  17, 18, 21, 22, 23, 24, 25, 4,        // From the Original Wiki - GPIO 0 through 7:   wpi  0 -  7
   0,  1,                               // I2C  - SDA1, SCL1                            wpi  8 -  9
   8,  7,                               // SPI  - CE1, CE0                              wpi 10 - 11
  10,  9, 11,                           // SPI  - MOSI, MISO, SCLK                      wpi 12 - 14
  14, 15,                               // UART - Tx, Rx                                wpi 15 - 16

// Padding:
      -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1,       // ... 31
  -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1,       // ... 47
  -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1,       // ... 63
};

// Revision 2:
static int pinToGpioR2[64] = {
  17, 18, 27, 22, 23, 24, 25, 4,        // From the Original Wiki - GPIO 0 through 7:   wpi  0 -  7
   2,  3,                               // I2C  - SDA0, SCL0                            wpi  8 -  9
   8,  7,                               // SPI  - CE1, CE0                              wpi 10 - 11
  10,  9, 11,                           // SPI  - MOSI, MISO, SCLK                      wpi 12 - 14
  14, 15,                               // UART - Tx, Rx                                wpi 15 - 16
  28, 29, 30, 31,                       // Rev 2: New GPIOs 8 though 11                 wpi 17 - 20
   5,  6, 13, 19, 26,                   // B+                                           wpi 21, 22, 23, 24, 25
  12, 16, 20, 21,                       // B+                                           wpi 26, 27, 28, 29
   0,  1,                               // B+                                           wpi 30, 31

// Padding:
  -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1,       // ... 47
  -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1,       // ... 63
};



// physToGpio:
//      Take a physical pin (1 through 26) and re-map it to the BCM_GPIO pin
//      Cope for 2 different board revisions here.
//      Also add in the P5 connector, so the P5 pins are 3,4,5,6, so 53,54,55,56

static int physToGpioR1 [64] = {
  -1,           // 0
  -1, -1,       // 1, 2
   0, -1,
   1, -1,
   4, 14,
  -1, 15,
  17, 18,
  21, -1,
  22, 23,
  -1, 24,
  10, -1,
   9, 25,
  11,  8,
  -1,  7,       // 25, 26

                                              -1, -1, -1, -1, -1,       // ... 31
  -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1,       // ... 47
  -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1,       // ... 63
};

static int physToGpioR2[64] = {
  -1,           // 0
  -1, -1,       // 1, 2
   2, -1,
   3, -1,
   4, 14,
  -1, 15,
  17, 18,
  27, -1,
  22, 23,
  -1, 24,
  10, -1,
   9, 25,
  11,  8,
  -1,  7,       // 25, 26

// B+
   0,  1,
   5, -1,
   6, 12,
  13, -1,
  19, 16,
  26, 20,
  -1, 21,

// the P5 connector on the Rev 2 boards:
  -1, -1,
  -1, -1,
  -1, -1,
  -1, -1,
  -1, -1,
  28, 29,
  30, 31,
  -1, -1,
  -1, -1,
  -1, -1,
  -1, -1,
};


// gpioToGPFSEL:
//	Map a BCM_GPIO pin to it's Function Selection
//	control port. (GPFSEL 0-5)
//	Groups of 10 - 3 bits per Function - 30 bits per port
static uint8_t gpioToGPFSEL [] = {
  0,0,0,0,0,0,0,0,0,0,
  1,1,1,1,1,1,1,1,1,1,
  2,2,2,2,2,2,2,2,2,2,
  3,3,3,3,3,3,3,3,3,3,
  4,4,4,4,4,4,4,4,4,4,
  5,5,5,5,5,5,5,5,5,5,
};


// gpioToShift
//	Define the shift up for the 3 bits per pin in each GPFSEL port
static uint8_t gpioToShift [] = {
  0,3,6,9,12,15,18,21,24,27,
  0,3,6,9,12,15,18,21,24,27,
  0,3,6,9,12,15,18,21,24,27,
  0,3,6,9,12,15,18,21,24,27,
  0,3,6,9,12,15,18,21,24,27,
  0,3,6,9,12,15,18,21,24,27,
};


// Port function select bits
#define	FSEL_INPT		0b000
#define	FSEL_OUTP		0b001
#define	FSEL_ALT0		0b100
#define	FSEL_ALT1		0b101
#define	FSEL_ALT2		0b110
#define	FSEL_ALT3		0b111
#define	FSEL_ALT4		0b011
#define	FSEL_ALT5		0b010


// gpioToPwmALT
//	the ALT value to put a GPIO pin into PWM mode
static uint8_t gpioToPwmALT [] = {
          0,         0,         0,         0,         0,         0,         0,         0,	//  0 ->  7
          0,         0,         0,         0, FSEL_ALT0, FSEL_ALT0,         0,         0, 	//  8 -> 15
          0,         0, FSEL_ALT5, FSEL_ALT5,         0,         0,         0,         0, 	// 16 -> 23
          0,         0,         0,         0,         0,         0,         0,         0,	// 24 -> 31
          0,         0,         0,         0,         0,         0,         0,         0,	// 32 -> 39
  FSEL_ALT0, FSEL_ALT0,         0,         0,         0, FSEL_ALT0,         0,         0,	// 40 -> 47
          0,         0,         0,         0,         0,         0,         0,         0,	// 48 -> 55
          0,         0,         0,         0,         0,         0,         0,         0,	// 56 -> 63
};


// gpioToGpClkALT0:
static uint8_t gpioToGpClkALT0 [] = {
          0,         0,         0,         0, FSEL_ALT0, FSEL_ALT0, FSEL_ALT0,         0,	//  0 ->  7
          0,         0,         0,         0,         0,         0,         0,         0, 	//  8 -> 15
          0,         0,         0,         0, FSEL_ALT5, FSEL_ALT5,         0,         0, 	// 16 -> 23
          0,         0,         0,         0,         0,         0,         0,         0,	// 24 -> 31
  FSEL_ALT0,         0, FSEL_ALT0,         0,         0,         0,         0,         0,	// 32 -> 39
          0,         0, FSEL_ALT0, FSEL_ALT0, FSEL_ALT0,         0,         0,         0,	// 40 -> 47
          0,         0,         0,         0,         0,         0,         0,         0,	// 48 -> 55
          0,         0,         0,         0,         0,         0,         0,         0,	// 56 -> 63
};


// gpioToGPSET:
//	(Word) offset to the GPIO Set registers for each GPIO pin

static uint8_t gpioToGPSET [] = {
   7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7,
   8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8,
};

// gpioToGPCLR:
//	(Word) offset to the GPIO Clear registers for each GPIO pin

static uint8_t gpioToGPCLR [] = {
  10,10,10,10,10,10,10,10,10,10,10,10,10,10,10,10,10,10,10,10,10,10,10,10,10,10,10,10,10,10,10,10,
  11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,
};


// gpioToGPLEV:
//	(Word) offset to the GPIO Input level registers for each GPIO pin

static uint8_t gpioToGPLEV [] = {
  13,13,13,13,13,13,13,13,13,13,13,13,13,13,13,13,13,13,13,13,13,13,13,13,13,13,13,13,13,13,13,13,
  14,14,14,14,14,14,14,14,14,14,14,14,14,14,14,14,14,14,14,14,14,14,14,14,14,14,14,14,14,14,14,14,
};


// gpioToPUDCLK
//	(Word) offset to the Pull Up Down Clock regsiter

static uint8_t gpioToPUDCLK [] = {
  38,38,38,38,38,38,38,38,38,38,38,38,38,38,38,38,38,38,38,38,38,38,38,38,38,38,38,38,38,38,38,38,
  39,39,39,39,39,39,39,39,39,39,39,39,39,39,39,39,39,39,39,39,39,39,39,39,39,39,39,39,39,39,39,39,
};

}  // namespace llhardware::io