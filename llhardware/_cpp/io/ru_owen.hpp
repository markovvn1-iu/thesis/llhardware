#pragma once

#include <vector>
#include <queue>
#include <mutex>
#include <atomic>

#include "core/core.hpp"
#include "serial.hpp"

namespace llhardware::io {

// The russian Owen protocol: https://owen.ru/
class RuOwen : public core::IOUpdate {
private:
    using QueueItem = std::pair<std::string, std::shared_ptr<core::StreamMaster>>;

    static const uint8_t PROTOCOL_LENGTH = 44;
    uint64_t timeout_ns;

    std::mutex data_mutex;
    std::queue<QueueItem> queue;

    std::atomic<int> state = 0; // 0 - nothing, 1 - need to start, 2 - writing, 3 - reading
    QueueItem active_message;
    uint8_t buf[PROTOCOL_LENGTH];
    uint8_t buf_i = 0;

    static void calc_crc(uint8_t data, uint8_t nbit, uint16_t* crc);
    void activate_next_message();
    void pack_package_byte(std::vector<char>* package, uint8_t byte, uint16_t* crc);
    uint8_t unpack_package_byte(const uint16_t& package_byte, uint16_t* crc);

protected:
    std::shared_ptr<Serial> serial;
    bool addr_len_8 = true;

    void on_attach() override;
    void on_detach() override;
    uint64_t update(int id, uint64_t update_time_ns, uint64_t cur_time_ns) override;
    void on_event(int fd, uint32_t flag) override;

public:
    struct PacketData {
        uint16_t addr;
        bool read_or_write;
        uint16_t cmd;
        std::vector<uint8_t> data;
    };

    RuOwen(std::shared_ptr<Serial> serial, bool addr_len_8 = true, uint64_t timeout_ns = 250000000);

    static uint16_t name2cmd(const std::string& name);

    inline bool get_addr_len_8() { return addr_len_8; };
    std::string make_packet(uint16_t addr, bool read_or_write, uint16_t cmd, std::vector<uint8_t> data);
    PacketData parse_packet(const std::string& packet);
    std::shared_ptr<core::Stream> send_packet(const std::string& packet);
};

};  // namespace llhardware::io