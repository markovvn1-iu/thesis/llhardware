#pragma once

#include <atomic>

#include "core/core.hpp"
#include "gpio.hpp"
#include "interfaces.hpp"

namespace llhardware::io {

class SoftPWM : public core::SimpleUpdate, public IPWMPulseWidth, public IPWMFillFactor {
private:
    bool state = false;
    uint64_t rest_time_ns = 0;

protected:
    std::shared_ptr<io::GPIO::IDigitalWritePin> pin;
    uint64_t pwm_period_ns;
    std::atomic<uint64_t> pwm_width = 0;

    void on_attach() override;
    uint64_t update(int id, uint64_t update_time_ns, uint64_t cur_time_ns) override;

public:
    SoftPWM(std::shared_ptr<io::GPIO::IDigitalWritePin> pin, uint64_t pwm_period_ns) : pin(pin), pwm_period_ns(pwm_period_ns) {}
    ~SoftPWM() { call_cancel(0); }

    uint64_t get_pulse_width() { return pwm_width; }
    void set_pulse_width(uint64_t width) { pwm_width = std::min(width, pwm_period_ns); };

    float get_fill_factor() { return pwm_width / (float)pwm_period_ns; }
    void set_fill_factor(float factor) { set_pulse_width((uint64_t)round(pwm_period_ns * factor)); }
};

};  // namespace llhardware::io