// Based on https://github.com/WiringPi/WiringPi/blob/master/wiringPi/wiringPi.c
#pragma once

#include <memory>
#include <mutex>
#include <string>
#include <stdexcept>

#include "core/core.hpp"


namespace llhardware::io {

class GPIOException : public std::runtime_error {  // TODO: Make better exceptions
    using std::runtime_error::runtime_error;
};

class GPIOLayoutException : public GPIOException {
public:
    GPIOLayoutException(const std::string& why) : GPIOException(
        "Unable to determine board revision from /proc/cpuinfo  -> " + why + " ->  You'd best google the error to find out why."
    ) {}
};

class GPIO {
public:
    enum GPIOMode {
        GPIO_MODE,
        PINS_MODE,
        PHYS_MODE,
    };

    enum DigitalValue {
        LOW = 0,
        HIGH = 1,
    };

    enum PullMode {
        PUD_OFF  = 0,
        PUD_DOWN = 1,
        PUD_UP   = 2,
    };

    enum InterruptMode {
        NONE = 0b00,
        FALLING = 0b01,
        RISING = 0b10,
        BOTH = 0b11,
    };

private:
    enum PinMode {
        INPUT      = 0,
        OUTPUT     = 1,
    };

    class LockedPin {
    public:
        const int pin;

        LockedPin(int pin) : pin(pin) {}
        ~LockedPin();
    };

    class Core {
    friend class LockedPin;
    private:
        std::mutex data_mutex;
        unsigned int piGpioPupOffset = 0;
        bool usingGpioMem = false;
        bool setuped = false;
        std::set<int> locked_pin;
        Core();

    public:
        struct Hardware {
            unsigned int* gpio;
            unsigned int* pwm;
            unsigned int* clk;
            unsigned int* pads;
            unsigned int* timer;
            unsigned int* timerIrqRaw;
        };

        std::mutex hw_mutex;
        /**
         * gpioLayout:
         * 1: A, B, Rev 1, 1.1
         * 2: A2, B2, A+, B+, CM, Pi2, Pi3, Zero, Zero W, Zero 2 W
         */
        int gpioLayout;
        int model, rev, maker;
        Hardware hw;

        Core(Core const&)            = delete;
        void operator=(Core const&)  = delete;
        static Core& get_instance();

        std::shared_ptr<LockedPin> lock_pin(int pin);
        void pull_control(int pin, PullMode mode);
        void sys_export(int pin);
        void sys_set_direction(int pin, const char* direction);  // in, out
        void sys_unexport(int pin);
    };

public:
    class IPin {
    private:
        const std::shared_ptr<LockedPin> locked_pin;
    protected:
        Core& core;
        IPin(std::shared_ptr<LockedPin> locked_pin) : locked_pin(locked_pin), pin(locked_pin->pin), core(Core::get_instance()) {}
    public:
        const int pin;
    };

    class IDigitalReadPin : virtual public IPin {
    public:
        using IPin::IPin;
        virtual DigitalValue digital_read() = 0;
    };

    class IPullUpDnControlPin : virtual public IPin {
    public:
        using IPin::IPin;
        virtual void pull_control(PullMode mode) = 0;
    };

    class IDigitalInterruptPin : virtual public IPin {
    protected:
        std::shared_ptr<core::StreamMaster> stream = core::StreamMaster::create();

    public:
        using IPin::IPin;
        virtual void setup_interrupt(InterruptMode mode) = 0;
        std::shared_ptr<core::Stream> get_interrupt_stream() { return stream; };
    };

    class IDigitalWritePin : virtual public IPin {
    public:
        using IPin::IPin;
        virtual void digital_write(DigitalValue value) = 0;
    };

    class MemInputPin : public IDigitalReadPin, public IPullUpDnControlPin {
    private:
        bool inverse;
        unsigned int* gpioLEV;   // GPIO Input level registers
        uint32_t gpioLEVmask;

    public:
        MemInputPin(std::shared_ptr<LockedPin> locked_pin, bool inverse);
        ~MemInputPin();
        MemInputPin(MemInputPin const&) = delete;
        void operator=(MemInputPin const&) = delete;

        DigitalValue digital_read() override;
        void pull_control(PullMode mode) override { core.pull_control(pin, mode); }
    };

    class MemOutputPin : public IDigitalWritePin {
    private:
        bool inverse;
        unsigned int* gpioSET;   // GPIO Set registers
        unsigned int* gpioCLR;   // GPIO Clear registers
        uint32_t gpioMask;

    public:
        MemOutputPin(std::shared_ptr<LockedPin> locked_pin, bool inverse);
        ~MemOutputPin();
        MemOutputPin(MemOutputPin const&) = delete;
        void operator=(MemOutputPin const&) = delete;

        void digital_write(DigitalValue value) override;
    };

    class SysInputPin : public IDigitalReadPin, public IPullUpDnControlPin, public IDigitalInterruptPin, public core::IOUpdate {
    private:
        int value_fd;
        bool inverse;

        void on_attach() override;
        void on_event(int fd, uint32_t flag) override;

    public:
        SysInputPin(std::shared_ptr<LockedPin> locked_pin, bool inverse);
        ~SysInputPin();
        SysInputPin(SysInputPin const&) = delete;
        void operator=(SysInputPin const&) = delete;

        DigitalValue digital_read() override;
        void pull_control(PullMode mode) override { IPin::core.pull_control(pin, mode); }
        void setup_interrupt(InterruptMode mode) override;
    };

    class SysOutputPin : public IDigitalWritePin {
    private:
        int value_fd;
        bool inverse;

    public:
        SysOutputPin(std::shared_ptr<LockedPin> locked_pin, bool inverse);
        ~SysOutputPin();
        SysOutputPin(SysOutputPin const&) = delete;
        void operator=(SysOutputPin const&) = delete;

        void digital_write(DigitalValue value) override;
    };

private:
    int* pinToGpio = NULL;
    Core& core;

protected:
    GPIO::GPIOMode mode;
    int get_pin(int pin) const;

public:
    GPIO(GPIO::GPIOMode mode);

    std::shared_ptr<MemInputPin> input(int pin, bool inverse = false);
    std::shared_ptr<SysInputPin> sys_input(int pin, bool inverse = false);
    std::shared_ptr<MemOutputPin> output(int pin, bool inverse = false, DigitalValue init_value = DigitalValue::LOW);
    std::shared_ptr<SysOutputPin> sys_output(int pin, bool inverse = false, DigitalValue init_value = DigitalValue::LOW);
};

}  // namespace llhardware::io