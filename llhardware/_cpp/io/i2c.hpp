#pragma once

#include <string>
#include <mutex>
#include <memory>
#include <vector>
#include <pybind11/pybind11.h>

#define I2C_SCAN_MODE_AUTO 0
#define I2C_SCAN_MODE_QUICK 1
#define I2C_SCAN_MODE_READ 2


namespace llhardware::io {

class I2C {
    friend class I2CDevice;

private:
    uint8_t current_addr = 255;
    std::mutex lock;
    int fd;
    unsigned long funcs;

    void check_i2c_functions(unsigned long required_funcs, const char* required_for);
    void i2c_smbus_access(uint8_t addr, char rw, uint8_t command, int size, uint8_t* data);

public:
    std::string filename;

    I2C(const std::string& filename);
    void close();

    std::vector<uint8_t> scan(uint8_t first = 0x08, uint8_t last = 0x77, int mode = I2C_SCAN_MODE_AUTO);
};

using PI2C = typename std::shared_ptr<I2C>;


class I2CDevice {
friend void init_module(pybind11::module &m);
private:
    PI2C core;
    uint8_t addr;

    uint8_t py_read_byte();
    uint8_t py_read_byte_data(uint8_t command);
    uint16_t py_read_word_data(uint8_t command);
    pybind11::bytes py_read_block_data(uint8_t command, uint8_t length);
    void py_write_block_data(uint8_t command, const std::string& value);

public:
    I2CDevice(PI2C core, uint8_t addr) : core(core), addr(addr) {}

    void read_byte(uint8_t* value);
    void write_byte(uint8_t value);

    void read_byte_data(uint8_t command, uint8_t* value);
    void write_byte_data(uint8_t command, uint8_t value);

    void read_word_data(uint8_t command, uint16_t* value);
    void write_word_data(uint8_t command, uint16_t value);

    void read_block_data(uint8_t command, uint8_t* value);  // value[0] - data length
    void write_block_data(uint8_t command, uint8_t* value);  // value[0] - data length
};

using PI2CDevice = typename std::shared_ptr<I2CDevice>;

};  // namespace llhardware::io
