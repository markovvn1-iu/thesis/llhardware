#include "serial.hpp"

#include <fcntl.h>
#include <cstring>
#include <unistd.h>
#include <sys/ioctl.h>
#include <termios.h>
#include <stdexcept>


using namespace llhardware::io;

uint32_t Serial::convert_baudrate(uint32_t baudrate) {
    switch (baudrate)
    {
        case      50:	return      B50;
        case      75:	return      B75;
        case     110:	return     B110;
        case     134:	return     B134;
        case     150:	return     B150;
        case     200:	return     B200;
        case     300:	return     B300;
        case     600:	return     B600;
        case    1200:	return    B1200;
        case    1800:	return    B1800;
        case    2400:	return    B2400;
        case    4800:	return    B4800;
        case    9600:	return    B9600;
        case   19200:	return   B19200;
        case   38400:	return   B38400;
        case   57600:	return   B57600;
        case  115200:	return  B115200;
        case  230400:	return  B230400;
        case  460800:	return  B460800;
        case  500000:	return  B500000;
        case  576000:	return  B576000;
        case  921600:	return  B921600;
        case 1000000:	return B1000000;
        case 1152000:	return B1152000;
        case 1500000:	return B1500000;
        case 2000000:	return B2000000;
        case 2500000:	return B2500000;
        case 3000000:	return B3000000;
        case 3500000:	return B3500000;
        case 4000000:	return B4000000;
    }
    throw std::runtime_error("Serial::convert_baudrate: Unknown baudrate (" + std::to_string(baudrate) + ")");
}

Serial::Serial(const std::string& filename, uint32_t baudrate)
    : filename(filename), baudrate(baudrate) {
    baudrate = convert_baudrate(baudrate);

    fd = open(filename.c_str(), O_RDWR | O_NOCTTY | O_NDELAY | O_NONBLOCK);
    if (fd < 0) throw std::runtime_error("Serial::Serial: Failed to open file " + filename + ": " + std::string(strerror(errno)));

    // Get and modify current options:
    {
        struct termios options;
        if (tcgetattr(fd, &options))
            throw std::runtime_error("Serial::Serial: Failed to get file attributes: " + std::string(strerror(errno)));

        cfmakeraw(&options);
        if (cfsetispeed(&options, baudrate) || cfsetospeed(&options, baudrate))
            throw std::runtime_error("Serial::Serial: Failed to set boundrate: " + std::string(strerror(errno)));

        options.c_cflag |= (CLOCAL | CREAD);
        options.c_cflag &= ~PARENB;
        options.c_cflag &= ~CSTOPB;
        options.c_cflag &= ~CSIZE;
        options.c_cflag |= CS8;
        options.c_lflag &= ~(ICANON | ECHO | ECHOE | ISIG);
        options.c_oflag &= ~OPOST;
        options.c_cc[VMIN]  = 0;
        options.c_cc[VTIME] = 100;  // Ten seconds (100 deciseconds)

        if (tcsetattr(fd, TCSANOW, &options))
            throw std::runtime_error("Serial::Serial: Failed to set file attributes: " + std::string(strerror(errno)));
    }

    usleep(10000);	// 10mS
}

void Serial::close() {
    if (fd < 0) return;
    ::close(fd);
    fd = -1;
}
