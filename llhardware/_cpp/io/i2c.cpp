#include "i2c.hpp"

#include <errno.h>
#include <fcntl.h>
#include <sys/ioctl.h>
#include <sys/types.h>
#include <unistd.h>
#include <cstring>

#include <linux/i2c.h>
#include <linux/i2c-dev.h>


using namespace llhardware::io;


I2C::I2C(const std::string& filename) : filename(filename) {
	if ((fd = open(filename.c_str(), O_RDWR)) < 0)
		throw std::runtime_error("I2C::I2C(): Failed to open " + filename + ": " + std::string(strerror(errno)));

	if (ioctl(fd, I2C_FUNCS, &funcs) < 0) {
		close();
		throw std::runtime_error("I2C::I2C(): Failed to get the adapter functionality matrix: " + std::string(strerror(errno)));
	}
}

void I2C::close() {
	if (fd < 0) return;
    ::close(fd);
	fd = -1;
}

void I2C::check_i2c_functions(unsigned long required_funcs, const char* required_for) {
	if (funcs & required_funcs == required_funcs) return;
	throw std::runtime_error("I2C::check_i2c_functions(): i2c adapter do not support functions required for " + std::string(required_for));
}

void I2C::i2c_smbus_access(uint8_t addr, char rw, uint8_t command, int size, uint8_t* data)
{
	if (fd < 0) throw std::runtime_error("I2C::i2c_smbus_access(): I2C device is not open");

	i2c_smbus_ioctl_data args;
	args.read_write = rw;
	args.command = command;
	args.size = size;
	args.data = (i2c_smbus_data*)data;

	const std::lock_guard<std::mutex> _lock(lock);
	if (addr != current_addr) {  // set slave address if neaded
		if (ioctl(fd, I2C_SLAVE, addr) < 0)
			throw std::runtime_error("I2C::i2c_smbus_access(): Unable to select I2C device with address " + std::to_string(addr) + ": " + std::string(strerror(errno)));
		current_addr = addr;
	}
	if (ioctl(fd, I2C_SMBUS, &args) != 0)
		throw std::runtime_error("I2C::i2c_smbus_access(): Failed to access i2c: " + std::string(strerror(errno)));
}

std::vector<uint8_t> I2C::scan(uint8_t first, uint8_t last, int mode) {
	std::vector<uint8_t> res;

	const std::lock_guard<std::mutex> _lock(lock);
	for (int i = first; i <= last; i++) {
		int cmd = mode;
		if (mode == I2C_SCAN_MODE_AUTO)
			cmd = ((i >= 0x30 && i <= 0x37) || (i >= 0x50 && i <= 0x5F)) ? I2C_SCAN_MODE_READ : I2C_SCAN_MODE_QUICK;
		
		if (cmd == I2C_SCAN_MODE_READ && !(funcs & I2C_FUNC_SMBUS_READ_BYTE)) continue;
		if (cmd == I2C_SCAN_MODE_QUICK && !(funcs & I2C_FUNC_SMBUS_QUICK)) continue;

		/* Set slave address */
		if (ioctl(fd, I2C_SLAVE, i) < 0) {
			if (errno == EBUSY) continue;
			throw std::runtime_error("I2C::scan(): Unable to select I2C device with address " + std::to_string(i) + ": " + std::string(strerror(errno)));
		}

		/* Probe this address */
		uint8_t data;
		i2c_smbus_ioctl_data args;
		if (cmd == I2C_SCAN_MODE_QUICK) {
			args.read_write = I2C_SMBUS_WRITE;
			args.command = 0;
			args.size = I2C_SMBUS_QUICK;
			args.data = NULL;
		} else if (cmd == I2C_SCAN_MODE_READ) {
			args.read_write = I2C_SMBUS_READ;
			args.command = 0;
			args.size = I2C_SMBUS_BYTE;
			args.data = (i2c_smbus_data*)&data;
		} else {
			throw std::runtime_error("I2C::scan(): Unknown scanning mode");
		}
	
		if (ioctl(fd, I2C_SMBUS, &args) < 0) continue;
		res.push_back(i);
	}

	return res;
}




void I2CDevice::read_byte(uint8_t* value) {
	core->check_i2c_functions(I2C_FUNC_SMBUS_READ_BYTE, "READ_BYTE");
	core->i2c_smbus_access(addr, I2C_SMBUS_READ, 0, I2C_SMBUS_BYTE, value);
}

void I2CDevice::write_byte(uint8_t value) {
	core->check_i2c_functions(I2C_FUNC_SMBUS_WRITE_BYTE, "WRITE_BYTE");
	core->i2c_smbus_access(addr, I2C_SMBUS_WRITE, value, I2C_SMBUS_BYTE, NULL);
}

void I2CDevice::read_byte_data(uint8_t command, uint8_t* value) {
	core->check_i2c_functions(I2C_FUNC_SMBUS_READ_BYTE_DATA, "READ_BYTE_DATA");
	core->i2c_smbus_access(addr, I2C_SMBUS_READ, command, I2C_SMBUS_BYTE_DATA, value);
}

void I2CDevice::write_byte_data(uint8_t command, uint8_t value) {
	core->check_i2c_functions(I2C_FUNC_SMBUS_WRITE_BYTE_DATA, "WRITE_BYTE_DATA");
	core->i2c_smbus_access(addr, I2C_SMBUS_WRITE, command, I2C_SMBUS_BYTE_DATA, &value);
}

void I2CDevice::read_word_data(uint8_t command, uint16_t* value) {
	core->check_i2c_functions(I2C_FUNC_SMBUS_READ_WORD_DATA, "READ_WORD_DATA");
	core->i2c_smbus_access(addr, I2C_SMBUS_READ, command, I2C_SMBUS_WORD_DATA, (uint8_t*)value);
}

void I2CDevice::write_word_data(uint8_t command, uint16_t value) {
	core->check_i2c_functions(I2C_FUNC_SMBUS_WRITE_WORD_DATA, "WRITE_WORD_DATA");
	core->i2c_smbus_access(addr, I2C_SMBUS_WRITE, command, I2C_SMBUS_WORD_DATA, (uint8_t*)&value);
}

void I2CDevice::read_block_data(uint8_t command, uint8_t* value) {
	core->check_i2c_functions(I2C_FUNC_SMBUS_READ_I2C_BLOCK, "READ_I2C_BLOCK");
	core->i2c_smbus_access(addr, I2C_SMBUS_READ, command, I2C_SMBUS_I2C_BLOCK_DATA, value);
}

void I2CDevice::write_block_data(uint8_t command, uint8_t* value) {
	core->check_i2c_functions(I2C_FUNC_SMBUS_WRITE_I2C_BLOCK, "WRITE_I2C_BLOCK");
	core->i2c_smbus_access(addr, I2C_SMBUS_WRITE, command, I2C_SMBUS_I2C_BLOCK_DATA, value);
}

uint8_t I2CDevice::py_read_byte() {
	uint8_t res;
	read_byte(&res);
	return res;
}

uint8_t I2CDevice::py_read_byte_data(uint8_t command) {
	uint8_t res;
	read_byte_data(command, &res);
	return res;
}

uint16_t I2CDevice::py_read_word_data(uint8_t command) {
	uint16_t res;
	read_word_data(command, &res);
	return res;
}

pybind11::bytes I2CDevice::py_read_block_data(uint8_t command, uint8_t length) {
	uint8_t data[length + 1];
	data[0] = length;
	read_block_data(command, data);
	return pybind11::bytes((char*)data + 1, length);
}

void I2CDevice::py_write_block_data(uint8_t command, const std::string& value) {
	if (value.size() > 255)
		throw std::runtime_error("PyI2CDevice::py_write_block_data(): value length is too long (must be <= 255)");
	uint8_t data[value.size() + 1];
	data[0] = value.size();
	memcpy(data + 1, value.c_str(), value.size());
	write_block_data(command, data);
}
