#include "ru_owen.hpp"

#include <sys/ioctl.h>

#include "types/types.hpp"

using namespace llhardware::io;
namespace core = llhardware::core;

RuOwen::RuOwen(std::shared_ptr<Serial> serial, bool addr_len_8, uint64_t timeout_ns)
    : serial(serial), addr_len_8(addr_len_8), timeout_ns(timeout_ns) {
    if (timeout_ns < 1000000) throw std::runtime_error("RuOwen::RuOwen: timeout_ns must be >= 1 000 000");
}

void RuOwen::on_attach() {
    activate_next_message();
}

void RuOwen::on_detach() {
    if (active_message.second) active_message.second->close();
    active_message.second.reset();
    state = 0;
}

uint64_t RuOwen::update(int id, uint64_t update_time_ns, uint64_t cur_time_ns) {
    activate_next_message();  // TODO: send tineout error
    return 0;
}

void RuOwen::activate_next_message() {
    if (active_message.second) active_message.second->close();

    bool has_message = false;
    {
        std::lock_guard lock(data_mutex);
        if (!queue.empty()) {
            has_message = true;
            active_message = queue.front(); queue.pop();
        }
    }

    if (!has_message) {
        active_message.second.reset();
        unset_event_mask(serial->get_fd());
        state = 0;
        return;
    }
    state = 2; buf_i = 0;
    ioctl(serial->get_fd(), TCFLSH, 0);  // flush receive buffer
    set_event_mask(serial->get_fd(), EPOLLOUT);
    call_later(0, timeout_ns);  // setup timeout for the request
}

void RuOwen::on_event(int fd, uint32_t flag) {
    if (fd != serial->get_fd()) return;
    if (state == 1) activate_next_message();  // called from send_message

    if ((flag & EPOLLOUT) && (state == 2)) {
        const std::string& msg = active_message.first;
        ssize_t size = serial->write(msg.c_str() + buf_i, msg.size() - buf_i);
        buf_i += size;
        if (buf_i >= msg.size()) {
            state = 3; buf_i = 0;
            set_event_mask(serial->get_fd(), EPOLLIN);
            return;
        }
    }
    if ((flag & EPOLLIN) && (state == 3)) {
        ssize_t size = serial->read(buf, PROTOCOL_LENGTH - buf_i);
        if (buf_i == 0 && size > 0 && buf[0] != '#') {
            for (int i = 1; i < size; i++) {
                if (buf[i] == '#') {
                    memmove(buf, buf + i, size - i);
                    buf_i = size - i;
                    break;
                }
            }
        } else {
            buf_i += size;
        }

        if (buf_i > 0 && (buf[buf_i - 1] == '\r' || buf_i == PROTOCOL_LENGTH)) {
            if (buf[buf_i - 1] == '\r') {
                active_message.second->push(type::StrDataItem::create(std::string((char*)buf + 1, buf_i - 2)));
            }
            activate_next_message();
        }
    }
}

void RuOwen::calc_crc(uint8_t data, uint8_t nbit, uint16_t* crc_ptr) {
    uint16_t& crc = *crc_ptr;
    for (int i = 0; i < nbit; i++, data <<= 1) {
        if ((data ^ (crc >> 8)) & 0x80) {
            crc <<= 1; crc ^= 0x8F57;
        } else {
            crc <<= 1;
        }
    }
}

uint16_t RuOwen::name2cmd(const std::string& name) {
    uint16_t hash = 0;
    uint char_count = 1;
    for (uint i = 0; (i < name.size()) || (char_count <= 4); i++, char_count++) {
        if (char_count > 4) throw std::runtime_error("RuOwen::name2cmd(): Name has more than 4 characters (not counting dots)");

        const char& ch = (i < name.size()) ? name[i] : ' ';
        uint8_t char_for_hash = 0;
        switch (ch) {
            case '0' ... '9': char_for_hash = ch - '0';      break;
            case 'A' ... 'Z': char_for_hash = ch - 'A' + 10; break;
            case 'a' ... 'z': char_for_hash = ch - 'a' + 10; break;
            case '-':         char_for_hash = 10 + 26;       break;
            case '_':         char_for_hash = 10 + 26 + 1;   break;
            case '/':         char_for_hash = 10 + 26 + 2;   break;
            case ' ':         char_for_hash = 10 + 26 + 3;   break;
            default: throw std::runtime_error("RuOwen::name2cmd(): Name has illegal characters");
        }
        char_for_hash *= 2;
        if (i + 1 < name.size() && name[i + 1] == '.') { i++; char_for_hash++; }
        calc_crc(char_for_hash << 1, 7, &hash);
    }
    return hash;
}

void RuOwen::pack_package_byte(std::vector<char>* package, uint8_t byte, uint16_t* crc) {
    package->push_back('G' + ((byte >> 4) & 0xF));
    package->push_back('G' + (byte & 0xF));
    if (crc != NULL) calc_crc(byte, 8, crc);
}

uint8_t RuOwen::unpack_package_byte(const uint16_t& package_byte, uint16_t* crc) {
    char* package = (char*)&package_byte;
    uint8_t res = ((package[0] - 'G') << 4) | (package[1] - 'G');
    if (crc != NULL) calc_crc(res, 8, crc);
    return res;
}

std::string RuOwen::make_packet(uint16_t addr, bool read_or_write, uint16_t cmd, std::vector<uint8_t> data) {
    if (data.size() > 15) throw std::runtime_error("RuOwen::make_packet(): data too long (max 15 bytes)");
    if (addr_len_8 && addr > 255) throw std::runtime_error("RuOwen::RuOwen: if addr_len_8 is true, addr must be in range [0, 255]");
    if (!addr_len_8 && addr > 2047) throw std::runtime_error("RuOwen::RuOwen: if addr_len_8 is false, addr must be in range [0, 2047]");

    uint16_t crc = 0;
    std::vector<char> res;
    res.reserve(12 + data.size() * 2);

    if (addr_len_8) {
        pack_package_byte(&res, addr & 0xFF, &crc);
        pack_package_byte(&res, (read_or_write ? 1 << 4 : 0) | data.size(), &crc);
    } else {
        pack_package_byte(&res, (addr >> 3) & 0xFF, &crc);
        pack_package_byte(&res, ((addr & 0x07) << 5) | (read_or_write ? 1 << 4 : 0) | data.size(), &crc);
    }

    pack_package_byte(&res, (cmd >> 8) & 0xFF, &crc);
    pack_package_byte(&res, cmd & 0xFF, &crc);
    for (const uint8_t& v : data) pack_package_byte(&res, v, &crc);
    pack_package_byte(&res, (crc >> 8) & 0xFF, NULL);
    pack_package_byte(&res, crc & 0xFF, NULL);

    return std::string(&res[0], res.size());
}

RuOwen::PacketData RuOwen::parse_packet(const std::string& packet_str) {
    if (packet_str.size() % 2 == 1 || packet_str.size() < 12 || packet_str.size() > PROTOCOL_LENGTH - 2)
        throw std::runtime_error("RuOwen::parse_packet(): incorrect packet length");

    RuOwen::PacketData res;
    uint16_t crc = 0;
    const uint16_t* packet = (uint16_t*)packet_str.c_str();
    
    uint8_t byte0 = unpack_package_byte(packet[0], &crc), byte1 = unpack_package_byte(packet[1], &crc);
    res.addr = addr_len_8 ? byte0 : (byte0 << 3 | byte1 >> 5);
    res.read_or_write = (byte1 >> 4) & 1;
    uint8_t size = byte1 & 0xF;
    if (packet_str.size() != 12 + size * 2)
        throw std::runtime_error("RuOwen::parse_packet(): incorrect internal packet length");
    
    res.cmd = ((uint16_t)unpack_package_byte(packet[2], &crc) << 8) | unpack_package_byte(packet[3], &crc);
    res.data.reserve(size);
    for (uint8_t i = 0; i < size; i++) res.data.push_back(unpack_package_byte(packet[4 + i], &crc));

    uint16_t packet_crc = ((uint16_t)unpack_package_byte(packet[4 + size], NULL) << 8) | unpack_package_byte(packet[5 + size], NULL);
    if (packet_crc != crc) throw std::runtime_error("RuOwen::parse_packet(): checksum mismatch");

    return res;
}

std::shared_ptr<core::Stream> RuOwen::send_packet(const std::string& packet) {
    std::shared_ptr<core::StreamMaster> stream = core::StreamMaster::create();
    {
        std::lock_guard lock(data_mutex);
        queue.push({'#' + packet + '\r', stream});
    }
    if (state == 0) {
        state = 1;
        set_event_mask(serial->get_fd(), EPOLLOUT);
    }
    return stream;
}
