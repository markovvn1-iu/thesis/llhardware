#pragma once

#include <string>
#include <cstdint>
#include <memory>

#include <pybind11/pybind11.h>

#include "gpio.hpp"


namespace llhardware::io {

class Serial {
friend void init_module(pybind11::module &m);
private:
    uint32_t convert_baudrate(uint32_t baudrate);

    ssize_t py_write(std::string buf) { return write(buf.data(), buf.size()); }
    pybind11::bytes py_read(size_t nbytes) { char buf[nbytes]; ssize_t res = read(buf, nbytes); return pybind11::bytes(buf, res); }

protected:
    int fd = -1;

    std::string filename;
    uint32_t baudrate;

public:
    Serial(const std::string& filename, uint32_t baudrate);
    ~Serial() { close(); }
    void close();

    inline int get_fd() { return fd; }
    inline std::string get_filename() { return filename; };
    inline uint32_t get_baudrate() { return baudrate; };

    inline ssize_t read(void* buf, size_t nbytes) { return ::read(fd, buf, nbytes); }
    ssize_t write(const void* buf, size_t n) { return ::write(fd, buf, n); }
};

}  // namespace llhardware::io
