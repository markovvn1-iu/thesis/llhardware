#include "ibus.hpp"

#include "types/types.hpp"

using namespace llhardware::io;

IBus::IBus(std::shared_ptr<Serial> serial, uint8_t channels, uint64_t timeout_ns)
    : serial(serial), channels(channels), timeout_ns(timeout_ns), stream(core::StreamMaster::create()) {
    if (channels < 1 || channels > 14) throw std::runtime_error("IBus::IBus: channels must be in range [1, 14]");
    if (timeout_ns < 1000000) throw std::runtime_error("IBus::IBus: timeout_ns must be >= 1 000 000");
}

IBus::~IBus() {
    unset_event_mask(serial->get_fd());
}

void IBus::on_attach() {
    set_event_mask(serial->get_fd(), EPOLLIN);
    call_later(0, timeout_ns);
}

uint64_t IBus::update(int id, uint64_t update_time_ns, uint64_t cur_time_ns) {
    stream->push(type::IBusChannelsDataItem::create(std::vector<uint16_t>(), false));
    return update_time_ns + timeout_ns;
}

void IBus::on_event(int fd, uint32_t flag) {
    if (fd != serial->get_fd()) return;

    if (flag & EPOLLIN) {
        uint8_t* new_buf = buf + buf_i;
        ssize_t size = serial->read(new_buf, PROTOCOL_LENGTH - buf_i);
        if (buf_i < 2) {
            for (int i = 0; i < size; i++) {
                if (buf_i < 1) {
                    if (new_buf[i] == PROTOCOL_LENGTH) {
                        buf_i = 1;
                        buf[0] = new_buf[i];
                    }
                } else {
                    if (new_buf[i] == PROTOCOL_COMMAND) {
                        buf_i = size - i + 1;
                        if (new_buf + i != buf + 1) memmove(buf + 1, new_buf + i, size - i);
                        break;
                    } else if (new_buf[i] != PROTOCOL_LENGTH) {
                        buf_i = 0;
                    }
                }
            }
        } else {
            buf_i += size;
        }

        if (buf_i != 32) return;
        buf_i = 0;

        uint16_t* channels_raw = (uint16_t*)buf;

        uint16_t checksum = 0xFFFF;
        for (int i = 0; i < PROTOCOL_LENGTH - 2; i++) checksum -= buf[i];
        if (checksum != channels_raw[15]) return;

        call_later(0, timeout_ns);
        std::vector<uint16_t> value(channels_raw + 1, channels_raw + 1 + channels);
        stream->push(type::IBusChannelsDataItem::create(value, true));
    }
}
