#include "soft_pwm.hpp"

using namespace llhardware::io;

void SoftPWM::on_attach() {
    state = false;
    call_soon(0);  // Start update chain
}

uint64_t SoftPWM::update(int id, uint64_t update_time_ns, uint64_t cur_time_ns) {
    if (id != 0) return 0;

    if (state && rest_time_ns != 0) {  // If current state is HIGH
        pin->digital_write(io::GPIO::DigitalValue::LOW);  // Set it to LOW
        state = false;
        return update_time_ns + rest_time_ns;  // Wait for the next period
    } else {
        uint64_t width = pwm_width;
        if (width == 0) {  // If duration of HIGH state is 0
            if (state) pin->digital_write(io::GPIO::DigitalValue::LOW);
            return update_time_ns + pwm_period_ns;  // Wait for a whole period
        }
        rest_time_ns = pwm_period_ns - width;
        if (!state) pin->digital_write(io::GPIO::DigitalValue::HIGH);
        state = true;
        return update_time_ns + width;  // Wait for the next state update
    }
}
