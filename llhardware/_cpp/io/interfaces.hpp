#pragma once

#include <cstdint>

namespace llhardware::io {

class IPWMPulseWidth {
public:
    virtual uint64_t get_pulse_width() = 0;
    virtual void set_pulse_width(uint64_t width) = 0;
};

class IPWMFillFactor {
public:
    virtual float get_fill_factor() = 0;
    virtual void set_fill_factor(float factor) = 0;
};

};  // namespace llhardware::io