#include <pybind11/pybind11.h>

namespace llhardware::controllers {
    void init_module(pybind11::module &m);
}; // namespace llhardware::controllers
