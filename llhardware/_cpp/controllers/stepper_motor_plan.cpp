#include "stepper_motor_plan.hpp"

using namespace llhardware::controllers;

void StepperMotorPlanController::on_detach() {
    std::lock_guard lock(new_plan_mutex);
    if (plan) new_plan_start_pos = plan->get_pos(core::get_time_ns());
    plan.reset();
    has_new_plan = new_plan != nullptr;
}

uint64_t StepperMotorPlanController::update(int id, uint64_t update_time_ns, uint64_t cur_time_ns) {
    if (id == 1) {  // Setup new planner
        {
            std::lock_guard lock(new_plan_mutex);
            if (!has_new_plan) return 0;
            plan = new_plan->compile(cur_time_ns, plan ? plan->get_pos(cur_time_ns) : new_plan_start_pos);
            has_new_plan = false;
        }
        call_soon(0);
        return 0;
    }
    if (id != 0) return 0;

    int64_t target_steps;
    uint64_t next_update_ns = plan->compute(cur_time_ns, &target_steps);
    motor->move_to(target_steps);
    return next_update_ns;
}

void StepperMotorPlanController::set_plan(std::shared_ptr<planners::IFrozenStepperMotorPlan> plan) {
    {
        std::lock_guard lock(new_plan_mutex);
        new_plan = plan;
        has_new_plan = true;
    }
    call_soon(1);  // Setup new planner
}

std::pair<uint64_t, std::pair<int64_t, float>> StepperMotorPlanController::get_cur_state() {
    uint64_t cur_time = core::get_time_ns();
    std::lock_guard lock(new_plan_mutex);
    if (plan) return {cur_time, plan->get_pos(cur_time)};
    return {cur_time, new_plan_start_pos};
}