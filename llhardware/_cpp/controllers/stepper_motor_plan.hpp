#pragma once

#include <memory>
#include <atomic>

#include "core/core.hpp"
#include "devices/devices.hpp"
#include "planners/planners.hpp"


namespace llhardware::controllers {

class StepperMotorPlanController : public core::SimpleUpdate {
private:
    std::shared_ptr<devices::IStepperMotor> motor;
    std::shared_ptr<planners::ICompiledStepperMotorPlan> plan;

    std::mutex new_plan_mutex;
    std::pair<int64_t, float> new_plan_start_pos = {0, 0};
    std::shared_ptr<planners::IFrozenStepperMotorPlan> new_plan;
    bool has_new_plan = false;

    void on_attach() override { call_soon(1); }
    void on_detach() override;
    uint64_t update(int id, uint64_t update_time_ns, uint64_t cur_time_ns) override;

public:
    StepperMotorPlanController(std::shared_ptr<devices::IStepperMotor> motor) : motor(motor) {}

    void set_plan(std::shared_ptr<planners::IFrozenStepperMotorPlan> plan);
    std::pair<uint64_t, std::pair<int64_t, float>> get_cur_state();
};

};  // namespace llhardware::controllers
