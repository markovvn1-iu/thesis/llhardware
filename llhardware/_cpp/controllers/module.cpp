#include <pybind11/pybind11.h>
#include <pybind11/stl.h>

#include "controllers.hpp"

namespace py = pybind11;
namespace llhardware::controllers {

void init_module(py::module &m) {
    py::class_<StepperMotorPlanController, std::shared_ptr<StepperMotorPlanController>, core::SimpleUpdate>(m, "StepperMotorPlanController")
        .def(py::init<std::shared_ptr<devices::IStepperMotor>>())
        .def("set_plan", &StepperMotorPlanController::set_plan)
        .def_property_readonly("cur_state", &StepperMotorPlanController::get_cur_state);
}

};  // namespace llhardware::controllers
