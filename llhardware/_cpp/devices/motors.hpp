#pragma once

#include <memory>
#include <atomic>

#include "core/core.hpp"
#include "io/io.hpp"
#include "interfaces.hpp"


namespace llhardware::devices {

class DigitalServo {
protected:
    std::shared_ptr<io::IPWMPulseWidth> pwm;
    std::pair<float, float> pos_range;
    std::pair<uint64_t, uint64_t> pulse_width_range;

    uint64_t pos2width(float pos);
    float width2pos(uint64_t width);

public:
    DigitalServo(std::shared_ptr<io::IPWMPulseWidth> pwm, std::pair<float, float> pos_range, std::pair<uint64_t, uint64_t> pulse_width_range);

    float get_pos() { return width2pos(pwm->get_pulse_width()); }
    void set_pos(float pos) { pwm->set_pulse_width(pos2width(pos)); }
};


class AnalogServo {
protected:
    std::shared_ptr<io::IPWMFillFactor> pwm;
    std::pair<float, float> pos_range;
    std::pair<float, float> fill_factor_range;

    float pos2factor(float pos);
    float factor2pos(float width);

public:
    AnalogServo(std::shared_ptr<io::IPWMFillFactor> pwm, std::pair<float, float> pos_range, std::pair<float, float> fill_factor_range);

    float get_pos() { return factor2pos(pwm->get_fill_factor()); }
    void set_pos(float pos) { pwm->set_fill_factor(pos2factor(pos)); }
};


class ElectronicSpeedController : protected DigitalServo {
public:
    ElectronicSpeedController(std::shared_ptr<io::IPWMPulseWidth> pwm, std::pair<float, float> speed_range, std::pair<uint64_t, uint64_t> pulse_width_range)
        : DigitalServo(pwm, speed_range, pulse_width_range) {}

    float get_speed() { return width2pos(pwm->get_pulse_width()); }
    void set_speed(float pos) { pwm->set_pulse_width(pos2width(pos)); }
};

class GPIOStepperMotor : public core::SimpleUpdate, public IStepperMotor {
private:
    std::shared_ptr<io::GPIO::IDigitalWritePin> dir, step;
    uint32_t gpio_delay_ns = 1000;

    bool step_last_edge = false;  // true - rasing edge, false - falling edge
    std::atomic<int64_t> target_steps = 0, real_steps = 0;
    int direction = 0; // 1 - positive, -1 - negative
    uint64_t next_gpio_access_ns = 0;

    void on_attach() override { call_soon(0); }
    uint64_t update(int id, uint64_t update_time_ns, uint64_t cur_time_ns) override;

public:
    GPIOStepperMotor(std::shared_ptr<io::GPIO::IDigitalWritePin> step, std::shared_ptr<io::GPIO::IDigitalWritePin> dir, uint32_t gpio_delay_ns = 1000)
        : step(step), dir(dir), gpio_delay_ns(gpio_delay_ns) {}
    ~GPIOStepperMotor() { call_cancel(0); }

    void move(int32_t steps) override { move_to(target_steps + steps); };
    void move_to(int64_t target_steps) override;
    int64_t get_steps() { return real_steps; }
};

};  // namespace llhardware::devices
