#include "motors.hpp"

using namespace llhardware::devices;

DigitalServo::DigitalServo(std::shared_ptr<io::IPWMPulseWidth> pwm, std::pair<float, float> pos_range, std::pair<uint64_t, uint64_t> pulse_width_range)
    : pwm(pwm), pos_range(pos_range), pulse_width_range(pulse_width_range) {
    if (pos_range.first == pos_range.second)
        throw std::logic_error("Start and end in pos_range must be different");
}

uint64_t DigitalServo::pos2width(float pos) {
    if (pos_range.first < pos_range.second) {
        if (pos <= pos_range.first) return pulse_width_range.first;
        if (pos >= pos_range.second) return pulse_width_range.second;
    } else {
        if (pos >= pos_range.first) return pulse_width_range.first;
        if (pos <= pos_range.second) return pulse_width_range.second;
    }
    if (pulse_width_range.first <= pulse_width_range.second) {
        return (int64_t)(round((pos - pos_range.first) / (pos_range.second - pos_range.first) * (pulse_width_range.second - pulse_width_range.first))) + pulse_width_range.first;    
    } else {
        return (int64_t)(round((pos - pos_range.first) / (pos_range.first - pos_range.second) * (pulse_width_range.first - pulse_width_range.second))) + pulse_width_range.first;
    }
}

float DigitalServo::width2pos(uint64_t width) {
    if (pulse_width_range.first < pulse_width_range.second) {
        if (width <= pulse_width_range.first) return pos_range.first;
        if (width >= pulse_width_range.second) return pos_range.second;
    } else {
        if (width >= pulse_width_range.first) return pos_range.first;
        if (width <= pulse_width_range.second) return pos_range.second;
    }
    if (pulse_width_range.first <= pulse_width_range.second) {
        return (width - pulse_width_range.first) * (pos_range.second - pos_range.first) / (pulse_width_range.second - pulse_width_range.first) + pos_range.first;
    } else {
        return (width - pulse_width_range.first) * (pos_range.first - pos_range.second) / (pulse_width_range.first - pulse_width_range.second) + pos_range.first;
    }
    
}


AnalogServo::AnalogServo(std::shared_ptr<io::IPWMFillFactor> pwm, std::pair<float, float> pos_range, std::pair<float, float> fill_factor_range)
    : pwm(pwm), pos_range(pos_range), fill_factor_range(fill_factor_range) {
    if (pos_range.first == pos_range.second)
        throw std::logic_error("Start and end in pos_range must be different");
}

float AnalogServo::pos2factor(float pos) {
    if (pos_range.first < pos_range.second) {
        if (pos <= pos_range.first) return fill_factor_range.first;
        if (pos >= pos_range.second) return fill_factor_range.second;
    } else {
        if (pos >= pos_range.first) return fill_factor_range.first;
        if (pos <= pos_range.second) return fill_factor_range.second;
    }
    return (pos - pos_range.first) / (pos_range.second - pos_range.first) * (fill_factor_range.second - fill_factor_range.first) + fill_factor_range.first;
}

float AnalogServo::factor2pos(float width) {
    if (fill_factor_range.first < fill_factor_range.second) {
        if (width <= fill_factor_range.first) return pos_range.first;
        if (width >= fill_factor_range.second) return pos_range.second;
    } else {
        if (width >= fill_factor_range.first) return pos_range.first;
        if (width <= fill_factor_range.second) return pos_range.second;
    }
    return (width - fill_factor_range.first) * (pos_range.second - pos_range.first) / (fill_factor_range.second - fill_factor_range.first) + pos_range.first;
}

void GPIOStepperMotor::move_to(int64_t target_steps) {
    if (this->target_steps == target_steps) return;
    this->target_steps = target_steps;
    call_soon(0);
}

uint64_t GPIOStepperMotor::update(int id, uint64_t update_time_ns, uint64_t cur_time_ns) {
    // Do not access GPIO faster than gpio_delay_ns
    if (next_gpio_access_ns > cur_time_ns) return next_gpio_access_ns;
    next_gpio_access_ns = cur_time_ns + gpio_delay_ns;

    if (step_last_edge) {
        step_last_edge = false;
        step->digital_write(io::GPIO::DigitalValue::LOW);
        return next_gpio_access_ns;
    }

    int64_t delta = target_steps - real_steps;
    if (delta == 0) return 0;

    int cur_direction = delta > 0 ? 1 : -1;
    if (direction != cur_direction) {
        direction = cur_direction;
        dir->digital_write(cur_direction > 0 ? io::GPIO::DigitalValue::HIGH : io::GPIO::DigitalValue::LOW);
        return next_gpio_access_ns;
    }

    step_last_edge = true;
    step->digital_write(io::GPIO::DigitalValue::HIGH);
    real_steps += cur_direction;
    return next_gpio_access_ns;
}
