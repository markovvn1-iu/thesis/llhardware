#include <pybind11/pybind11.h>
#include <pybind11/stl.h>

#include "devices.hpp"

namespace py = pybind11;
namespace llhardware::devices {

void init_module(py::module &m) {
    py::class_<DigitalServo>(m, "DigitalServo")
        .def(py::init<std::shared_ptr<io::IPWMPulseWidth>, std::pair<float, float>, std::pair<uint64_t, uint64_t>>())
        .def_property("position", &DigitalServo::get_pos, &DigitalServo::set_pos);

    py::class_<AnalogServo>(m, "AnalogServo")
        .def(py::init<std::shared_ptr<io::IPWMFillFactor>, std::pair<float, float>, std::pair<float, float>>())
        .def_property("position", &AnalogServo::get_pos, &AnalogServo::set_pos);

    py::class_<ElectronicSpeedController>(m, "ElectronicSpeedController")
        .def(py::init<std::shared_ptr<io::IPWMPulseWidth>, std::pair<float, float>, std::pair<uint64_t, uint64_t>>())
        .def_property("speed", &ElectronicSpeedController::get_speed, &ElectronicSpeedController::set_speed);

    py::class_<IStepperMotor, std::shared_ptr<IStepperMotor>>(m, "IStepperMotor")
        .def("move", &IStepperMotor::move)
        .def("move_to", &IStepperMotor::move_to)
        .def_property_readonly("steps", &IStepperMotor::get_steps);

    py::class_<GPIOStepperMotor, std::shared_ptr<GPIOStepperMotor>, IStepperMotor, core::SimpleUpdate>(m, "GPIOStepperMotor")
        .def(py::init<std::shared_ptr<io::GPIO::IDigitalWritePin>, std::shared_ptr<io::GPIO::IDigitalWritePin>, uint32_t>());
}

};  // namespace llhardware::devices
