from llhardware._cpp.core import SimpleUpdate
from llhardware._cpp.io import GPIO, IPWMFillFactor, IPWMPulseWidth

class DigitalServo:
    position: float
    def __init__(
        self, pwm: IPWMPulseWidth, pos_range: tuple[float, float], pulse_width_range: tuple[int, int]
    ) -> None: ...

class AnalogServo:
    position: float
    def __init__(
        self, pwm: IPWMFillFactor, pos_range: tuple[float, float], fill_factor_range: tuple[float, float]
    ) -> None: ...

class ElectronicSpeedController:
    speed: float
    def __init__(
        self, pwm: IPWMPulseWidth, speed_range: tuple[float, float], pulse_width_range: tuple[int, int]
    ) -> None: ...

class IStepperMotor:
    @property
    def steps(self) -> int: ...
    def move(self, steps: int) -> None: ...
    def move_to(self, target_steps: int) -> None: ...

class GPIOStepperMotor(SimpleUpdate, IStepperMotor):
    def __init__(self, step: GPIO.IDigitalWritePin, dir_: GPIO.IDigitalWritePin, gpio_delay_ns: int = 1000) -> None: ...
