#include <pybind11/pybind11.h>

namespace llhardware::devices {
    void init_module(pybind11::module &m);
}; // namespace llhardware::devices
