#pragma once

#include <cstdint>

namespace llhardware::devices {

class IStepperMotor {
public:
    virtual void move(int32_t steps) = 0;
    virtual void move_to(int64_t target_steps) = 0;
    virtual int64_t get_steps() = 0;
};

};  // namespace llhardware::devices