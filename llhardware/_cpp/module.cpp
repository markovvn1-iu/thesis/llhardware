#include <pybind11/pybind11.h>
#include "io/module.hpp"
#include "core/module.hpp"
#include "types/module.hpp"
#include "devices/module.hpp"
#include "controllers/module.hpp"
#include "planners/module.hpp"

namespace py = pybind11;
using namespace llhardware;

PYBIND11_MODULE(_cpp, m) {
    py::module core = m.def_submodule("core");
    core::init_module(core);

    py::module types = m.def_submodule("types");
    type::init_module(types);

    py::module io = m.def_submodule("io");
    io::init_module(io);

    py::module devices = m.def_submodule("devices");
    devices::init_module(devices);

    py::module planners = m.def_submodule("planners");
    planners::init_module(planners);

    py::module controllers = m.def_submodule("controllers");
    controllers::init_module(controllers);
}
