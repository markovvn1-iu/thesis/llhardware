#include "simple_plans.hpp"
#include "utils.hpp"

using namespace llhardware::planners;

std::shared_ptr<IFrozenStepperMotorPlan> SimplePlan::freeze(uint64_t start_time_ns, std::pair<int64_t, float> start_pos, double warmup_k) {
    double delta_pos = (start_vel + end_vel) / 2 * duration;
    return std::make_shared<FrozenStepperMotorSimplePlan>(start_time_ns, start_time_ns + (uint64_t)(duration * 1e9), start_pos, start_pos + delta_pos, start_vel / 1e9, end_vel / 1e9, warmup_k);
}

std::shared_ptr<IFrozenStepperMotorPlan> GroupPlan::freeze(uint64_t start_time_ns, std::pair<int64_t, float> start_pos, double warmup_k) {
    uint64_t end_time_ns = start_time_ns;
    std::pair<int64_t, float> end_pos = start_pos;

    std::vector<std::shared_ptr<IFrozenStepperMotorPlan>> plans;
    for (auto& plan : this->plans) {
        std::shared_ptr<IFrozenStepperMotorPlan> frozen_plan = plan->freeze(end_time_ns, end_pos, warmup_k);
        plans.push_back(frozen_plan);
        end_time_ns = frozen_plan->end_time_ns;
        end_pos = frozen_plan->end_pos;
    }

    return std::make_shared<FrozenStepperMotorGroupPlan>(start_time_ns, end_time_ns, start_pos, end_pos, plans);
}




void FrozenStepperMotorSimplePlan::append_parabola(
    uint64_t start_time_ns, uint64_t end_time_ns,
    std::pair<int64_t, float> start_pos, std::pair<int64_t, float> end_pos,
    double start_vel, double end_vel,
    std::vector<std::shared_ptr<ICompiledStepperMotorPlan>>* res
) {
    double accel = (end_vel - start_vel) / (end_time_ns - start_time_ns);
    double t_min = -start_vel / accel;
    double t_delta = std::pow(abs(accel), -0.75);
    double border1 = t_min - t_delta, border2 = t_min + t_delta;

    if (border1 > end_time_ns - start_time_ns || border2 < 0) {
        res->push_back(std::make_shared<CompiledStepperMotorNewtonParabolaPlan>(start_time_ns, end_time_ns, start_pos, end_pos, start_vel, end_vel, accel));
        return;
    }

    if (0 < border1) {
        res->push_back(std::make_shared<CompiledStepperMotorNewtonParabolaPlan>(
            start_time_ns,
            start_time_ns + (uint64_t)(border1),
            start_pos,
            start_pos + (start_vel * border1 + accel * border1 * border1 / 2),
            start_vel,
            start_vel + accel * border1,
            accel
        ));
    }
    if (border1 < end_time_ns - start_time_ns && 0 < border2) {
        bool b1 = 0 < border1, b2 = border2 < end_time_ns - start_time_ns;
        res->push_back(std::make_shared<CompiledStepperMotorParabolaPlan>(
            b1 ? start_time_ns + (uint64_t)(border1) : start_time_ns,
            b2 ? start_time_ns + (uint64_t)(border2) : end_time_ns,
            b1 ? start_pos + (start_vel * border1 + accel * border1 * border1 / 2) : start_pos,
            b2 ? start_pos + (start_vel * border2 + accel * border2 * border2 / 2) : end_pos,
            b1 ? start_vel + accel * border1 : start_vel,
            b2 ? start_vel + accel * border2 : end_vel,
            accel
        ));
    }
    if (border2 < end_time_ns - start_time_ns) {
        res->push_back(std::make_shared<CompiledStepperMotorNewtonParabolaPlan>(
            start_time_ns + (uint64_t)(border2),
            end_time_ns,
            start_pos + (start_vel * border2 + accel * border2 * border2 / 2),
            end_pos,
            start_vel + accel * border2,
            end_vel,
            accel
        ));
    }
}

void FrozenStepperMotorSimplePlan::_compile(uint64_t* cur_time_ns_ptr, std::pair<int64_t, float>* cur_pos_ptr, std::vector<std::shared_ptr<ICompiledStepperMotorPlan>>* res) {
    uint64_t& cur_time_ns = *cur_time_ns_ptr;
    std::pair<int64_t, float>& cur_pos = *cur_pos_ptr;
    
    if (cur_time_ns >= end_time_ns) return;
    if (cur_time_ns < start_time_ns) cur_time_ns = start_time_ns;

    double x_delta = (double)(cur_pos.first - start_pos.first) + cur_pos.second - get_target_pos(cur_time_ns);
    double warmup_k = x_delta > 0 ? this->warmup_k : -this->warmup_k;
    uint64_t warmup_stop_ns = (uint64_t)(x_delta / warmup_k);

    double cur_vel = start_vel;

    if (warmup_stop_ns > 1000) {
        uint64_t warmup_end_time_ns = std::min(cur_time_ns + warmup_stop_ns, end_time_ns);
        std::pair<int64_t, float> warmup_end_pos = get_pos(warmup_end_time_ns);
        if (start_vel == end_vel) {
            res->push_back(std::make_shared<CompiledStepperMotorConstSpeedPlan>(
                cur_time_ns,
                warmup_end_time_ns,
                cur_pos,
                warmup_end_pos,
                start_vel - warmup_k)
            );
        } else {
            double warmup_start_vel = start_vel + (end_vel - start_vel) / (end_time_ns - start_time_ns) * (cur_time_ns - start_time_ns);
            double warmup_end_vel = start_vel + (end_vel - start_vel) / (end_time_ns - start_time_ns) * (warmup_end_time_ns - start_time_ns);
            append_parabola(
                cur_time_ns,
                warmup_end_time_ns,
                cur_pos,
                warmup_end_pos,
                warmup_start_vel - warmup_k,
                warmup_end_vel - warmup_k,
                res
            );
            cur_vel = warmup_end_vel;
        }
        cur_time_ns = warmup_end_time_ns;
        cur_pos = warmup_end_pos;
    }
    if (cur_time_ns >= end_time_ns) return;

    if (start_vel == end_vel) {
        res->push_back(std::make_shared<CompiledStepperMotorConstSpeedPlan>(
            cur_time_ns,
            end_time_ns,
            cur_pos,
            end_pos,
            start_vel)
        );
    } else {
        append_parabola(
            cur_time_ns,
            end_time_ns,
            cur_pos,
            end_pos,
            cur_vel,
            end_vel,
            res
        );
    }
    cur_time_ns = end_time_ns;
    cur_pos = end_pos;
}

// to get correct result need to add start_pos.first
double FrozenStepperMotorSimplePlan::get_target_pos(uint64_t cur_time_ns) const {
    double t = cur_time_ns - start_time_ns;
    return start_pos.second + start_vel * t + accel * t * t / 2;
}

std::pair<int64_t, float> FrozenStepperMotorSimplePlan::get_pos(uint64_t cur_time_ns) {
    if (cur_time_ns <= start_time_ns) return start_pos;
    if (cur_time_ns >= end_time_ns) return end_pos;
    double pos = get_target_pos(cur_time_ns);
    int64_t pos_part = static_cast<int64_t>(floor(pos));
    return {start_pos.first + pos_part, (float)(pos - pos_part)};
}




FrozenStepperMotorGroupPlan::FrozenStepperMotorGroupPlan(
    uint64_t start_time_ns, uint64_t end_time_ns,
    std::pair<int64_t, float> start_pos, std::pair<int64_t, float> end_pos,
    std::vector<std::shared_ptr<IFrozenStepperMotorPlan>> plans
) : IFrozenStepperMotorPlan(start_time_ns, end_time_ns, start_pos, end_pos) {
    uint64_t time_ns = start_time_ns;
    for (auto& p : plans) {
        if (p->end_time_ns <= time_ns) continue;
        if (p->start_time_ns > time_ns) time_ns = p->start_time_ns;
        this->plans[time_ns] = p;
        time_ns = p->end_time_ns;
    }
}

void FrozenStepperMotorGroupPlan::_compile(uint64_t* cur_time_ns, std::pair<int64_t, float>* cur_pos, std::vector<std::shared_ptr<ICompiledStepperMotorPlan>>* res) {
    for (auto& p : plans) p.second->_compile(cur_time_ns, cur_pos, res);
}

std::pair<int64_t, float> FrozenStepperMotorGroupPlan::get_pos(uint64_t cur_time_ns) {
    if (cur_time_ns <= start_time_ns) return start_pos;
    if (cur_time_ns >= end_time_ns) return end_pos;
    auto it = plans.upper_bound(cur_time_ns);
    if (it == plans.begin()) return start_pos;
    it--;
    return it->second->get_pos(cur_time_ns);
}
