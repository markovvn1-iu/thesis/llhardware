#pragma once

#include <vector>
#include <cstdint>

std::pair<int64_t, float> operator+(std::pair<int64_t, float> p, double delta);
