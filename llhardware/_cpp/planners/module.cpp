#include <pybind11/pybind11.h>
#include <pybind11/stl.h>

#include <memory>

#include "planners.hpp"

namespace py = pybind11;
namespace llhardware::planners {

void init_module(py::module &m) {
    py::class_<ICompiledStepperMotorPlan, std::shared_ptr<ICompiledStepperMotorPlan>>(m, "ICompiledStepperMotorPlan")
        .def_readonly("start_time_ns", &ICompiledStepperMotorPlan::start_time_ns)
        .def_readonly("end_time_ns", &ICompiledStepperMotorPlan::end_time_ns)
        .def_readonly("start_pos", &ICompiledStepperMotorPlan::start_pos)
        .def_readonly("end_pos", &ICompiledStepperMotorPlan::end_pos)
        .def("get_pos", &ICompiledStepperMotorPlan::get_pos)
        .def("compute", [](std::shared_ptr<ICompiledStepperMotorPlan> self, uint64_t cur_time_ns){
            int64_t res;
            return std::pair<uint64_t, int64_t>{self->compute(cur_time_ns, &res), res};
        });

    py::class_<IFrozenStepperMotorPlan, std::shared_ptr<IFrozenStepperMotorPlan>>(m, "IFrozenStepperMotorPlan")
        .def("get_pos", &IFrozenStepperMotorPlan::get_pos)
        .def("compile", &IFrozenStepperMotorPlan::compile)
        .def_readonly("start_time_ns", &IFrozenStepperMotorPlan::start_time_ns)
        .def_readonly("end_time_ns", &IFrozenStepperMotorPlan::end_time_ns)
        .def_readonly("start_pos", &IFrozenStepperMotorPlan::start_pos)
        .def_readonly("end_pos", &IFrozenStepperMotorPlan::end_pos);

    py::class_<Plan, std::shared_ptr<Plan>>(m, "Plan")
        .def("freeze", &Plan::freeze);

    py::class_<SimplePlan, std::shared_ptr<SimplePlan>, Plan>(m, "SimplePlan")
        .def(py::init<double, double, double>())
        .def("freeze", &SimplePlan::freeze)
        .def_readonly("start_vel", &SimplePlan::start_vel)
        .def_readonly("end_vel", &SimplePlan::end_vel)
        .def_readonly("duration", &SimplePlan::duration);

    py::class_<FrozenStepperMotorSimplePlan, std::shared_ptr<FrozenStepperMotorSimplePlan>, IFrozenStepperMotorPlan>(m, "FrozenStepperMotorSimplePlan")
        .def_readonly("start_vel", &FrozenStepperMotorSimplePlan::start_vel)
        .def_readonly("end_vel", &FrozenStepperMotorSimplePlan::end_vel);

    py::class_<GroupPlan, std::shared_ptr<GroupPlan>, Plan>(m, "GroupPlan")
        .def(py::init<std::vector<std::shared_ptr<Plan>>>())
        .def("freeze", &GroupPlan::freeze)
        .def_readonly("plans", &GroupPlan::plans);

    py::class_<FrozenStepperMotorGroupPlan, std::shared_ptr<FrozenStepperMotorGroupPlan>, IFrozenStepperMotorPlan>(m, "FrozenStepperMotorGroupPlan");
}

};  // namespace llhardware::planners
