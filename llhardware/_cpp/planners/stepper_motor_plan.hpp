#pragma once

#include <vector>

#include "core/core.hpp"
#include "interfaces.hpp"


namespace llhardware::planners {

class CompiledStepperMotorPlan : public ICompiledStepperMotorPlan {
private:
    std::map<uint64_t, std::shared_ptr<ICompiledStepperMotorPlan>> plans;

public:
    CompiledStepperMotorPlan(
        uint64_t start_time_ns, uint64_t end_time_ns,
        std::pair<int64_t, float> start_pos, std::pair<int64_t, float> end_pos,
        std::vector<std::shared_ptr<ICompiledStepperMotorPlan>> plans
    );

    std::pair<int64_t, float> get_pos(uint64_t cur_time_ns) override;
    uint64_t compute(uint64_t cur_time_ns, int64_t* target_steps) override;
};



class CompiledStepperMotorParabolaPlan : public ICompiledStepperMotorPlan {
private:
    const double accel;
    double t_mid;
    uint64_t pos_mid;
    double d_mult, d_part;

    double get_target_pos(uint64_t cur_time_ns) const;  // to get correct result need to add start_pos.first

public:
    const double start_vel, end_vel;

    CompiledStepperMotorParabolaPlan(
        uint64_t start_time_ns, uint64_t end_time_ns,
        std::pair<int64_t, float> start_pos, std::pair<int64_t, float> end_pos,
        double start_vel, double end_vel, double accel
    );

    std::pair<int64_t, float> get_pos(uint64_t cur_time_ns) override;
    uint64_t compute(uint64_t cur_time_ns, int64_t* target_steps) override;
};

class CompiledStepperMotorNewtonParabolaPlan : public ICompiledStepperMotorPlan {
private:
    double accel;

    double get_target_pos(uint64_t cur_time_ns) const;  // to get correct result need to add start_pos.first

public:
    const double start_vel, end_vel;

    CompiledStepperMotorNewtonParabolaPlan(
        uint64_t start_time_ns, uint64_t end_time_ns,
        std::pair<int64_t, float> start_pos, std::pair<int64_t, float> end_pos,
        double start_vel, double end_vel, double accel
    );

    std::pair<int64_t, float> get_pos(uint64_t cur_time_ns) override;
    uint64_t compute(uint64_t cur_time_ns, int64_t* target_steps) override;
};

class CompiledStepperMotorConstSpeedPlan : public ICompiledStepperMotorPlan {
private:
    double d_mult, d_part;

    double get_target_pos(uint64_t cur_time_ns) const;  // to get correct result need to add start_pos.first

public:
    const double vel;

    CompiledStepperMotorConstSpeedPlan(
        uint64_t start_time_ns, uint64_t end_time_ns,
        std::pair<int64_t, float> start_pos, std::pair<int64_t, float> end_pos,
        double vel
    );

    std::pair<int64_t, float> get_pos(uint64_t cur_time_ns) override;
    uint64_t compute(uint64_t cur_time_ns, int64_t* target_steps) override;
};

};  // namespace llhardware::planners
