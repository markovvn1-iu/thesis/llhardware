#pragma once

#include "stepper_motor_plan.hpp"

#include <memory>


namespace llhardware::planners {

class Plan {
public:
    virtual std::shared_ptr<IFrozenStepperMotorPlan> freeze(uint64_t start_time_ns, std::pair<int64_t, float> start_pos, double warmup_k) = 0;
};

class SimplePlan : public Plan {
public:
    const double start_vel, end_vel;  // steps in seconds
    const double duration;  // seconds

    SimplePlan(double start_vel, double end_vel, double duration)
        : start_vel(start_vel), end_vel(end_vel), duration(duration) {}

    std::shared_ptr<IFrozenStepperMotorPlan> freeze(uint64_t start_time_ns, std::pair<int64_t, float> start_pos, double warmup_k) override;
};

class GroupPlan : public Plan {
public:
    const std::vector<std::shared_ptr<Plan>> plans;

    GroupPlan(std::vector<std::shared_ptr<Plan>> plans) : plans(plans) {}

    std::shared_ptr<IFrozenStepperMotorPlan> freeze(uint64_t start_time_ns, std::pair<int64_t, float> start_pos, double warmup_k) override;
};




class FrozenStepperMotorSimplePlan : public IFrozenStepperMotorPlan {
protected:
    const double accel;

    void append_parabola(
        uint64_t start_time_ns, uint64_t end_time_ns,
        std::pair<int64_t, float> start_pos, std::pair<int64_t, float> end_pos,
        double start_vel, double end_vel,
        std::vector<std::shared_ptr<ICompiledStepperMotorPlan>>* res
    );
    void _compile(uint64_t* cur_time_ns, std::pair<int64_t, float>* cur_pos, std::vector<std::shared_ptr<ICompiledStepperMotorPlan>>* res) override;
    double get_target_pos(uint64_t cur_time_ns) const;  // to get correct result need to add start_pos.first

public:
    const double start_vel, end_vel;
    const double warmup_k;

    FrozenStepperMotorSimplePlan(
        uint64_t start_time_ns, uint64_t end_time_ns,
        std::pair<int64_t, float> start_pos, std::pair<int64_t, float> end_pos,
        double start_vel, double end_vel, double warmup_k
    ) : IFrozenStepperMotorPlan(start_time_ns, end_time_ns, start_pos, end_pos),
        start_vel(start_vel), end_vel(end_vel), warmup_k(warmup_k),
        accel((end_vel - start_vel) / (end_time_ns - start_time_ns)) {}

    std::pair<int64_t, float> get_pos(uint64_t cur_time_ns) override;
};

class FrozenStepperMotorGroupPlan : public IFrozenStepperMotorPlan {
private:
    std::map<uint64_t, std::shared_ptr<IFrozenStepperMotorPlan>> plans;

protected:
    void _compile(uint64_t* cur_time_ns, std::pair<int64_t, float>* cur_pos, std::vector<std::shared_ptr<ICompiledStepperMotorPlan>>* res) override;

public:
    FrozenStepperMotorGroupPlan(
        uint64_t start_time_ns, uint64_t end_time_ns,
        std::pair<int64_t, float> start_pos, std::pair<int64_t, float> end_pos,
        std::vector<std::shared_ptr<IFrozenStepperMotorPlan>> plans
    );

    std::pair<int64_t, float> get_pos(uint64_t cur_time_ns) override;
};


};  // namespace llhardware::planners
