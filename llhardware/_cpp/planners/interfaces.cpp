#include "interfaces.hpp"
#include "stepper_motor_plan.hpp"

using namespace llhardware::planners;

std::shared_ptr<ICompiledStepperMotorPlan> IFrozenStepperMotorPlan::compile(uint64_t cur_time_ns, std::pair<int64_t, float> cur_pos) {
    std::vector<std::shared_ptr<ICompiledStepperMotorPlan>> plans;
    uint64_t end_time_ns = cur_time_ns;
    std::pair<int64_t, float> end_pos = cur_pos;
    _compile(&end_time_ns, &end_pos, &plans);
    if (plans.size() == 0) return std::make_shared<CompiledStepperMotorPlan>(cur_time_ns, cur_time_ns, cur_pos, cur_pos, plans);
    return std::make_shared<CompiledStepperMotorPlan>(
        plans[0]->start_time_ns,
        plans[plans.size() - 1]->end_time_ns,
        plans[0]->start_pos,
        plans[plans.size() - 1]->end_pos,
        plans
    );
}
