#include "stepper_motor_plan.hpp"
#include "utils.hpp"

#include <algorithm>

using namespace llhardware::planners;

CompiledStepperMotorPlan::CompiledStepperMotorPlan(
    uint64_t start_time_ns, uint64_t end_time_ns,
    std::pair<int64_t, float> start_pos, std::pair<int64_t, float> end_pos,
    std::vector<std::shared_ptr<ICompiledStepperMotorPlan>> plans
) : ICompiledStepperMotorPlan(start_time_ns, end_time_ns, start_pos, end_pos) {
    uint64_t time_ns = start_time_ns;
    for (auto& p : plans) {
        if (p->end_time_ns <= time_ns) continue;
        if (p->start_time_ns > time_ns) time_ns = p->start_time_ns;
        this->plans[time_ns] = p;
        time_ns = p->end_time_ns;
    }
}

std::pair<int64_t, float> CompiledStepperMotorPlan::get_pos(uint64_t cur_time_ns) {
    if (cur_time_ns <= start_time_ns) return start_pos;
    if (cur_time_ns >= end_time_ns) return end_pos;
    auto it = plans.upper_bound(cur_time_ns);
    if (it == plans.begin()) return start_pos;
    it--;
    return it->second->get_pos(cur_time_ns);
}

uint64_t CompiledStepperMotorPlan::compute(uint64_t cur_time_ns, int64_t* target_steps) {
    if (cur_time_ns < start_time_ns) { *target_steps = start_pos.first + (int64_t)round(start_pos.second); return start_time_ns; }
    if (cur_time_ns >= end_time_ns) { *target_steps = end_pos.first + (int64_t)round(end_pos.second); return 0; }

    auto it = plans.upper_bound(cur_time_ns);
    if (it == plans.begin()) { *target_steps = start_pos.first + (int64_t)round(start_pos.second); return start_time_ns; };
    it--;
    uint64_t next_update_ns = it->second->compute(cur_time_ns, target_steps);
    if (next_update_ns > 0) return next_update_ns;
    while (true) {
        it++;
        if (it == plans.end()) break;
        int64_t new_target_steps;
        next_update_ns = it->second->compute(it->second->start_time_ns, &new_target_steps);
        if (new_target_steps != *target_steps) return it->second->start_time_ns;
        if (next_update_ns > 0) return next_update_ns;
    }
    return 0;
}





CompiledStepperMotorParabolaPlan::CompiledStepperMotorParabolaPlan(
    uint64_t start_time_ns, uint64_t end_time_ns,
    std::pair<int64_t, float> start_pos, std::pair<int64_t, float> end_pos,
    double start_vel, double end_vel, double accel
) : ICompiledStepperMotorPlan(start_time_ns, end_time_ns, start_pos, end_pos), start_vel(start_vel), end_vel(end_vel), accel(accel) {

    if (end_vel == start_vel)
        throw std::runtime_error("CompiledStepperMotorParabolaPlan::CompiledStepperMotorParabolaPlan(): failed to create: end_vel == start_vel");

    d_mult = 1 / accel;
    t_mid = -start_vel * d_mult;
    pos_mid = (int64_t)round(start_pos.second - start_vel * start_vel / 2 * d_mult);
    d_part = t_mid * t_mid - 2 * d_mult * start_pos.second;
}

// to get correct result need to add start_pos.first
double CompiledStepperMotorParabolaPlan::get_target_pos(uint64_t cur_time_ns) const {
    double t = cur_time_ns - start_time_ns;
    return start_pos.second + start_vel * t + accel * t * t / 2;
}

std::pair<int64_t, float> CompiledStepperMotorParabolaPlan::get_pos(uint64_t cur_time_ns) {
    if (cur_time_ns <= start_time_ns) return start_pos;
    if (cur_time_ns >= end_time_ns) return end_pos;
    double pos = get_target_pos(cur_time_ns);
    int64_t pos_part = static_cast<int64_t>(floor(pos));
    return {start_pos.first + pos_part, (float)(pos - pos_part)};
}

uint64_t CompiledStepperMotorParabolaPlan::compute(uint64_t cur_time_ns, int64_t* target_steps) {
    if (cur_time_ns < start_time_ns) { *target_steps = start_pos.first + (int64_t)round(start_pos.second); return start_time_ns; }
    if (cur_time_ns >= end_time_ns) { *target_steps = end_pos.first + (int64_t)round(end_pos.second); return 0; }

    int64_t cur_pos = round(get_target_pos(cur_time_ns));
    *target_steps = start_pos.first + cur_pos;

    uint64_t t = cur_time_ns - start_time_ns;
    bool parabola_part = (t >= t_mid) || (cur_pos == pos_mid);  // true - right, false - left
    int64_t target_pos = parabola_part ^ (d_mult > 0) ? 2 * cur_pos - 1 : 2 * cur_pos + 1;
    double D2 = d_mult * target_pos + d_part;
    uint64_t next_update_ns = (uint64_t)(t_mid + (D2 <= 0 ? 0 : parabola_part ? sqrt(D2) : -sqrt(D2)));
    next_update_ns += start_time_ns + 10;
    return next_update_ns < end_time_ns ? next_update_ns : 0;
}





// to get correct result need to add start_pos.first
double CompiledStepperMotorNewtonParabolaPlan::get_target_pos(uint64_t cur_time_ns) const {
    double t = cur_time_ns - start_time_ns;
    return start_pos.second + start_vel * t + accel * t * t / 2;
}

CompiledStepperMotorNewtonParabolaPlan::CompiledStepperMotorNewtonParabolaPlan(
    uint64_t start_time_ns, uint64_t end_time_ns,
    std::pair<int64_t, float> start_pos, std::pair<int64_t, float> end_pos,
    double start_vel, double end_vel, double accel
) : ICompiledStepperMotorPlan(start_time_ns, end_time_ns, start_pos, end_pos), start_vel(start_vel), end_vel(end_vel), accel(accel) {
    if (end_vel == start_vel)
        throw std::runtime_error("CompiledStepperMotorNewtonParabolaPlan::CompiledStepperMotorNewtonParabolaPlan(): failed to create: end_vel == start_vel");
    if (!(end_vel > 0 && start_vel > 0) && !(end_vel < 0 && start_vel < 0))
        throw std::runtime_error("CompiledStepperMotorNewtonParabolaPlan::CompiledStepperMotorNewtonParabolaPlan(): failed to create: incorrect end_vel and start_vel");
};

std::pair<int64_t, float> CompiledStepperMotorNewtonParabolaPlan::get_pos(uint64_t cur_time_ns) {
    if (cur_time_ns <= start_time_ns) return start_pos;
    if (cur_time_ns >= end_time_ns) return end_pos;
    double pos = get_target_pos(cur_time_ns);
    int64_t pos_part = static_cast<int64_t>(floor(pos));
    return {start_pos.first + pos_part, (float)(pos - pos_part)};
}

uint64_t CompiledStepperMotorNewtonParabolaPlan::compute(uint64_t cur_time_ns, int64_t* target_steps) {
    if (cur_time_ns < start_time_ns) { *target_steps = start_pos.first + (int64_t)round(start_pos.second); return start_time_ns; }
    if (cur_time_ns >= end_time_ns) { *target_steps = end_pos.first + (int64_t)round(end_pos.second); return 0; }

    double cur_pos = get_target_pos(cur_time_ns);
    int64_t cur_pos_part = round(cur_pos);
    cur_pos -= cur_pos_part;
    *target_steps = start_pos.first + cur_pos_part;

    uint64_t t = cur_time_ns - start_time_ns;
    double delta_pos = start_vel > 0 ? 0.5 - cur_pos : -0.5 - cur_pos;

    uint64_t next_update_ns = (uint64_t)(delta_pos / (start_vel + accel * t));
    next_update_ns += cur_time_ns + 10;
    return next_update_ns < end_time_ns ? next_update_ns : 0;
}






CompiledStepperMotorConstSpeedPlan::CompiledStepperMotorConstSpeedPlan(
    uint64_t start_time_ns, uint64_t end_time_ns,
    std::pair<int64_t, float> start_pos, std::pair<int64_t, float> end_pos,
    double vel
) : ICompiledStepperMotorPlan(start_time_ns, end_time_ns, start_pos, end_pos), vel(vel) {

    if (vel != 0) {
        d_mult = 1 / (2 * vel);
        d_part = -(start_pos.second * 2) * d_mult;
    }
}

// to get correct result need to add start_pos.first
double CompiledStepperMotorConstSpeedPlan::get_target_pos(uint64_t cur_time_ns) const {
    double t = cur_time_ns - start_time_ns;
    return start_pos.second + vel * t;
}

std::pair<int64_t, float> CompiledStepperMotorConstSpeedPlan::get_pos(uint64_t cur_time_ns) {
    if (cur_time_ns <= start_time_ns) return start_pos;
    if (cur_time_ns >= end_time_ns) return end_pos;
    double pos = get_target_pos(cur_time_ns);
    int64_t pos_part = static_cast<int64_t>(floor(pos));
    return {start_pos.first + pos_part, (float)(pos - pos_part)};
}

uint64_t CompiledStepperMotorConstSpeedPlan::compute(uint64_t cur_time_ns, int64_t* target_steps) {
    if (cur_time_ns < start_time_ns) { *target_steps = start_pos.first + (int64_t)round(start_pos.second); return start_time_ns; }
    if (cur_time_ns >= end_time_ns) { *target_steps = end_pos.first + (int64_t)round(end_pos.second); return 0; }

    int64_t cur_pos = round(get_target_pos(cur_time_ns));
    *target_steps = start_pos.first + cur_pos;
    if (vel == 0) return 0;

    int64_t target_pos = d_mult > 0 ? 2 * cur_pos + 1 : 2 * cur_pos - 1;
    uint64_t next_update_ns = (uint64_t)(target_pos * d_mult + d_part);
    next_update_ns += start_time_ns + 10;
    return next_update_ns < end_time_ns ? next_update_ns : 0;
}
