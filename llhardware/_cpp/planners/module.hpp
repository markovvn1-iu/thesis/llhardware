#include <pybind11/pybind11.h>

namespace llhardware::planners {
    void init_module(pybind11::module &m);
}; // namespace llhardware::planners
