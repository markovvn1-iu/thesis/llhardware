#pragma once

#include <cstdint>
#include <memory>
#include <vector>

namespace llhardware::planners {

class ICompiledStepperMotorPlan {
public:
    const uint64_t start_time_ns, end_time_ns;
    const std::pair<int64_t, float> start_pos, end_pos;

    ICompiledStepperMotorPlan(
        uint64_t start_time_ns, uint64_t end_time_ns,
        std::pair<int64_t, float> start_pos, std::pair<int64_t, float> end_pos
    ) : start_time_ns(start_time_ns), end_time_ns(end_time_ns), start_pos(start_pos), end_pos(end_pos) {};

    virtual std::pair<int64_t, float> get_pos(uint64_t cur_time_ns) = 0;
    virtual uint64_t compute(uint64_t cur_time_ns, int64_t* target_steps) = 0;
};

class IFrozenStepperMotorPlan {
public:
    const uint64_t start_time_ns, end_time_ns;
    const std::pair<int64_t, float> start_pos, end_pos;

    IFrozenStepperMotorPlan(
        uint64_t start_time_ns, uint64_t end_time_ns,
        std::pair<int64_t, float> start_pos, std::pair<int64_t, float> end_pos
    ) : start_time_ns(start_time_ns), end_time_ns(end_time_ns), start_pos(start_pos), end_pos(end_pos) {};

    std::shared_ptr<ICompiledStepperMotorPlan> compile(uint64_t cur_time_ns, std::pair<int64_t, float> cur_pos);
    virtual void _compile(uint64_t* cur_time_ns, std::pair<int64_t, float>* cur_pos, std::vector<std::shared_ptr<ICompiledStepperMotorPlan>>* res) = 0;
    virtual std::pair<int64_t, float> get_pos(uint64_t cur_time_ns) = 0;
};


};  // namespace llhardware::planners