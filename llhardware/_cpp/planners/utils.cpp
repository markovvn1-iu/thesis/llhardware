#include "utils.hpp"

#include <cmath>

std::pair<int64_t, float> operator+(std::pair<int64_t, float> p, double delta) {
    double p_part = p.second + delta;
    int64_t p_int_part = static_cast<int64_t>(floor(p_part));
    return {p.first + p_int_part, (float)(p_part - p_int_part)};
}