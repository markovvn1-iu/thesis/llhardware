#include <pybind11/pybind11.h>

#include "controllers/module.hpp"
#include "stream/module.hpp"
#include "thread/module.hpp"


namespace py = pybind11;
namespace llhardware::core {

void init_module(py::module &m) {
    py::module controllers = m.def_submodule("controllers");
    controllers::init_module(controllers);
    init_module_thread(m);
    init_module_stream(m);
}

};  // namespace llhardware::io
