#pragma once

#include <mutex>
#include <memory>

#include "core/controllers/controllers.hpp"


namespace llhardware::core {

namespace ct = controllers;

class SimpleUpdate : public ct::CollectionItem, public ct::PeriodicUpdateItem {
friend class ThreadUpdaterSimple;
private:
    using ct::BaseItem::mutex;
    using ct::CollectionItem::_attach_self;
    using ct::CollectionItem::_detach_self;
    using ct::PeriodicUpdateItem::_attach_self;
    using ct::PeriodicUpdateItem::_detach_self;

    bool _detach() override {
        bool res = ct::BaseItem::_detach_self();
        ct::CollectionItem::_detach_self();
        ct::PeriodicUpdateItem::_detach_self();
        return res;
    }

    template<class ThreadCore>
    void attach(std::shared_ptr<SimpleUpdate> self, std::shared_ptr<ThreadCore> core) {
        std::lock_guard lock(mutex);
        _detach();
        ct::PeriodicUpdateItem::_attach_self(self, core);
        ct::CollectionItem::_attach_self(self, core);
        ct::BaseItem::_attach_self(self, core);
    }

public:
    virtual ~SimpleUpdate() { detach(); }

    using ct::BaseItem::is_attached;
    using ct::BaseItem::detach;
};


class ThreadUpdaterSimple {
protected:
    template<class ThreadCore>
    inline void _add(std::shared_ptr<SimpleUpdate> obj, std::shared_ptr<ThreadCore> core) { obj->attach(obj, core); }

public:
    virtual void add(std::shared_ptr<SimpleUpdate> obj) = 0;
};

};  // namespace llhardware::core
