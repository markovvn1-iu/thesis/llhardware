#pragma once

#include <mutex>
#include <memory>
#include <thread>
#include <map>

#include "base_update.hpp"
#include "simple_update.hpp"
#include "io_update.hpp"


namespace llhardware::core {

namespace ct = controllers;

class EventThreadUpdaterCore : public ct::CollectionCore, public ct::PeriodicUpdateCore, public ct::FileUpdateCore {
private:
    struct WorkerArgs {
        std::thread thr;
        int epoll_fd;
        int event_fd;
        int timer_fd;
        bool is_working = true;
        std::shared_ptr<EventThreadUpdaterCore> self;
    };

    std::recursive_mutex thread_mutex;
    std::mutex data_mutex;
    std::map<int, uint32_t> fd2mask;
    std::shared_ptr<WorkerArgs> thread_data = NULL;

    void _epoll_register(int epoll_fd, int fd, uint32_t mask);
    void _epoll_modify(int epoll_fd, int fd, uint32_t mask);
    void _epoll_unregister(int epoll_fd, int fd);

    void _worker_thread(std::shared_ptr<WorkerArgs>* raw_args);
    void _on_set_event_mask(int fd, uint32_t mask) override;
    void _on_next_update_ns_changed() override { std::lock_guard lock(data_mutex); _send_event_to_thread(); }

    void _send_event_to_thread();

public:
    void start(std::shared_ptr<EventThreadUpdaterCore> self);
    void stop();
};


class EventThreadUpdater : public ThreadUpdaterBase, public ThreadUpdaterSimple, public ThreadUpdaterIO {
private:
    std::shared_ptr<EventThreadUpdaterCore> core;

public:
    EventThreadUpdater() : core(std::make_shared<EventThreadUpdaterCore>()) {}

    void start() override { core->start(core); }
    void stop() override { core->stop(); }

    void add(std::shared_ptr<SimpleUpdate> obj) override { if (obj) ThreadUpdaterSimple::_add(obj, core); }
    void add(std::shared_ptr<IOUpdate> obj) override { if (obj) ThreadUpdaterIO::_add(obj, core); }
    bool remove(std::shared_ptr<ct::BaseItem> obj) override { return obj ? core->remove(obj) : false; }
    bool has(std::shared_ptr<ct::CollectionItem> obj) override { return obj ? core->has(obj) : false; }
    void clear() override { core->clear(); };
};

};  // namespace llhardware::core
