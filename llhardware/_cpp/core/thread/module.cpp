#include <pybind11/pybind11.h>
#include <pybind11/stl.h>
#include <pybind11/functional.h>

#include "thread.hpp"


namespace py = pybind11;
namespace llhardware::core {

namespace ct = controllers;

class PySimpleUpdate : public SimpleUpdate {
    using SimpleUpdate::SimpleUpdate;

    void on_attach() override {
        PYBIND11_OVERRIDE_NAME(
            void,                 /* Return type */
            SimpleUpdate,         /* Parent class */
            "_on_attach",  /* Name of method in Python */
            on_attach,     /* Name of function in C++ */
        );
    }

    void on_detach() override {
        PYBIND11_OVERRIDE_NAME(
            void,                 /* Return type */
            SimpleUpdate,         /* Parent class */
            "_on_detach",  /* Name of method in Python */
            on_detach,     /* Name of function in C++ */
        );
    }

    uint64_t update(int id, uint64_t update_time_ns, uint64_t cur_time_ns) override {
        PYBIND11_OVERRIDE_PURE_NAME(
            uint64_t,             /* Return type */
            SimpleUpdate,         /* Parent class */
            "_update",            /* Name of method in Python */
            update,               /* Name of function in C++ */
            id, update_time_ns, cur_time_ns    /* Argument(s) */
        );
    }
};

class PyIOUpdate : public IOUpdate {
    using IOUpdate::IOUpdate;

    void on_attach() override {
        PYBIND11_OVERRIDE_NAME(
            void,                 /* Return type */
            IOUpdate,             /* Parent class */
            "_on_attach",  /* Name of method in Python */
            on_attach,     /* Name of function in C++ */
        );
    }

    void on_detach() override {
        PYBIND11_OVERRIDE_NAME(
            void,                 /* Return type */
            IOUpdate,             /* Parent class */
            "_on_detach",  /* Name of method in Python */
            on_detach,     /* Name of function in C++ */
        );
    }

    uint64_t update(int id, uint64_t update_time_ns, uint64_t cur_time_ns) override {
        PYBIND11_OVERRIDE_PURE_NAME(
            uint64_t,             /* Return type */
            IOUpdate,             /* Parent class */
            "_update",            /* Name of method in Python */
            update,               /* Name of function in C++ */
            id, update_time_ns, cur_time_ns    /* Argument(s) */
        );
    }

    void on_event(int fd, uint32_t flag) override {
        PYBIND11_OVERRIDE_PURE_NAME(
            void,              /* Return type */
            IOUpdate,             /* Parent class */
            "_on_event",            /* Name of method in Python */
            on_event,               /* Name of function in C++ */
            fd, flag    /* Argument(s) */
        );
    }
};

void init_module_thread(py::module &m) {
    py::class_<SimpleUpdate, std::shared_ptr<SimpleUpdate>, ct::CollectionItem, ct::PeriodicUpdateItem, PySimpleUpdate>(m, "SimpleUpdate")
        .def(py::init<>());

    py::class_<IOUpdate, std::shared_ptr<IOUpdate>, ct::CollectionItem, ct::PeriodicUpdateItem, ct::FileUpdateItem, PyIOUpdate>(m, "IOUpdate")
        .def(py::init<>());

    py::class_<ThreadUpdaterBase, std::shared_ptr<ThreadUpdaterBase>>(m, "ThreadUpdaterBase")
        .def("start", &ThreadUpdaterBase::start, py::call_guard<py::gil_scoped_release>())
        .def("stop", &ThreadUpdaterBase::stop, py::call_guard<py::gil_scoped_release>())
        .def("remove", &ThreadUpdaterBase::remove, py::call_guard<py::gil_scoped_release>())
        .def("has", &ThreadUpdaterBase::has, py::call_guard<py::gil_scoped_release>())
        .def("clear", &ThreadUpdaterBase::clear, py::call_guard<py::gil_scoped_release>());

    py::class_<ThreadUpdaterSimple, std::shared_ptr<ThreadUpdaterSimple>>(m, "ThreadUpdaterSimple")
        .def("add", &ThreadUpdaterSimple::add, py::call_guard<py::gil_scoped_release>());
    
    py::class_<ThreadUpdaterIO, std::shared_ptr<ThreadUpdaterIO>>(m, "ThreadUpdaterIO")
        .def("add", &ThreadUpdaterIO::add, py::call_guard<py::gil_scoped_release>());

    py::class_<BusyThreadUpdater, std::shared_ptr<BusyThreadUpdater>, ThreadUpdaterBase, ThreadUpdaterSimple>(m, "BusyThreadUpdater")
        .def(py::init<>());

    py::class_<EventThreadUpdater, std::shared_ptr<EventThreadUpdater>, ThreadUpdaterBase, ThreadUpdaterSimple, ThreadUpdaterIO>(m, "EventThreadUpdater")
        .def(py::init<>())
        .def("add", static_cast<void (EventThreadUpdater::*)(std::shared_ptr<SimpleUpdate>)>(&EventThreadUpdater::add), py::call_guard<py::gil_scoped_release>())
        .def("add", static_cast<void (EventThreadUpdater::*)(std::shared_ptr<IOUpdate>)>(&EventThreadUpdater::add), py::call_guard<py::gil_scoped_release>());
}

};  // namespace llhardware::io
