#pragma once

#include <mutex>
#include <memory>

#include "core/controllers/controllers.hpp"


namespace llhardware::core {

namespace ct = controllers;

class ThreadUpdaterBase {
public:
    virtual void start() = 0;
    virtual void stop() = 0;

    virtual bool remove(std::shared_ptr<ct::BaseItem> obj_weak) = 0;
    virtual bool has(std::shared_ptr<ct::CollectionItem> obj_weak) = 0;
    virtual void clear() = 0;
};

};  // namespace llhardware::core
