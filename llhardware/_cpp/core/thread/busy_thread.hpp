#pragma once

#include <mutex>
#include <memory>
#include <thread>

#include "base_update.hpp"
#include "simple_update.hpp"


namespace llhardware::core {

namespace ct = controllers;

class BusyThreadUpdaterCore : public ct::CollectionCore, public ct::PeriodicUpdateCore {
private:
    struct WorkerArgs {
        std::thread thr;
        bool is_working = true;
        std::shared_ptr<BusyThreadUpdaterCore> self;
    };

    std::recursive_mutex thread_mutex;
    std::shared_ptr<WorkerArgs> thread_data = NULL;

    void _worker_thread(std::shared_ptr<WorkerArgs>* raw_args);
    void _on_next_update_ns_changed() override {};

public:
    void start(std::shared_ptr<BusyThreadUpdaterCore> self);
    void stop();
};


class BusyThreadUpdater : public ThreadUpdaterBase, public ThreadUpdaterSimple {
private:
    std::shared_ptr<BusyThreadUpdaterCore> core;

public:
    BusyThreadUpdater() : core(std::make_shared<BusyThreadUpdaterCore>()) {}

    void start() override { core->start(core); }
    void stop() override { core->stop(); }

    void add(std::shared_ptr<SimpleUpdate> obj) override { if (obj) ThreadUpdaterSimple::_add(obj, core); }
    bool remove(std::shared_ptr<ct::BaseItem> obj) override { return obj ? core->remove(obj) : false; }
    bool has(std::shared_ptr<ct::CollectionItem> obj) override { return obj ? core->has(obj) : false; }
    void clear() override { core->clear(); };
};


};  // namespace llhardware::core
