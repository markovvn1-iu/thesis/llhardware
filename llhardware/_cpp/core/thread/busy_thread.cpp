#include "busy_thread.hpp"


using namespace llhardware::core;

void BusyThreadUpdaterCore::_worker_thread(std::shared_ptr<WorkerArgs>* raw_args) {
    std::shared_ptr<WorkerArgs> args(*((std::shared_ptr<WorkerArgs>*)raw_args));
    delete raw_args;

    uint64_t cur_time_ns, next_update_ns;
    while (args->is_working) _process_queue(&cur_time_ns, &next_update_ns);
}

void BusyThreadUpdaterCore::start(std::shared_ptr<BusyThreadUpdaterCore> self) {
    const std::lock_guard lock(thread_mutex);

    if (thread_data) stop();

    thread_data.reset(new WorkerArgs);
    thread_data->self = self;
    thread_data->thr = std::thread(&BusyThreadUpdaterCore::_worker_thread, this, new std::shared_ptr<WorkerArgs>(thread_data));
}

void BusyThreadUpdaterCore::stop() {
    const std::lock_guard lock(thread_mutex);

    if (!thread_data) return;
    thread_data->is_working = false;
    thread_data->thr.join();
    thread_data.reset();
}
