#include "event_thread.hpp"

#include <cstring>
#include <sys/epoll.h>
#include <sys/eventfd.h>
#include <sys/timerfd.h>
#include <unistd.h>

using namespace llhardware::core;

void EventThreadUpdaterCore::_epoll_register(int epoll_fd, int fd, uint32_t mask) {
    epoll_event event;
    memset(&event, 0, sizeof(event));
    event.events = mask;
    event.data.fd = fd;

    if (epoll_ctl(epoll_fd, EPOLL_CTL_ADD, fd, &event) < 0)
        throw std::runtime_error("EventThreadUpdaterCore::_epoll_register(): Failed to register fd in poll: " + std::string(strerror(errno)));
}

void EventThreadUpdaterCore::_epoll_modify(int epoll_fd, int fd, uint32_t mask) {
    epoll_event event;
    memset(&event, 0, sizeof(event));
    event.events = mask;
    event.data.fd = fd;

    if (epoll_ctl(epoll_fd, EPOLL_CTL_MOD, fd, &event) < 0)
        throw std::runtime_error("EventThreadUpdaterCore::_epoll_modify(): Failed to modify fd in poll: " + std::string(strerror(errno)));
}

void EventThreadUpdaterCore::_epoll_unregister(int epoll_fd, int fd) {
    if (epoll_ctl(epoll_fd, EPOLL_CTL_DEL, fd, NULL) < 0)
        throw std::runtime_error("EventThreadUpdaterCore::_epoll_unregister(): Failed to unregister fd in poll: " + std::string(strerror(errno)));
}

void EventThreadUpdaterCore::_worker_thread(std::shared_ptr<WorkerArgs>* raw_args) {
    std::shared_ptr<WorkerArgs> args(*((std::shared_ptr<WorkerArgs>*)raw_args));
    delete raw_args;

    struct itimerspec it_timer_spec = {{0, 0}, {0, 0}};

    struct epoll_event events[16];

    uint64_t cur_time_ns, next_update_ns;
    while (args->is_working) {
        int status = _process_queue(&cur_time_ns, &next_update_ns);
        if (status == 1) status = _process_queue(&cur_time_ns, &next_update_ns);

        if (status == 0) {
            uint64_t wait_for = next_update_ns - cur_time_ns;
            it_timer_spec.it_value.tv_nsec = static_cast<int64_t>(wait_for % ((uint64_t)1e9));
            it_timer_spec.it_value.tv_sec = static_cast<int64_t>(wait_for / ((uint64_t)1e9));
            if (timerfd_settime(args->timer_fd, 0, &it_timer_spec, NULL) != 0)
                throw std::runtime_error("EventThreadUpdaterCore::_worker_thread(): timerfd_settime() failed: " + std::string(strerror(errno)));
        }
        int events_count = epoll_wait(args->epoll_fd, events, 16, status == 1 ? 0 : -1);
        if (events_count < 0)
            throw std::runtime_error("EventThreadUpdaterCore::_worker_thread(): epoll_wait() failed: " + std::string(strerror(errno)));

        for (int i = 0; i < events_count; i++) {
            struct epoll_event& event = events[i];
            if (event.data.fd == args->event_fd) {
                eventfd_t value; eventfd_read(args->event_fd, &value);
            } else if (event.data.fd == args->timer_fd) {
                uint64_t dummybuf; int expired = read(args->timer_fd, &dummybuf, sizeof(uint64_t));
            } else {
                _process_event(event.data.fd, event.events);
            }
        }
    }
}

void EventThreadUpdaterCore::_on_set_event_mask(int fd, uint32_t mask) {
    std::lock_guard lock(data_mutex);
    if (mask == 0) {
        if (fd2mask.erase(fd) && thread_data) _epoll_unregister(thread_data->epoll_fd, fd);
    } else {
        if (fd2mask.count(fd) > 0 && thread_data) {
            _epoll_modify(thread_data->epoll_fd, fd, mask);
        } else if (thread_data) {
            _epoll_register(thread_data->epoll_fd, fd, mask);
        }
        fd2mask[fd] = mask;
    }
}

void EventThreadUpdaterCore::_send_event_to_thread() {
    if (!thread_data) return;
    if (eventfd_write(thread_data->event_fd, 1) != 0)
        throw std::runtime_error("EventThreadUpdaterCore::_send_event_to_thread(): eventfd_write failed: " + std::string(strerror(errno)));
}

void EventThreadUpdaterCore::start(std::shared_ptr<EventThreadUpdaterCore> self) {
    const std::lock_guard thread_lock(thread_mutex);

    if (thread_data) stop();

    int epoll_fd = epoll_create(1);
    if (epoll_fd < 0) throw std::runtime_error("EventThreadUpdaterCore::start(): Failed to create epoll: " + std::string(strerror(errno)));

    int event_fd = eventfd(0, EFD_NONBLOCK);
    if (event_fd < 0) throw std::runtime_error("EventThreadUpdaterCore::start(): Failed to create eventfd: " + std::string(strerror(errno)));

    int timer_fd = timerfd_create(CLOCK_MONOTONIC, 0);
    if (timer_fd < 0) throw std::runtime_error("EventThreadUpdaterCore::start(): Failed to create timerfd: " + std::string(strerror(errno)));

    _epoll_register(epoll_fd, event_fd, EPOLLIN);
    _epoll_register(epoll_fd, timer_fd, EPOLLIN);

    std::lock_guard lock(data_mutex);
    for (auto it = fd2mask.begin(); it != fd2mask.end(); it++) _epoll_register(epoll_fd, it->first, it->second);
    thread_data.reset(new WorkerArgs);
    thread_data->self = self;
    thread_data->epoll_fd = epoll_fd;
    thread_data->event_fd = event_fd;
    thread_data->timer_fd = timer_fd;
    thread_data->thr = std::thread(&EventThreadUpdaterCore::_worker_thread, this, new std::shared_ptr<WorkerArgs>(thread_data));
}

void EventThreadUpdaterCore::stop() {
    const std::lock_guard thread_lock(thread_mutex);

    if (!thread_data) return;
    std::shared_ptr<WorkerArgs> thread_data;
    {
        std::lock_guard lock(data_mutex);
        this->thread_data->is_working = false;
        _send_event_to_thread();

        thread_data = this->thread_data;
        this->thread_data.reset();
    }

    thread_data->thr.join();
    close(thread_data->epoll_fd);
    close(thread_data->event_fd);
    close(thread_data->timer_fd);
}
