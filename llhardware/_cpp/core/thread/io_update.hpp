#pragma once

#include <mutex>
#include <memory>

#include "core/controllers/controllers.hpp"


namespace llhardware::core {

namespace ct = controllers;

class IOUpdate : public ct::CollectionItem, public ct::PeriodicUpdateItem, public ct::FileUpdateItem {
friend class ThreadUpdaterIO;
private:
    using ct::BaseItem::mutex;
    using ct::CollectionItem::_attach_self;
    using ct::CollectionItem::_detach_self;
    using ct::PeriodicUpdateItem::_attach_self;
    using ct::PeriodicUpdateItem::_detach_self;
    using ct::FileUpdateItem::_attach_self;
    using ct::FileUpdateItem::_detach_self;

    bool _detach() override {
        bool res = ct::BaseItem::_detach_self();
        ct::CollectionItem::_detach_self();
        ct::PeriodicUpdateItem::_detach_self();
        ct::FileUpdateItem::_detach_self();
        return res;
    }

    template<class ThreadCore>
    void attach(std::shared_ptr<IOUpdate> self, std::shared_ptr<ThreadCore> core) {
        std::lock_guard lock(mutex);
        _detach();
        ct::FileUpdateItem::_attach_self(self, core);
        ct::PeriodicUpdateItem::_attach_self(self, core);
        ct::CollectionItem::_attach_self(self, core);
        ct::BaseItem::_attach_self(self, core);
    }

protected:
    uint64_t update(int id, uint64_t update_time_ns, uint64_t cur_time_ns) override { return 0; }

public:
    virtual ~IOUpdate() { detach(); }

    using ct::BaseItem::is_attached;
    using ct::BaseItem::detach;
};


class ThreadUpdaterIO {
protected:
    template<class ThreadCore>
    inline void _add(std::shared_ptr<IOUpdate> obj, std::shared_ptr<ThreadCore> core) { obj->attach(obj, core); }

public:
    virtual void add(std::shared_ptr<IOUpdate> obj) = 0;
};

};  // namespace llhardware::core
