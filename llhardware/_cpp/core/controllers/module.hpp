#pragma once

#include <pybind11/pybind11.h>

namespace llhardware::core::controllers {
    void init_module(pybind11::module &m);
}; // namespace llhardware::core::controllers
