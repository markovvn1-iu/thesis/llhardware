#include "file_update.hpp"

#include <fcntl.h>
#include <cstring>


using namespace llhardware::core::controllers;

void FileUpdateItem::_attach_self(std::shared_ptr<FileUpdateItem> self, std::shared_ptr<FileUpdateCore> core) {
    this->core = FileUpdateItemCore::attach(core, self);
}

void FileUpdateItem::_detach_self() {
    if (!core) return;
    core->detach();
    core.reset();
}

void FileUpdateItem::_on_event(std::shared_ptr<FileUpdateItemCore> core, int fd, uint32_t flag) {
    std::lock_guard lock(mutex);
    if (core != this->core) return;
    try {
        on_event(fd, flag);
    } catch (...) {
        // TODO: handle KeyboardInterrupt
    }
}

FileUpdateItem::~FileUpdateItem() {
    std::lock_guard lock(mutex);
    if (core) core->detach();
}

void FileUpdateItem::set_event_mask(int fd, uint32_t mask) {
    std::lock_guard lock(mutex);
    if (core) core->set_event_mask(fd, mask);
}





std::shared_ptr<FileUpdateItemCore> FileUpdateItemCore::attach(std::shared_ptr<FileUpdateCore> core, std::shared_ptr<FileUpdateItem> obj) {
    std::shared_ptr<FileUpdateItemCore> obj_core = std::make_shared<FileUpdateItemCore>(core, obj);
    obj_core->self_weak = obj_core;
    core->_add(obj, obj_core);
    return obj_core;
}

void FileUpdateItemCore::detach() {
    if (auto core = core_weak.lock()) core->_remove(obj_weak, self_weak.lock());
}

void FileUpdateItemCore::on_event(int fd, uint32_t flag) {
    if (auto obj = obj_weak.lock()) obj->_on_event(self_weak.lock(), fd, flag);
}

void FileUpdateItemCore::set_event_mask(int fd, uint32_t mask) {
    if (auto core = core_weak.lock()) core->set_event_mask(self_weak.lock(), fd, mask);
}




void FileUpdateCore::_remove(std::weak_ptr<FileUpdateItem> obj_weak, std::shared_ptr<FileUpdateItemCore> obj_core) {
    for (auto it = obj_core->fd2mask.begin(); it != obj_core->fd2mask.end(); it++) {
        int fd = it->first;
        if (!fd2obj.erase(fd)) return;
        _on_set_event_mask(fd, 0);
    }
    obj_core->fd2mask.clear();
}

void FileUpdateCore::set_event_mask(std::shared_ptr<FileUpdateItemCore> obj_core, int fd, uint32_t mask) {
    auto obj = obj_core->obj_weak.lock();
    if (!obj) return;

    const std::lock_guard lock(mutex);
    if (mask == 0) {
        if (!obj_core->fd2mask.erase(fd)) return;
        if (!fd2obj.erase(fd)) return;
    } else {
        auto obj_it = obj_core->fd2mask.find(fd);
        if (obj_it != obj_core->fd2mask.end() && obj_it->second == mask) return;
        auto core_it = fd2obj.find(fd);
        if (core_it != fd2obj.end() && core_it->second.lock() != obj_core) {
            throw std::runtime_error("FileUpdateCore::set_event_mask(): File discriptor already registered by another object");
        }
        if (core_it == fd2obj.end()) { // new fd
            int flags = fcntl(fd, F_GETFL);
            if (flags < 0) throw std::runtime_error("FileUpdateCore::set_event_mask(): Failed to get flags for the file " + std::to_string(fd) + ": " + std::string(strerror(errno)));
            if (flags & O_NONBLOCK == 0) throw std::runtime_error("FileUpdateCore::set_event_mask(): File discriptor " + std::to_string(fd) + " do not have O_NONBLOCK flag");
        }
        obj_core->fd2mask[fd] = mask;
        fd2obj[fd] = obj_core;
    }
    _on_set_event_mask(fd, mask);
}

void FileUpdateCore::_process_event(int fd, uint32_t flag) {
    auto core_it = fd2obj.find(fd);
    if (core_it == fd2obj.end()) return;
    if (std::shared_ptr<FileUpdateItemCore> core = core_it->second.lock()) {
        try {
            core->on_event(fd, flag);
        } catch (...) {
            // TODO: обработать ошибку KeyboardInterrupt
            // TODO: обработать ошибки
        }
    }
}
