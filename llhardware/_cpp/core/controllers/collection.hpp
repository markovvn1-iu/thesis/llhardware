#pragma once

#include <mutex>
#include <memory>
#include <map>

#include "base.hpp"


namespace llhardware::core::controllers {

class CollectionItemCore;
class CollectionCore;

class CollectionItem : virtual public BaseItem {
friend class CollectionCore;  // access _attach and _detach
friend class CollectionItemCore;  // access callbacks
private:
    std::shared_ptr<CollectionItemCore> core;
    bool _detach_if_core_is(std::shared_ptr<CollectionItemCore> core);

protected:
    // helper functions
    void _attach_self(std::shared_ptr<CollectionItem> self, std::shared_ptr<CollectionCore> core);
    void _detach_self();

public:
    virtual ~CollectionItem();
};

class CollectionItemCore {
friend class CollectionCore;  // access core
private:
    const std::weak_ptr<CollectionCore> core_weak;
    std::weak_ptr<CollectionItemCore> self_weak;
    const std::weak_ptr<CollectionItem> obj_weak;

public:
    CollectionItemCore(std::weak_ptr<CollectionCore> core_weak, std::weak_ptr<CollectionItem> obj_weak) : core_weak(core_weak), obj_weak(obj_weak) {}
    static std::shared_ptr<CollectionItemCore> attach(std::shared_ptr<CollectionCore> core, std::shared_ptr<CollectionItem> obj);
    void detach();
};

class CollectionCore : virtual public BaseCore {
friend class CollectionItemCore;  // access _add and _remove
private:
    std::mutex mutex;
    std::map<std::weak_ptr<CollectionItem>, std::weak_ptr<CollectionItemCore>, std::owner_less<std::weak_ptr<CollectionItem>>> obj2core;

    void _add(std::shared_ptr<CollectionItem> obj, std::shared_ptr<CollectionItemCore> obj_core);
    void _remove(std::weak_ptr<CollectionItem> obj_weak, std::shared_ptr<CollectionItemCore> obj_core);

public:
    bool has(std::shared_ptr<CollectionItem> obj);
    void clear();
};

};  // namespace llhardware::core::controllers
