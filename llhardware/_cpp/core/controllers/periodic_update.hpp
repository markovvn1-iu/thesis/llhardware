#pragma once

#include <mutex>
#include <memory>
#include <map>
#include <queue>

#include "core/timer.hpp"
#include "base.hpp"


namespace llhardware::core::controllers {

class PeriodicUpdateItemCore;
class PeriodicUpdateCore;

class PeriodicUpdateItem : virtual public BaseItem {
friend class PeriodicUpdateCore;  // access _attach and _detach
friend class PeriodicUpdateItemCore;  // access callbacks
private:
    std::shared_ptr<PeriodicUpdateItemCore> core;

    // calback functions
    uint64_t _update(std::shared_ptr<PeriodicUpdateItemCore> core, int id, uint64_t update_time_ns, uint64_t cur_time_ns);

protected:
    // helper functions
    void _attach_self(std::shared_ptr<PeriodicUpdateItem> self, std::shared_ptr<PeriodicUpdateCore> core);
    void _detach_self();

protected:
    // callbacks
    virtual uint64_t update(int id, uint64_t update_time_ns, uint64_t cur_time_ns) = 0;  // return time for the next update (0 - do not update)

    // methods
    void call_at(int id, uint64_t update_time_ns);  // 0 - do not call
    inline void call_soon(int id) { call_later(id, 0); }
    void call_later(int id, int64_t delay_ns) { call_at(id, (delay_ns < 0) ? 0 : get_time_ns() + delay_ns); };
    inline void call_cancel(int id) { call_at(id, 0); }

public:
    virtual ~PeriodicUpdateItem();
};



class PeriodicUpdateItemCore {
friend class PeriodicUpdateCore;  // access data
private:
    struct UpdateInfo {
        uint64_t next_update_ns = 0;
        bool force = false;  // do not allow rescheduling in core.schedule_next function
        UpdateInfo() {};
        UpdateInfo(uint64_t next_update_ns, bool force) : next_update_ns(next_update_ns), force(force) {}
    };

    const std::weak_ptr<PeriodicUpdateCore> core_weak;
    std::weak_ptr<PeriodicUpdateItemCore> self_weak;
    const std::weak_ptr<PeriodicUpdateItem> obj_weak;
    std::map<int, UpdateInfo> id2info;

public:
    PeriodicUpdateItemCore(std::weak_ptr<PeriodicUpdateCore> core_weak, std::weak_ptr<PeriodicUpdateItem> obj_weak) : core_weak(core_weak), obj_weak(obj_weak) {}
    static std::shared_ptr<PeriodicUpdateItemCore> attach(std::shared_ptr<PeriodicUpdateCore> core, std::shared_ptr<PeriodicUpdateItem> obj);
    void detach();

    // callbacks
    uint64_t update(int id, uint64_t update_time_ns, uint64_t cur_time_ns);

    // methods
    void call_at(int id, uint64_t update_time_ns);
};




class PeriodicUpdateCore : virtual public BaseCore {
friend class PeriodicUpdateItemCore;  // access _add and _remove
private:
    struct QueueItemType {
        int id;
        uint64_t update_time_ns;
        std::weak_ptr<PeriodicUpdateItemCore> data;

        QueueItemType() {}
        QueueItemType(uint64_t update_time_ns, std::weak_ptr<PeriodicUpdateItemCore> data, int id)
            : update_time_ns(update_time_ns), data(data), id(id) {}

        bool operator < (const QueueItemType& other) const { return update_time_ns > other.update_time_ns; }
    };

    std::mutex data_mutex;
    std::priority_queue<QueueItemType> queue;

    void _add(std::shared_ptr<PeriodicUpdateItem> obj, std::shared_ptr<PeriodicUpdateItemCore> obj_core) {}
    void _remove(std::weak_ptr<PeriodicUpdateItem> obj_weak, std::shared_ptr<PeriodicUpdateItemCore> obj_core) {}

    /**
     * return status:
     *   -1 - no obj, wait for an event (unknown time)
     *   0 - no obj, wait until next_update_ns
     *   1 - has obj
    */
    int get_next(QueueItemType* _qitem, uint64_t* _cur_time_ns, uint64_t* _next_update_ns);

    void schedule_next(const QueueItemType& qitem, std::shared_ptr<PeriodicUpdateItemCore> core, uint64_t next_update_ns);

    bool call_at(std::shared_ptr<PeriodicUpdateItemCore> obj, int id, uint64_t update_time_ns);

protected:
    virtual void _on_next_update_ns_changed() = 0;  // called only if call_later change the next_update_ns

    /**
     * return:
     *   -1 - no obj, wait for an event (unknown time)
     *   0 - no obj, wait until next_update_ns
     *   1 - has obj, repeat call
    */
    int _process_queue(uint64_t* cur_time_ns, uint64_t* next_update_ns);

};

};  // namespace llhardware::core
