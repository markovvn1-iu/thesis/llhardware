#include "collection.hpp"

#include <vector>


using namespace llhardware::core::controllers;

bool CollectionItem::_detach_if_core_is(std::shared_ptr<CollectionItemCore> core) {
    std::lock_guard lock(mutex);
    if (this->core != core) return false;
    return _detach();
}

void CollectionItem::_attach_self(std::shared_ptr<CollectionItem> self, std::shared_ptr<CollectionCore> core) {
    this->core = CollectionItemCore::attach(core, self);
}

void CollectionItem::_detach_self() {
    if (!core) return;
    core->detach();
    core.reset();
}

CollectionItem::~CollectionItem() {
    std::lock_guard lock(mutex);
    if (core) core->detach();
}



std::shared_ptr<CollectionItemCore> CollectionItemCore::attach(std::shared_ptr<CollectionCore> core, std::shared_ptr<CollectionItem> obj) {
    std::shared_ptr<CollectionItemCore> obj_core = std::make_shared<CollectionItemCore>(core, obj);
    obj_core->self_weak = obj_core;
    core->_add(obj, obj_core);
    return obj_core;
}

void CollectionItemCore::detach() {
    if (auto core = core_weak.lock()) core->_remove(obj_weak, self_weak.lock());
}

void CollectionCore::_add(std::shared_ptr<CollectionItem> obj, std::shared_ptr<CollectionItemCore> obj_core) {
    std::lock_guard lock(mutex);
    obj2core[obj] = obj_core;
}

void CollectionCore::_remove(std::weak_ptr<CollectionItem> obj_weak, std::shared_ptr<CollectionItemCore> obj_core) {
    std::lock_guard lock(mutex);
    obj2core.erase(obj_weak);
}

bool CollectionCore::has(std::shared_ptr<CollectionItem> obj) {
    std::lock_guard lock(mutex);
    return obj2core.count(obj) > 0;
}

void CollectionCore::clear() {
    std::vector<std::weak_ptr<CollectionItemCore>> core4delete;
    {
        const std::lock_guard lock(mutex);
        core4delete.reserve(obj2core.size());
        for (auto it = obj2core.begin(); it != obj2core.end(); it++) {
            core4delete.push_back(it->second);
        }
    }

    for (auto obj_core_weak = core4delete.begin(); obj_core_weak != core4delete.end(); obj_core_weak++) {
        if (auto obj_core = obj_core_weak->lock()) {
            if (auto obj = obj_core->obj_weak.lock()) obj->_detach_if_core_is(obj_core);
        }
    }
}
