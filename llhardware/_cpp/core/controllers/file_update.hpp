#pragma once

#include <mutex>
#include <memory>
#include <map>
#include <set>
#include <sys/epoll.h>

#include "core/timer.hpp"
#include "base.hpp"


namespace llhardware::core::controllers {

class FileUpdateItemCore;
class FileUpdateCore;

class FileUpdateItem : virtual public BaseItem {
friend class FileUpdateCore;  // access _attach and _detach
friend class FileUpdateItemCore;  // access callbacks
private:
    std::shared_ptr<FileUpdateItemCore> core;

    // calback functions
    void _on_event(std::shared_ptr<FileUpdateItemCore> core, int fd, uint32_t flag);

protected:
    // helper functions
    void _attach_self(std::shared_ptr<FileUpdateItem> self, std::shared_ptr<FileUpdateCore> core);
    void _detach_self();

protected:
    // callbacks
    virtual void on_event(int fd, uint32_t flag) = 0;

    // methods
    void set_event_mask(int fd, uint32_t mask);
    void unset_event_mask(int fd) { set_event_mask(fd, 0); }

public:
    virtual ~FileUpdateItem();
};



class FileUpdateItemCore {
friend class FileUpdateCore;  // access data
private:
    const std::weak_ptr<FileUpdateCore> core_weak;
    std::weak_ptr<FileUpdateItemCore> self_weak;
    const std::weak_ptr<FileUpdateItem> obj_weak;
    std::map<int, uint32_t> fd2mask;

public:
    FileUpdateItemCore(std::weak_ptr<FileUpdateCore> core_weak, std::weak_ptr<FileUpdateItem> obj_weak) : core_weak(core_weak), obj_weak(obj_weak) {}
    static std::shared_ptr<FileUpdateItemCore> attach(std::shared_ptr<FileUpdateCore> core, std::shared_ptr<FileUpdateItem> obj);
    void detach();

    // callbacks
    void on_event(int fd, uint32_t flag);

    // methods
    void set_event_mask(int fd, uint32_t mask);
};




class FileUpdateCore : virtual public BaseCore {
friend class FileUpdateItemCore;  // access _add and _remove
private:
    std::mutex mutex;
    std::map<int, std::weak_ptr<FileUpdateItemCore>> fd2obj;

    void _add(std::shared_ptr<FileUpdateItem> obj, std::shared_ptr<FileUpdateItemCore> obj_core) {}
    void _remove(std::weak_ptr<FileUpdateItem> obj_weak, std::shared_ptr<FileUpdateItemCore> obj_core);

    void set_event_mask(std::shared_ptr<FileUpdateItemCore> obj_core, int fd, uint32_t mask);

protected:
    virtual void _on_set_event_mask(int fd, uint32_t mask) = 0;
    void _process_event(int fd, uint32_t flag);
};

};  // namespace llhardware::core
