#pragma once

#include <mutex>
#include <memory>
#include <set>
#include <queue>

#include "core/timer.hpp"
#include "base.hpp"
#include "types/types.hpp"


namespace llhardware::core::controllers {

class StreamItemCore;
class StreamCore;

class StreamItem : virtual public BaseItem {
friend class StreamCore;  // access _attach and _detach
friend class StreamItemCore;  // access callbacks
private:
    std::shared_ptr<StreamItemCore> core;

protected:
    // helper functions
    bool _attach_self(std::shared_ptr<StreamItem> self, std::shared_ptr<StreamCore> parent, bool get_last_value);
    void _detach_self();

    // calback functions
    virtual void _on_get_data(std::shared_ptr<StreamItemCore> core, type::PDataItem item);

protected:
    // callbacks
    virtual void on_get_data(type::PDataItem item) = 0;

public:
    virtual ~StreamItem();
};



class StreamItemCore {
friend class StreamCore;  // access data
private:
    const std::shared_ptr<StreamCore> parent;  // hold transform tree
    std::weak_ptr<StreamItemCore> self_weak;
    const std::weak_ptr<StreamItem> obj_weak;

public:
    StreamItemCore(std::weak_ptr<StreamItem> obj_weak, std::shared_ptr<StreamCore> parent)
        : obj_weak(obj_weak), parent(parent) {}
    static std::shared_ptr<StreamItemCore> attach(std::shared_ptr<StreamItem> obj, std::shared_ptr<StreamCore> parent, bool get_last_value);
    void detach();

    // callbacks
    void on_get_data(type::PDataItem item);
};




class StreamCore : virtual public BaseCore {
friend class StreamItemCore;  // access _add and _remove
friend class TransformStreamCore;
private:
    bool _add(std::shared_ptr<StreamItem> obj, std::shared_ptr<StreamItemCore> obj_core, bool get_last_value);
    void _remove(std::weak_ptr<StreamItem> obj_weak, std::shared_ptr<StreamItemCore> obj_core);

protected:
    mutable std::mutex mutex;
    type::PDataItem last_item = nullptr;
    bool is_open = true;

    std::set<std::weak_ptr<StreamItemCore>, std::owner_less<std::weak_ptr<StreamItemCore>>> subs;
    std::set<std::weak_ptr<StreamCore>, std::owner_less<std::weak_ptr<StreamCore>>> next_nodes;

public:
    inline type::PDataItem get_last_item() const { const std::lock_guard lock(mutex); return last_item; }

    virtual void push(type::PDataItem item);
    void close();
};



class TransformStreamCore : public StreamCore {
protected:
    const type::PDataTransform func;
    const std::shared_ptr<StreamCore> parent;  // hold transform tree
    std::weak_ptr<TransformStreamCore> self;

public:
    TransformStreamCore(std::shared_ptr<StreamCore> parent, type::PDataTransform func) : parent(parent), func(func) {}
    static std::shared_ptr<TransformStreamCore> create(std::shared_ptr<StreamCore> parent, type::PDataTransform func);
    virtual ~TransformStreamCore();

    void push(type::PDataItem item) override;
};

};  // namespace llhardware::core
