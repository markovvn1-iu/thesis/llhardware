#include "stream.hpp"


using namespace llhardware::core::controllers;

bool StreamItem::_attach_self(std::shared_ptr<StreamItem> self, std::shared_ptr<StreamCore> parent, bool get_last_value) {
    this->core = StreamItemCore::attach(self, parent, get_last_value);
    return (bool)(this->core);
}

void StreamItem::_detach_self() {
    if (!core) return;
    core->detach();
    core.reset();
}

void StreamItem::_on_get_data(std::shared_ptr<StreamItemCore> core, type::PDataItem item) {
    std::lock_guard lock(mutex);
    if (core != this->core) return;
    try {
        on_get_data(item);
    } catch (...) {
        // TODO: handle KeyboardInterrupt
    }
}

StreamItem::~StreamItem() {
    std::lock_guard lock(mutex);
    if (core) core->detach();
}



std::shared_ptr<StreamItemCore> StreamItemCore::attach(std::shared_ptr<StreamItem> obj, std::shared_ptr<StreamCore> parent, bool get_last_value) {
    std::shared_ptr<StreamItemCore> obj_core = std::make_shared<StreamItemCore>(obj, parent);
    obj_core->self_weak = obj_core;
    if (!parent->_add(obj, obj_core, get_last_value)) return nullptr;
    return obj_core;
}

void StreamItemCore::detach() {
    parent->_remove(obj_weak, self_weak.lock());
}

void StreamItemCore::on_get_data(type::PDataItem item) {
    if (auto obj = obj_weak.lock()) obj->_on_get_data(self_weak.lock(), item);
}



bool StreamCore::_add(std::shared_ptr<StreamItem> obj, std::shared_ptr<StreamItemCore> obj_core, bool get_last_value) {
    std::lock_guard lock(mutex);
    if (get_last_value && last_item) obj->on_get_data(last_item);
    if (!is_open) return false;
    subs.insert(obj_core);
    return true;
}

void StreamCore::_remove(std::weak_ptr<StreamItem> obj_weak, std::shared_ptr<StreamItemCore> obj_core) {
    std::lock_guard lock(mutex);
    subs.erase(obj_core);
}

void StreamCore::push(type::PDataItem item) {
    if (!item) return;
    const std::lock_guard lock(mutex);
    if (!is_open) return;
    last_item = item;
    for (auto it = subs.begin(); it != subs.end(); it++) {
        if (std::shared_ptr<StreamItemCore> sub = it->lock()) sub->on_get_data(item);
    }
    for (auto it = next_nodes.begin(); it != next_nodes.end(); it++) {
        if (auto trans = it->lock()) trans->push(item);
    }
}

void StreamCore::close() {
    const std::lock_guard lock(mutex);
    is_open = false;
    for (auto it = next_nodes.begin(); it != next_nodes.end(); it++) {
        if (auto trans = it->lock()) trans->close();
    }
}

std::shared_ptr<TransformStreamCore> TransformStreamCore::create(std::shared_ptr<StreamCore> parent, type::PDataTransform func) {
    const auto res = std::make_shared<TransformStreamCore>(parent, func);
    res->self = res;
    const std::lock_guard lock(parent->mutex);
    res->is_open = parent->is_open;
    if (parent->last_item) res->last_item = func->transform(parent->last_item);
    parent->next_nodes.insert(res);
    return res;
}

TransformStreamCore::~TransformStreamCore() {
    const std::scoped_lock lock(parent->mutex, mutex);
    parent->next_nodes.erase(self);
}

void TransformStreamCore::push(type::PDataItem item) {
    if (!item) return;
    item = func->transform(item);
    StreamCore::push(item);
}


