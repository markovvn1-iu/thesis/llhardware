#include "periodic_update.hpp"


using namespace llhardware::core::controllers;

void PeriodicUpdateItem::_attach_self(std::shared_ptr<PeriodicUpdateItem> self, std::shared_ptr<PeriodicUpdateCore> core) {
    this->core = PeriodicUpdateItemCore::attach(core, self);
}

void PeriodicUpdateItem::_detach_self() {
    if (!core) return;
    core->detach();
    core.reset();
}

uint64_t PeriodicUpdateItem::_update(std::shared_ptr<PeriodicUpdateItemCore> core, int id, uint64_t update_time_ns, uint64_t cur_time_ns) {
    std::lock_guard lock(mutex);
    if (core != this->core) return 0;
    return update(id, update_time_ns, cur_time_ns);
}

PeriodicUpdateItem::~PeriodicUpdateItem() {
    std::lock_guard lock(mutex);
    if (core) core->detach();
}

void PeriodicUpdateItem::call_at(int id, uint64_t update_time_ns) {
    std::lock_guard lock(mutex);
    if (core) core->call_at(id, update_time_ns);
}




std::shared_ptr<PeriodicUpdateItemCore> PeriodicUpdateItemCore::attach(std::shared_ptr<PeriodicUpdateCore> core, std::shared_ptr<PeriodicUpdateItem> obj) {
    std::shared_ptr<PeriodicUpdateItemCore> obj_core = std::make_shared<PeriodicUpdateItemCore>(core, obj);
    obj_core->self_weak = obj_core;
    core->_add(obj, obj_core);
    return obj_core;
}

void PeriodicUpdateItemCore::detach() {
    if (auto core = core_weak.lock()) core->_remove(obj_weak, self_weak.lock());
}

uint64_t PeriodicUpdateItemCore::update(int id, uint64_t update_time_ns, uint64_t cur_time_ns) {
    if (auto obj = obj_weak.lock()) return obj->_update(self_weak.lock(), id, update_time_ns, cur_time_ns);
    return 0;
}

void PeriodicUpdateItemCore::call_at(int id, uint64_t update_time_ns) {
    if (auto core = core_weak.lock()) core->call_at(self_weak.lock(), id, update_time_ns);
}




/**
 * return status:
 *   -1 - no obj, wait for an event (unknown time)
 *   0 - no obj, wait until next_update_ns
 *   1 - has obj
*/
int PeriodicUpdateCore::get_next(QueueItemType* _qitem, uint64_t* _cur_time_ns, uint64_t* _next_update_ns) {
    const std::lock_guard self_lock(data_mutex);

    while (!queue.empty()) {
        QueueItemType qitem = queue.top();
        uint64_t cur_time_ns = get_time_ns();
        if (cur_time_ns < qitem.update_time_ns) {
            *_cur_time_ns = cur_time_ns;
            *_next_update_ns = qitem.update_time_ns;
            return 0;
        }
        queue.pop();

        auto core = qitem.data.lock();
        if (!core) continue;
        auto qitem_info = core->id2info.find(qitem.id);
        if (qitem_info == core->id2info.end()) continue;  // update was canceled
        if (qitem.update_time_ns != qitem_info->second.next_update_ns) continue;  // update was rescheduled
        qitem_info->second.force = false;  // allow rescheduling

        // Return object
        *_qitem = qitem;
        *_cur_time_ns = cur_time_ns;
        return 1;
    }
    return -1;
}

void PeriodicUpdateCore::schedule_next(const QueueItemType& qitem, std::shared_ptr<PeriodicUpdateItemCore> core, uint64_t next_update_ns) {
    const std::lock_guard self_lock(data_mutex);

    auto qitem_info = core->id2info.find(qitem.id);
    if (qitem_info != core->id2info.end() && qitem_info->second.force) return;  // do not reschedule forced info

    if (next_update_ns == 0) {
        core->id2info.erase(qitem.id);
    } else {
        core->id2info[qitem.id] = {next_update_ns, false};
        queue.push({next_update_ns, qitem.data, qitem.id});
    }
}

bool PeriodicUpdateCore::call_at(std::shared_ptr<PeriodicUpdateItemCore> obj, int id, uint64_t update_time_ns) {
    bool next_update_ns_changed = false;
    {
        const std::lock_guard lock(data_mutex);
        if (update_time_ns == 0) {
            obj->id2info.erase(id);
            return true;
        }

        obj->id2info[id] = {update_time_ns, true};
        next_update_ns_changed = queue.empty() || update_time_ns < queue.top().update_time_ns;
        queue.push({update_time_ns, obj, id});
    }
    if (next_update_ns_changed) _on_next_update_ns_changed();
    return true;
}

/**
 * return:
 *   -1 - no obj, wait for an event (unknown time)
 *   0 - no obj, wait until next_update_ns
 *   1 - has obj, repeat call
*/
int PeriodicUpdateCore::_process_queue(uint64_t* cur_time_ns, uint64_t* next_update_ns) {
    QueueItemType qitem;

    int status = get_next(&qitem, cur_time_ns, next_update_ns);
    if (status != 1) return status;
    if (auto core = qitem.data.lock()) {
        try {
            uint64_t update_time_ns = core->update(qitem.id, qitem.update_time_ns, *cur_time_ns);
            schedule_next(qitem, core, update_time_ns);
        } catch (...) {
            schedule_next(qitem, core, 0);
            // TODO: обработать ошибку KeyboardException
            // TODO: обработать ошибки
        }
    }
    return status;
}
