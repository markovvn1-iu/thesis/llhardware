#include <pybind11/pybind11.h>
#include <pybind11/stl.h>

#include "controllers.hpp"


namespace py = pybind11;
namespace llhardware::core::controllers {

class PyPubBaseItem : public BaseItem {
public:
    using BaseItem::on_attach;
    using BaseItem::on_detach;
};

class PyPubPeriodicUpdateItem : public PeriodicUpdateItem {
public:
    using PeriodicUpdateItem::call_at;
    using PeriodicUpdateItem::call_soon;
    using PeriodicUpdateItem::call_later;
    using PeriodicUpdateItem::call_cancel;
    using PeriodicUpdateItem::update;
};

class PyPubFileUpdateItem : public FileUpdateItem {
public:
    using FileUpdateItem::set_event_mask;
    using FileUpdateItem::unset_event_mask;
    using FileUpdateItem::on_event;
};

class PyPubStreamItem : public StreamItem {
public:
    using StreamItem::on_get_data;
};

void init_module(py::module &m) {
    py::class_<BaseItem, std::shared_ptr<BaseItem>>(m, "BaseItem")
        .def("_on_attach", &PyPubBaseItem::on_attach)
        .def("_on_detach", &PyPubBaseItem::on_detach)
        .def_property_readonly("is_attached", &BaseItem::is_attached)
        .def("detach", &BaseItem::detach, py::call_guard<py::gil_scoped_release>());

    py::class_<CollectionItem, std::shared_ptr<CollectionItem>, BaseItem>(m, "CollectionItem");

    py::class_<PeriodicUpdateItem, std::shared_ptr<PeriodicUpdateItem>, BaseItem>(m, "PeriodicUpdateItem")
        .def("_call_at", &PyPubPeriodicUpdateItem::call_at, py::call_guard<py::gil_scoped_release>())
        .def("_call_soon", &PyPubPeriodicUpdateItem::call_soon, py::call_guard<py::gil_scoped_release>())
        .def("_call_later", &PyPubPeriodicUpdateItem::call_later, py::call_guard<py::gil_scoped_release>())
        .def("_call_cancel", &PyPubPeriodicUpdateItem::call_cancel, py::call_guard<py::gil_scoped_release>())
        .def("_update", &PyPubPeriodicUpdateItem::update);

    py::class_<FileUpdateItem, std::shared_ptr<FileUpdateItem>, BaseItem>(m, "FileUpdateItem")
        .def("_set_event_mask", &PyPubFileUpdateItem::set_event_mask, py::call_guard<py::gil_scoped_release>())
        .def("_unset_event_mask", &PyPubFileUpdateItem::unset_event_mask, py::call_guard<py::gil_scoped_release>())
        .def("_on_event", &PyPubFileUpdateItem::on_event);

    py::class_<StreamItem, std::shared_ptr<StreamItem>, BaseItem>(m, "StreamItem")
        .def("_on_get_data", &PyPubStreamItem::on_get_data);
}

};  // namespace llhardware::core::controllers
