#include "base.hpp"


using namespace llhardware::core::controllers;

std::shared_ptr<BaseItemCore> BaseItem::get_core_threadsafe() {
    std::lock_guard lock(mutex);
    return core;  // Otherwise the value may be invalid (half-copied, for example)
}

bool BaseItem::_detach_if_core_is(std::shared_ptr<BaseItemCore> core) {
    std::lock_guard lock(mutex);
    if (this->core != core) return false;
    return _detach();
}

void BaseItem::_attach_self(std::shared_ptr<BaseItem> self, std::shared_ptr<BaseCore> core) {
    this->core = BaseItemCore::attach(core, self);
    try {
        on_attach();
    } catch(...) {
        // TODO: handle KeyboardInterrupt
    }
    
}

bool BaseItem::_detach_self() {
    if (!core) return false;
    core->detach();
    core.reset();
    try {
        on_detach();
    } catch(...) {
        // TODO: handle KeyboardInterrupt
    }
    return true;
}

BaseItem::~BaseItem() {
    std::lock_guard lock(mutex);
    if (core) core->detach();
}

bool BaseItem::is_attached() const {
    std::lock_guard lock(mutex);
    return (core) ? core->is_attached() : false;
}

bool BaseItem::detach() {
    std::lock_guard lock(mutex);
    return _detach();
}




std::shared_ptr<BaseItemCore> BaseItemCore::attach(std::shared_ptr<BaseCore> core, std::shared_ptr<BaseItem> obj) {
    std::shared_ptr<BaseItemCore> obj_core = std::make_shared<BaseItemCore>(core, obj);
    obj_core->self_weak = obj_core;
    core->_add(obj, obj_core);
    return obj_core;
}

void BaseItemCore::detach() {
    if (auto core = core_weak.lock()) core->_remove(obj_weak, self_weak.lock());
}

bool BaseCore::remove(std::shared_ptr<BaseItem> obj) {
    if (!obj) return false;
    auto obj_core = obj->get_core_threadsafe();
    if (!obj_core) return false;
    std::shared_ptr<BaseCore> core = obj_core->core_weak.lock();
    if (!core) return false;
    if (core.get() != this) return false;
    return obj->_detach_if_core_is(obj_core);
}
