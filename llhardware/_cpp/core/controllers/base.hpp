#pragma once

#include <mutex>
#include <memory>
#include <map>

#include "base.hpp"


namespace llhardware::core::controllers {


class BaseItemCore;
class BaseCore;

class BaseItem {
friend class BaseCore;  // access _attach and _detach
friend class BaseItemCore;  // access callbacks
private:
    std::shared_ptr<BaseItemCore> core;
    std::shared_ptr<BaseItemCore> get_core_threadsafe();
    bool _detach_if_core_is(std::shared_ptr<BaseItemCore> core);

protected:
    mutable std::recursive_mutex mutex;

    // helper functions
    void _attach_self(std::shared_ptr<BaseItem> self, std::shared_ptr<BaseCore> core);
    bool _detach_self();
    virtual bool _detach() = 0;  // mutex already locked

protected:
    // callbacks
    virtual void on_attach() {}
    virtual void on_detach() {}

public:
    virtual ~BaseItem();

    // methods
    bool is_attached() const;
    bool detach();
};

class BaseItemCore {
friend class BaseCore;  // access core
private:
    const std::weak_ptr<BaseCore> core_weak;
    std::weak_ptr<BaseItemCore> self_weak;
    const std::weak_ptr<BaseItem> obj_weak;

public:
    BaseItemCore(std::weak_ptr<BaseCore> core_weak, std::weak_ptr<BaseItem> obj_weak) : core_weak(core_weak), obj_weak(obj_weak) {}
    static std::shared_ptr<BaseItemCore> attach(std::shared_ptr<BaseCore> core, std::shared_ptr<BaseItem> obj);
    void detach();

    inline bool is_attached() const { return !core_weak.expired(); };
};

class BaseCore {
friend class BaseItemCore;  // access _add and _remove
private:
    void _add(std::shared_ptr<BaseItem> obj, std::shared_ptr<BaseItemCore> obj_core) {}
    void _remove(std::weak_ptr<BaseItem> obj_weak, std::shared_ptr<BaseItemCore> obj_core) {}

public:
    bool remove(std::shared_ptr<BaseItem> obj);
};

};  // namespace llhardware::core::controllers
