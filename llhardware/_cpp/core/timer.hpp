#pragma once

#include <cstdint>


namespace llhardware::core {

uint64_t get_time_ns();

void delay_microseconds(unsigned int micro);

}  // namespace llhardware::core
