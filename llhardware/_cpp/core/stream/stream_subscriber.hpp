#pragma once

#include <mutex>
#include <memory>

#include "types/types.hpp"
#include "core/controllers/controllers.hpp"


namespace llhardware::core {

namespace ct = controllers;

class StreamSubscriber : public ct::CollectionItem, public ct::StreamItem {
friend class SimpleStream;
private:
    using ct::BaseItem::mutex;
    using ct::CollectionItem::_attach_self;
    using ct::CollectionItem::_detach_self;
    using ct::StreamItem::_attach_self;
    using ct::StreamItem::_detach_self;

    bool _detach() override {
        bool res = ct::BaseItem::_detach_self();
        ct::CollectionItem::_detach_self();
        ct::StreamItem::_detach_self();
        return res;
    }

    template<class StreamCore>
    bool attach(std::shared_ptr<StreamSubscriber> self, std::shared_ptr<StreamCore> core, std::shared_ptr<ct::StreamCore> parent, bool get_last_value) {
        std::lock_guard lock(mutex);
        _detach();
        if (!ct::StreamItem::_attach_self(self, parent, get_last_value)) return false;
        ct::CollectionItem::_attach_self(self, core);
        ct::BaseItem::_attach_self(self, core);
        return true;
    }

public:
    virtual ~StreamSubscriber() { detach(); }

    using ct::BaseItem::is_attached;
    using ct::BaseItem::detach;
};


class SimpleStream {
protected:
    template<class StreamCore>
    inline bool _add(std::shared_ptr<StreamSubscriber> obj, std::shared_ptr<StreamCore> core, std::shared_ptr<ct::StreamCore> parent, bool get_last_value) {
      return obj->attach(obj, core, parent, get_last_value);
    }

public:
    virtual bool add(std::shared_ptr<StreamSubscriber> obj, bool get_last_value) = 0;
    virtual std::shared_ptr<SimpleStream> transform(type::PDataTransform func) = 0;
    inline std::shared_ptr<SimpleStream> operator |(type::PDataTransform func) { return transform(func); };
};

};  // namespace llhardware::core
