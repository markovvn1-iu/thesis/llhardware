#include "stream.hpp"

using namespace llhardware::core;


std::shared_ptr<SimpleStream> TransformedStream::transform(type::PDataTransform func) {
    return std::make_shared<TransformedStream>(core, ct::TransformStreamCore::create(parent, func));
}

std::shared_ptr<SimpleStream> Stream::transform(type::PDataTransform func) {
    return std::make_shared<TransformedStream>(core, ct::TransformStreamCore::create(core, func));
}