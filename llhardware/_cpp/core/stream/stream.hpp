#pragma once

#include <mutex>
#include <memory>
#include <thread>

#include "stream_subscriber.hpp"


namespace llhardware::core {

namespace ct = controllers;

class StreamCore : public ct::CollectionCore, public ct::StreamCore {};


class Stream : public SimpleStream {
protected:
    std::shared_ptr<StreamCore> core;
    Stream(std::shared_ptr<StreamCore> core) : core(core) {}

public:
    bool add(std::shared_ptr<StreamSubscriber> obj, bool get_last_value = false) override {
        return obj ? SimpleStream::_add(obj, core, core, get_last_value): false;
    }
    bool remove(std::shared_ptr<ct::BaseItem> obj) { return obj ? core->remove(obj) : false; }
    bool has(std::shared_ptr<ct::CollectionItem> obj) { return obj ? core->has(obj) : false; }
    inline type::PDataItem get_last_item() const { return core->get_last_item(); }

    std::shared_ptr<SimpleStream> transform(type::PDataTransform func) override;
};


class StreamMaster : public Stream {
public:
    template<class... T> static std::shared_ptr<StreamMaster> create(T&&... t) { return std::make_shared<StreamMaster>(std::forward<T>(t)...); }
    StreamMaster() : Stream(std::make_shared<StreamCore>()) {}

    void push(type::PDataItem item) { core->push(item); }
    void close() { core->close(); core->clear(); };
};


class TransformedStream : public SimpleStream {
private:
    std::shared_ptr<StreamCore> core;
    std::shared_ptr<ct::StreamCore> parent;  // hold transform tree
    TransformedStream(std::shared_ptr<StreamCore> core) : core(core) {}

public:
    TransformedStream(std::shared_ptr<StreamCore> core, std::shared_ptr<ct::StreamCore> parent)
        : core(core), parent(parent) {}

    bool add(std::shared_ptr<StreamSubscriber> obj, bool get_last_value = false) override {
        return obj ? SimpleStream::_add(obj, core, parent, get_last_value) : false;
    }

    std::shared_ptr<SimpleStream> transform(type::PDataTransform func) override;
};

};  // namespace llhardware::core
