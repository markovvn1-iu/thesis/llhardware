#include <pybind11/pybind11.h>
#include <pybind11/stl.h>

#include "stream.hpp"

namespace py = pybind11;
namespace llhardware::core {

class PyStreamSubscriber : public StreamSubscriber {
    /* Inherit the constructors */
    using StreamSubscriber::StreamSubscriber;

    void on_get_data(type::PDataItem item) override {
        PYBIND11_OVERRIDE_PURE_NAME(
            void,          /* Return type */
            StreamSubscriber,       /* Parent class */
            "_on_get_data",  /* Name of method in Python */
            on_get_data,     /* Name of function in C++ */
            item             /* Argument(s) */
        );
    }

    void on_attach() override {
        PYBIND11_OVERRIDE_NAME(
            void,          /* Return type */
            StreamSubscriber,       /* Parent class */
            "_on_attach",  /* Name of method in Python */
            on_attach,     /* Name of function in C++ */
        );
    }

    void on_detach() override {
        PYBIND11_OVERRIDE_NAME(
            void,          /* Return type */
            StreamSubscriber,       /* Parent class */
            "_on_detach",  /* Name of method in Python */
            on_detach,     /* Name of function in C++ */
        );
    }
};

void init_module_stream(py::module &m) {
    py::class_<StreamSubscriber, std::shared_ptr<StreamSubscriber>, ct::CollectionItem, ct::StreamItem, PyStreamSubscriber>(m, "StreamSubscriber")
        .def(py::init<>());

    py::class_<SimpleStream, std::shared_ptr<SimpleStream>>(m, "SimpleStream")
        .def("add", &SimpleStream::add, py::call_guard<py::gil_scoped_release>())
        .def("transform", &SimpleStream::transform);

    py::class_<Stream, std::shared_ptr<Stream>, SimpleStream>(m, "Stream")
        .def("has", &Stream::has, py::call_guard<py::gil_scoped_release>())
        .def("remove", &Stream::remove, py::call_guard<py::gil_scoped_release>())
        .def_property_readonly("last_item", &Stream::get_last_item, py::call_guard<py::gil_scoped_release>());

    py::class_<StreamMaster, std::shared_ptr<StreamMaster>, Stream>(m, "StreamMaster")
        .def(py::init<>())
        .def("push", &StreamMaster::push, py::call_guard<py::gil_scoped_release>())
        .def("close", &StreamMaster::close, py::call_guard<py::gil_scoped_release>());
}

};  // namespace llhardware::io
