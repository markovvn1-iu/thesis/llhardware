from typing import Optional, overload

from llhardware._cpp.types import DataItem, DataTransform

from . import controllers

class SimpleUpdate(controllers.CollectionItem, controllers.PeriodicUpdateItem):
    def __init__(self) -> None: ...

class IOUpdate(controllers.CollectionItem, controllers.PeriodicUpdateItem, controllers.FileUpdateItem):
    def __init__(self) -> None: ...

class ThreadUpdaterBase:
    def start(self) -> None: ...
    def stop(self) -> None: ...
    def has(self, obj: controllers.CollectionItem) -> bool: ...
    def remove(self, obj: controllers.BaseItem) -> bool: ...
    def clear(self) -> None: ...

class ThreadUpdaterSimple:
    def add(self, obj: SimpleUpdate) -> None: ...

class ThreadUpdaterIO:
    def add(self, obj: IOUpdate) -> None: ...

class BusyThreadUpdater(ThreadUpdaterBase, ThreadUpdaterSimple):
    def __init__(self) -> None: ...

class EventThreadUpdater(ThreadUpdaterBase, ThreadUpdaterSimple, ThreadUpdaterIO):
    def __init__(self) -> None: ...
    @overload
    def add(self, obj: SimpleUpdate) -> None: ...
    @overload
    def add(self, obj: IOUpdate) -> None: ...

class StreamSubscriber(controllers.CollectionItem, controllers.StreamItem):
    def __init__(self) -> None: ...

class SimpleStream:
    def add(self, sub: StreamSubscriber, get_last_value: bool = ...) -> bool: ...
    def transform(self, transform: DataTransform) -> "SimpleStream": ...

class Stream(SimpleStream):
    @property
    def last_item(self) -> Optional[DataItem]: ...
    def has(self, sub: controllers.CollectionItem) -> None: ...
    def remove(self, sub: controllers.BaseItem) -> bool: ...

class StreamMaster(Stream):
    def __init__(self) -> None: ...
    def push(self, item: DataItem) -> None: ...
    def close(self) -> None: ...
