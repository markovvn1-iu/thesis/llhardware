#include "timer.hpp"

#include <ctime>

uint64_t llhardware::core::get_time_ns()
{
    timespec t;
    clock_gettime(CLOCK_MONOTONIC, &t);
    return (uint64_t)(t.tv_sec) * 1000000000 + t.tv_nsec;
}

void llhardware::core::delay_microseconds(unsigned int micro) {
    if (micro == 0) return;

    unsigned int uSecs = micro % 1000000;
    unsigned int wSecs = micro / 1000000;

    if (micro < 100) {
        uint64_t stop_time = get_time_ns() + micro * 1000;
        while (get_time_ns() < stop_time);
        return;
    }

    struct timespec sleeper;
    sleeper.tv_sec  = wSecs;
    sleeper.tv_nsec = uSecs * 1000L;
    nanosleep(&sleeper, NULL);
}
