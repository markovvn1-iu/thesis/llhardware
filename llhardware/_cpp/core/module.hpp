#pragma once

#include <pybind11/pybind11.h>

namespace llhardware::core {
    void init_module(pybind11::module &m);
}; // namespace llhardware::core
