from abc import ABC, abstractmethod
from typing import Any, Callable, Generic, Optional, TypeVar, final

from loguru import logger

from llhardware import _cpp

from .data_item_base import DataItem

TFrom = TypeVar("TFrom")
TTo = TypeVar("TTo")


class DataTransform(Generic[TFrom, TTo], ABC):
    class PyDataTransform(_cpp.types.DataTransform):
        _main: "DataTransform[Any, Any]"  # TODO: fix cyclical dependencies

        def __init__(self, main: "DataTransform[Any, Any]") -> None:
            self._main = main
            super().__init__()

        @logger.catch
        def transform(self, item: _cpp.types.DataItem) -> Optional[_cpp.types.DataItem]:
            if isinstance(item, _cpp.types.PyDataItem):
                res = self._main.transform(item.value)
            else:
                res = self._main.transform(DataItem._from_cpp(item))
            if res is None:
                return None
            if isinstance(res, DataItem):
                return res._base
            return _cpp.types.PyDataItem(res)

    _base: _cpp.types.DataTransform

    def __init__(self, base: Optional[_cpp.types.DataTransform] = None) -> None:
        super().__init__()
        if base is None:
            base = DataTransform.PyDataTransform(self)
        if not isinstance(base, _cpp.types.DataTransform):
            raise TypeError(f"base has type '{type(base).__name__}' when an '_cpp.type.DataTransform' is expected")
        self._base = base

    @abstractmethod
    def transform(self, item: TFrom) -> Optional[TTo]:
        pass


class CppDataTransform(DataTransform[TFrom, TTo]):
    def transform(self, item: TFrom) -> Optional[TTo]:
        return DataItem._from_cpp(self._base.transform(DataItem._to_cpp(item)))


@final
class LambdaDataTransform(DataTransform[TFrom, TTo]):
    __func: Optional[Callable[[TFrom], TTo]]

    @classmethod
    def create(cls, func: Callable[[TFrom], TTo]) -> "LambdaDataTransform[TFrom, TTo]":
        return cls(func)

    def __init__(self, func: Callable[[TFrom], TTo]) -> None:
        super().__init__()
        self.__func = func

    def transform(self, item: TFrom) -> Optional[TTo]:
        assert self.__func is not None
        return self.__func(item)


@final
class GroupDataTransform(CppDataTransform[TFrom, TTo]):
    @classmethod
    def create(cls, transforms: list[DataTransform[Any, Any]]) -> "GroupDataTransform[TFrom, TTo]":
        return cls([t._base for t in transforms])

    def __init__(self, transforms: list[_cpp.types.DataTransform]) -> None:
        super().__init__(_cpp.types.GroupDataTransform(transforms))
