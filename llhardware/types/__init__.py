from llhardware import _cpp

from .data_item_base import DataItem
from .data_items import (
    BoolDataItem,
    FloatDataItem,
    GyroAccelTempDataItem,
    IBusChannelsDataItem,
    IntDataItem,
    PyDataItem,
    StrDataItem,
    Vec3FloatDataItem,
)
from .data_transform import DataTransform, GroupDataTransform, LambdaDataTransform

DataItem._register_type(_cpp.types.DataItemType.PYTHON_OBJECT, PyDataItem)
DataItem._register_type(_cpp.types.DataItemType.BOOL, BoolDataItem)
DataItem._register_type(_cpp.types.DataItemType.INT, IntDataItem)
DataItem._register_type(_cpp.types.DataItemType.FLOAT, FloatDataItem)
DataItem._register_type(_cpp.types.DataItemType.STRING, StrDataItem)
DataItem._register_type(_cpp.types.DataItemType.VEC3_FLOAT, Vec3FloatDataItem)
DataItem._register_type(_cpp.types.DataItemType.GYRO_ACCEL_TEMP, GyroAccelTempDataItem)
DataItem._register_type(_cpp.types.DataItemType.IBUS_CHANNELS, IBusChannelsDataItem)

__all__ = [
    "DataItem",
    "PyDataItem",
    "BoolDataItem",
    "IntDataItem",
    "FloatDataItem",
    "StrDataItem",
    "Vec3FloatDataItem",
    "GyroAccelTempDataItem",
    "DataTransform",
    "LambdaDataTransform",
    "GroupDataTransform",
    "IBusChannelsDataItem",
]
