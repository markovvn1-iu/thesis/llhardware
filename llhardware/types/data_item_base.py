from abc import ABC
from typing import Any, Optional, Type

from llhardware import _cpp


class DataItem(ABC):
    _base: _cpp.types.DataItem
    _item_dict: dict[int, Type["DataItem"]] = {}

    @classmethod
    def _register_type(cls, type_id: int, wrapper: Type["DataItem"]) -> None:
        cls._item_dict[type_id] = wrapper

    @classmethod
    def _from_cpp(cls, base: Optional[_cpp.types.DataItem]) -> Any:
        if base is None:
            return None
        if isinstance(base, _cpp.types.PyDataItem):
            return base.value
        if base.type not in cls._item_dict:
            raise ValueError("Class with type={base.type} was not registered")
        return cls._item_dict[base.type](base)

    @classmethod
    def _to_cpp(cls, value: Any) -> _cpp.types.DataItem:
        if isinstance(value, DataItem):
            return value._base
        return _cpp.types.PyDataItem(value)

    def __init__(self, base: _cpp.types.DataItem) -> None:
        self._base = base
