from typing import Generic, Optional, TypeVar, final

from llhardware import _cpp

from .data_item_base import DataItem
from .data_transform import CppDataTransform, DataTransform

PythonType = TypeVar("PythonType")


@final
class PyDataItem(Generic[PythonType], DataItem):
    _base: _cpp.types.PyDataItem

    @property
    def value(self) -> PythonType:
        return self._base.value

    @classmethod
    def create(cls, value: PythonType) -> "PyDataItem[PythonType]":
        return cls(_cpp.types.PyDataItem(value))


@final
class BoolDataItem(DataItem):
    _base: _cpp.types.BoolDataItem

    @property
    def value(self) -> bool:
        return self._base.value

    @classmethod
    def create(cls, value: bool) -> "BoolDataItem":
        return cls(_cpp.types.BoolDataItem(value))

    @classmethod
    def get_value(cls) -> DataTransform["BoolDataItem", "PyDataItem[bool]"]:
        return CppDataTransform(_cpp.types.BoolDataItem.get_value())


@final
class IntDataItem(DataItem):
    _base: _cpp.types.IntDataItem

    @property
    def value(self) -> int:
        return self._base.value

    @classmethod
    def create(cls, value: int) -> "IntDataItem":
        return cls(_cpp.types.IntDataItem(value))

    @classmethod
    def get_value(cls) -> DataTransform["IntDataItem", "PyDataItem[int]"]:
        return CppDataTransform(_cpp.types.IntDataItem.get_value())


@final
class FloatDataItem(DataItem):
    # TODO: Add compare operations
    _base: _cpp.types.FloatDataItem

    @property
    def value(self) -> float:
        return self._base.value

    @classmethod
    def create(cls, value: float) -> "FloatDataItem":
        return cls(_cpp.types.FloatDataItem(value))

    @classmethod
    def get_value(cls) -> DataTransform["FloatDataItem", "PyDataItem[float]"]:
        return CppDataTransform(_cpp.types.FloatDataItem.get_value())


@final
class StrDataItem(DataItem):
    _base: _cpp.types.StrDataItem

    @property
    def value(self) -> str:
        return self._base.value

    @classmethod
    def create(cls, value: str) -> "StrDataItem":
        return cls(_cpp.types.StrDataItem(value))

    @classmethod
    def get_value(cls) -> DataTransform["StrDataItem", "PyDataItem[str]"]:
        return CppDataTransform(_cpp.types.StrDataItem.get_value())


@final
class Vec3FloatDataItem(DataItem):
    _base: _cpp.types.Vec3FloatDataItem
    _value: tuple[Optional[float], Optional[float], Optional[float]]

    @property
    def value(self) -> tuple[Optional[float], Optional[float], Optional[float]]:
        return self._value

    @property
    def x(self) -> Optional[float]:
        return self._value[0]

    @property
    def y(self) -> Optional[float]:
        return self._value[1]

    @property
    def z(self) -> Optional[float]:
        return self._value[2]

    @classmethod
    def create(cls, x: Optional[float], y: Optional[float], z: Optional[float]) -> "Vec3FloatDataItem":
        mask = (0 if x is None else 0b001) | (0 if y is None else 0b010) | (0 if z is None else 0b100)
        return cls(_cpp.types.Vec3FloatDataItem(x or 0, y or 0, z or 0, mask))

    @classmethod
    def get_x(cls) -> DataTransform["Vec3FloatDataItem", "FloatDataItem"]:
        return CppDataTransform(_cpp.types.Vec3FloatDataItem.get_x())

    @classmethod
    def get_y(cls) -> DataTransform["Vec3FloatDataItem", "FloatDataItem"]:
        return CppDataTransform(_cpp.types.Vec3FloatDataItem.get_y())

    @classmethod
    def get_z(cls) -> DataTransform["Vec3FloatDataItem", "FloatDataItem"]:
        return CppDataTransform(_cpp.types.Vec3FloatDataItem.get_z())

    def __init__(self, base: _cpp.types.Vec3FloatDataItem) -> None:
        super().__init__(base)
        x, y, z, mask = self._base.value
        self._value = (x if mask & 0b001 else None, y if mask & 0b010 else None, z if mask & 0b100 else None)


@final
class GyroAccelTempDataItem(DataItem):
    _base: _cpp.types.GyroAccelTempDataItem
    _gyro: tuple[Optional[float], Optional[float], Optional[float]]
    _accel: tuple[Optional[float], Optional[float], Optional[float]]
    _temp: Optional[float]

    @property
    def gyro(self) -> tuple[Optional[float], Optional[float], Optional[float]]:
        return self._gyro

    @property
    def accel(self) -> tuple[Optional[float], Optional[float], Optional[float]]:
        return self._accel

    @property
    def temp(self) -> Optional[float]:
        return self._temp

    @classmethod
    def create(
        cls,
        gyro: Optional[tuple[Optional[float], Optional[float], Optional[float]]],
        accel: Optional[tuple[Optional[float], Optional[float], Optional[float]]],
        temp: Optional[float],
    ) -> "GyroAccelTempDataItem":
        value = (gyro or (None, None, None)) + (accel or (None, None, None)) + (temp,)
        mask = sum((0 if v is None else 1 << i) for i, v in enumerate(value))
        return cls(_cpp.types.GyroAccelTempDataItem(*[v or 0 for v in value], mask))  # type: ignore

    @classmethod
    def get_gyro(cls) -> DataTransform["GyroAccelTempDataItem", "Vec3FloatDataItem"]:
        return CppDataTransform(_cpp.types.GyroAccelTempDataItem.get_gyro())

    @classmethod
    def get_accel(cls) -> DataTransform["GyroAccelTempDataItem", "Vec3FloatDataItem"]:
        return CppDataTransform(_cpp.types.GyroAccelTempDataItem.get_accel())

    @classmethod
    def get_temp(cls) -> DataTransform["GyroAccelTempDataItem", "FloatDataItem"]:
        return CppDataTransform(_cpp.types.GyroAccelTempDataItem.get_temp())

    def __init__(self, base: _cpp.types.GyroAccelTempDataItem) -> None:
        super().__init__(base)
        gx, gy, gz, ax, ay, az, temp, mask = self._base.value
        value = tuple((v if mask & (1 << i) else None) for i, v in enumerate([gx, gy, gz, ax, ay, az, temp]))
        self._gyro, self._accel, self._temp = value[0:3], value[3:6], value[6]  # type: ignore


@final
class IBusChannelsDataItem(DataItem):
    _base: _cpp.types.IBusChannelsDataItem
    _value: list[int]
    _is_connected: bool

    @property
    def value(self) -> list[int]:
        return self._value

    @property
    def is_connected(self) -> bool:
        return self._is_connected

    @classmethod
    def create(cls, value: list[int], is_connected: bool) -> "IBusChannelsDataItem":
        return cls(_cpp.types.IBusChannelsDataItem(value, is_connected))

    @classmethod
    def get_channel(cls, channel: int) -> DataTransform["IBusChannelsDataItem", "IntDataItem"]:
        return CppDataTransform(_cpp.types.IBusChannelsDataItem.get_channel(channel))

    @classmethod
    def get_is_connected(cls) -> DataTransform["IBusChannelsDataItem", "BoolDataItem"]:
        return CppDataTransform(_cpp.types.IBusChannelsDataItem.get_is_connected())

    def __init__(self, base: _cpp.types.IBusChannelsDataItem) -> None:
        super().__init__(base)
        self._value, self._is_connected = self._base.value, self._base.is_connected
