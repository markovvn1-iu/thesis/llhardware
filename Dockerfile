# docker build -t temp --build-arg CMAKE_BUILD_PARALLEL_LEVEL=16 --output type=local,dest=dist/ .

ARG BUILD_IMAGE=python:3.9 \
	CMAKE_BUILD_PARALLEL_LEVEL=4

FROM $BUILD_IMAGE as build

RUN pip install poetry==1.4.2 && mkdir /app

WORKDIR /app

ENV CMAKE_BUILD_PARALLEL_LEVEL=$CMAKE_BUILD_PARALLEL_LEVEL

COPY . /app

RUN find /app/llhardware -name "*.so" -delete && poetry build -f wheel



FROM scratch as artifact
COPY --from=build /app/dist/ /

