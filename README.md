<div align="center">
  <img src=".gitlab/logo.png" height="128px"/><br/>
  <h1>Low Level Hardware</h1>
  <p>Python library for developing low level hardware</p>
</div>



## 📝 About The Project

Python library to simplify and accelerate low-level software development to speed up the design and prototyping of new inventions. Usage examples can be found in the `scripts` directory.

## ⚡️ Quick start

Install package using *pip* package manager:

```bash
pip install --index-url https://gitlab.com/api/v4/projects/43571806/packages/pypi/simple llhardware
```

### :star: Poetry-way to quick start

To install this package with *poetry* you have to add the new source to your `pyproject.toml` file:

```bash
[[tool.poetry.source]]
name = "llhardware"
url = "https://gitlab.com/api/v4/projects/43571806/packages/pypi/simple"
```

And add credentials for the read-only package repository access to the `poetry.toml` file:

```bash
[repositories]
[repositories.llhardware]
url = "https://gitlab.com/api/v4/projects/43571806/packages/pypi/simple"

[http-basic]
[http-basic.llhardware]
username = "pypi"
password = "pyxQRtboKuXoK63mNGLk"
```

You can then add the package as a dependency:

```bash
poetry add --source llhardware llhardware
```

## ⚙️ Developing

Download the repository and change the current directory:

```bash
git clone https://gitlab.com/markovvn1-iu/thesis/llhardware.git && cd llhardware
```

Configure virtual environment. Make sure a `.venv` folder has been created after this step.

```bash
sudo apt install i2c-tools
make venv
# source .venv/bin/activate
```

Run application:

```bash
make up
```

Run linters and formaters:

```bash
make lint  # check code quality
make test  # run test quality
make format  # beautify code
```

## :computer: Contributors

<p>
  :boy: <b>Vladimir Markov</b> <br>
  &nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Email: <a>Markovvn1@gmail.com</a> <br>
  &nbsp;&nbsp;&nbsp;&nbsp;&nbsp; GitLab: <a href="https://gitlab.com/markovvn1">@markovvn1</a> <br>
</p>