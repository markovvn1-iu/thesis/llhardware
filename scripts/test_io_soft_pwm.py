import time

from llhardware import core, io  # Import core and io modules

thr = core.BusyThreadUpdater.create()  # Create working thread
thr.start()  # Start working thread

# Create GPIO, setup 17 pin as output and create SoftPWM object
s = io.SoftPWM.create(io.GPIO.create().output(17), int(1e9) // 100, thr)

s.pulse_width = 1_500_000  # Set width of the pulse
time.sleep(1)  # Sleep for 1 second
s.fill_factor = 0.5  # Set PWM fill factor to 50%
