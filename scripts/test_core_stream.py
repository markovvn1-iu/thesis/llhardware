import time

from llhardware import core

stream: core.StreamMaster[int] = core.StreamMaster.create()
stream.push(111)
sub1 = core.LambdaStreamSubscriber.create(lambda v: print("sub1", v), stream)
sub2 = core.LambdaStreamSubscriber.create(lambda v: print("sub2", v), stream | (lambda x: x * 2) | (lambda x: x + 3))
# stream.push(123)
time.sleep(0.2)
