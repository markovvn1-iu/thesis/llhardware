from llhardware import core, io, types

thr = core.EventThreadUpdater.create()
ibus = io.IBus.create(io.Serial.create("/dev/ttyS0", io.Serial.Baundrate.B115200), channels=6, thr=thr)
sub: core.LambdaStreamSubscriber[types.IBusChannelsDataItem] = core.LambdaStreamSubscriber.create(
    lambda x: print(x.value, x.is_connected), ibus.stream
)
thr.start()
