from typing import List

from llhardware import core, devices, io, types


# Define a class to represent plain devices
class MyPlainDevices:
    # Declare public devices
    remote_control_stream: core.Stream[types.IBusChannelsDataItem]
    main_engine: devices.ElectronicSpeedController
    servos: List[devices.DigitalServo]

    def __init__(self) -> None:
        # Create threads
        self._event_thr = core.EventThreadUpdater.create()
        self._busy_thr = core.BusyThreadUpdater.create()

        # Create an iBus protocol handler on top of the Serial interface
        self._ibus = io.IBus.create(
            io.Serial.create("/dev/ttyS0", io.Serial.Baundrate.B115200),
            channels=6,
            thr=self._event_thr,
        )
        self.remote_control_stream = self._ibus.stream

        # Create GPIO
        gpio = io.GPIO.create()
        # Creating a controller for the main engine of an aircraft
        self.main_engine = devices.ElectronicSpeedController.create(
            io.SoftPWM.create(gpio.output(17), int(1e9) // 50, self._busy_thr),
            speed_range=(0, 100),
            pulse_width_range=(1_000_000, 2_000_000),
            init_speed=0,
        )
        # Creating controllers to control all servo drives
        self.servos = [
            self.create_servo(gpio, 18, self._busy_thr),
            self.create_servo(gpio, 27, self._busy_thr),
            self.create_servo(gpio, 22, self._busy_thr),
            self.create_servo(gpio, 23, self._busy_thr),
        ]

    # Auxiliary function to create a servo drive controller
    @staticmethod
    def create_servo(gpio: io.GPIO, pin: int, busy_thr: core.BusyThreadUpdater) -> devices.DigitalServo:
        return devices.DigitalServo.create(
            io.SoftPWM.create(gpio.output(pin), int(1e9) // 100, busy_thr),
            pos_range=(-90, 90),
            pulse_width_range=(500_000, 2_500_000),
        )

    def start(self) -> None:
        self._event_thr.start()
        self._busy_thr.start()

    def stop(self) -> None:
        self._event_thr.start()
        self._busy_thr.start()


class MyPlain:
    devices: MyPlainDevices

    def __init__(self) -> None:
        # Create all devices
        self.devices = MyPlainDevices()
        # Subscribe to receive new messages from the remote control
        self._remote_control_sub = core.LambdaStreamSubscriber.create(
            self.on_remote_command, self.devices.remote_control_stream
        )

    def on_remote_command(self, data: types.IBusChannelsDataItem) -> None:
        # Do some plain control
        self.devices.main_engine.speed = (data.value[0] - 1000) / 1000 * 100
