import time

from llhardware import core, devices, io

thr = core.BusyThreadUpdater.create()
thr.start()

gpio = io.GPIO.create()
esc = devices.ElectronicSpeedController.create(
    io.SoftPWM.create(gpio.output(17), int(1e9) // 50, thr),
    speed_range=(0, 100),
    pulse_width_range=(1_000_000, 2_000_000),
    init_speed=0,
)


def create_servo(pin: int) -> devices.DigitalServo:
    return devices.DigitalServo.create(
        io.SoftPWM.create(gpio.output(pin), int(1e9) // 100, thr),
        pos_range=(-90, 90),
        pulse_width_range=(500_000, 2_500_000),
    )


servos = [create_servo(18), create_servo(27), create_servo(22), create_servo(23)]


def play() -> None:
    for s in servos:
        s.position = 0
    time.sleep(0.5)
    for s in servos:
        s.position = 90
    time.sleep(0.5)
    for s in servos:
        s.position = -90
    time.sleep(0.5)
    for s in servos:
        s.position = 90
    time.sleep(0.5)
    for s in servos:
        s.position = -90
        time.sleep(0.5)
    esc.speed = 10
    time.sleep(5.0)
    esc.speed = 0
