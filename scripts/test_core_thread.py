import time

from llhardware import core

thr = core.BusyThreadUpdater.create()

start_time = time.monotonic_ns()
a = core.LambdaSimpleUpdate.create(
    lambda _id, _: print(f"Update 300! {(time.monotonic_ns() - start_time) / 1e9}"), 300_000_000
)
b = core.LambdaSimpleUpdate.create(
    lambda _id, _: print(f"Update 500! {(time.monotonic_ns() - start_time) / 1e9}"), 500_000_000
)
thr.add(a)
thr.add(b)

thr.start()
time.sleep(5.02)
del b
time.sleep(5.02)
thr.stop()
