from llhardware import controllers, core, devices, io, planners

gpio = io.GPIO.create()
en = gpio.output(11, inverse=True, init_value=io.GPIO.DigitalValue.LOW)

thr = core.BusyThreadUpdater.create()
motorX = controllers.StepperMotorPlanController.create(
    devices.GPIOStepperMotor.create(gpio.output(4), gpio.output(17), thr), thr
)
motorY = controllers.StepperMotorPlanController.create(
    devices.GPIOStepperMotor.create(gpio.output(27), gpio.output(22), thr), thr
)
motorZ = controllers.StepperMotorPlanController.create(
    devices.GPIOStepperMotor.create(gpio.output(10), gpio.output(9), thr), thr
)
thr.start()

en.digital_write(io.GPIO.DigitalValue.HIGH)
p = planners.GroupPlan.create([planners.SimplePlan.create(0, 60000, 0.2), planners.SimplePlan.create(60000, 0, 0.2)])
motorY.set_plan(p.freeze(motorY.cur_state.cur_time, motorY.cur_state.pos, 10000 / 1e9))
