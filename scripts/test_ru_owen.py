import asyncio
import time

from llhardware import core, io

owen = io.RuOwen.create(io.Serial.create("/dev/ttyUSB0", io.Serial.Baundrate.B115200), timeout_ns=int(20e9))
# serial = io.Serial.create("/dev/ttyS0", io.Serial.Baundrate.B115200)
# owen = io.RuOwen.create(serial)

thr = core.EventThreadUpdater.create()
thr.add(owen)
thr.start()


async def main(n: int) -> int:
    packet = owen.make_packet(16, True, owen.name2cmd("r.Cn"))
    old_value = ""
    start_time = time.monotonic_ns()
    new_task = core.AsyncFutureStreamSubscriber.create(owen.send_packet(packet))
    for i in range(n - 1):
        old_task = new_task
        if i + 1 < n - 1:
            new_task = core.AsyncFutureStreamSubscriber.create(owen.send_packet(packet))
        try:
            new_value = bin(owen.parse_packet((await old_task).value).data[0])[2:]
            if new_value != old_value:
                old_value = new_value
                print(new_value)
        except asyncio.CancelledError as e:
            print(repr(e))
    return time.monotonic_ns() - start_time


# loop = asyncio.new_event_loop()
# packet = owen.make_packet(16, True, owen.name2cmd("r.Cn"))
# loop.run_until_complete(core.AsyncFutureStreamSubscriber.create(owen.send_packet(packet), loop=loop)).value

asyncio.run(main(1000))
