from typing import List, Tuple

import matplotlib.pyplot as plt

from llhardware import planners

p = planners.SimplePlan.create(0, -20000, 0.0095)
cp = p.freeze(500_000, (0, 0), 10000 / 1e9).compile(1_000_000, (0, 0))


steps: List[Tuple[int, int]] = [(0, 0)]
next_update_ns = 1_000_000
# while next_update_ns >= 0:
for i in range(20):
    steps.append((next_update_ns, steps[-1][1]))
    next_update_ns, target_pos = cp.compute(next_update_ns)
    print(steps[-1][0], next_update_ns, target_pos)
    steps.append((steps[-1][0], target_pos))


t = [(next_update_ns - 500_000) * i // 1000 for i in range(1000)]
ideal_plot = [p.start_vel * ti / 1e9 + (p.end_vel - p.start_vel) / p.duration * ti * ti / 2e18 for ti in t]
plt.plot([ti + 500_000 for ti in t], ideal_plot)
plt.plot([s[0] for s in steps], [s[1] for s in steps])
plt.show()
