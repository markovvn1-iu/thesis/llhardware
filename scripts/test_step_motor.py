from llhardware import core, devices, io

gpio = io.GPIO.create()
en = gpio.output(11, inverse=True, init_value=io.GPIO.DigitalValue.LOW)

thr = core.BusyThreadUpdater.create()
motorX = devices.GPIOStepperMotor.create(gpio.output(4), gpio.output(17), thr)
motorY = devices.GPIOStepperMotor.create(gpio.output(27), gpio.output(22), thr)
motorZ = devices.GPIOStepperMotor.create(gpio.output(10), gpio.output(9), thr)
thr.start()

en.digital_write(io.GPIO.DigitalValue.HIGH)
motorY.move(10)
